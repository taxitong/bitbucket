## Welcome to My Repository. 
My name is Nhat Luu-Quang and this is the repo that contains outstanding projects that I have worked on in FPT university courses.

Java Web Lab:  
(Lab1) Demo Video: Hana Shop: [Link](https://www.youtube.com/watch?v=_T6f8yw-29E&list=PLQA1x_4Op540MRvvGIn7mb02HPcPb4ZPr&ab_channel=Luu-QuangNhat)  
(Lab2) Demo Video: Quiz Online: [Link](https://www.youtube.com/watch?v=hUFBw9AGm_E&list=PLQA1x_4Op543OWQPNaMbMxM8b6ujZGt_7&ab_channel=Luu-QuangNhat)  
(Lab3) Demo Video: Car Rental: [Link](https://www.youtube.com/watch?v=Hhuu8WpC0Fo&list=PLQA1x_4Op543_0BbtID-nhWWY3C0SUsNk&ab_channel=Luu-QuangNhat)  