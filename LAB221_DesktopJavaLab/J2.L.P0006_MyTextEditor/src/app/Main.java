/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import com.ozten.font.JFontChooser;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.FileDialog;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.Highlighter;
import javax.swing.undo.UndoManager;

/**
 *
 * @author Admin
 */
public class Main extends javax.swing.JFrame {

    String fileName = "Untitle";
    String fileAddress = null;
    boolean isNew = true;
    boolean isSave = true;
    boolean isTextHasChange = false;

    String saveFont_FileName = "SaveFont\\saveFontAddr.txt";

    UndoManager um = new UndoManager();
    boolean justOpen_CanNotUndoYet = false;
    int countUndo = 0;

    int linenum;
    int columnnum;

    public Main() {
        initComponents();
        setSize(600, 600);
        setTitle(fileName);
        setLocationRelativeTo(null);

        mniCopy.setEnabled(false);
        mniCut.setEnabled(false);
        mniDelete.setEnabled(false);
        mniSelectAll.setVisible(false);
        loadFont();

        txtTextArea.getDocument().addDocumentListener(new DocumentListener() {//check J2.S.P0115_VerifyInputData
            @Override
            public void insertUpdate(DocumentEvent e) {
                isTextHasChange = true;
                justOpen_CanNotUndoYet = false;
                countUndo++;
                //System.out.println(countUndo);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                isTextHasChange = true;
                justOpen_CanNotUndoYet = false;
                countUndo++;
                //System.out.println(countUndo);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                //isTextHasChange = true;
            }
        });

        txtTextArea.getDocument().addUndoableEditListener(new UndoableEditListener() {
            @Override
            public void undoableEditHappened(UndoableEditEvent e) {
                um.addEdit(e.getEdit());
            }
        });

        txtTextArea.addCaretListener(new CaretListener() {
            //column number and line number of cursor's current position
            //https://stackoverflow.com/questions/5139995/java-column-number-and-line-number-of-cursors-current-position
            @Override
            public void caretUpdate(CaretEvent e) {
                try {
                    int caretpos = txtTextArea.getCaretPosition();
                    linenum = txtTextArea.getLineOfOffset(caretpos);
                    columnnum = caretpos - txtTextArea.getLineStartOffset(linenum);
                    linenum += 1;
                } catch (Exception ex) {
                }

                // Once we know the position of the line and the column, pass it to a helper function for updating the status bar.
                updateStatus(linenum, columnnum);
            }

            private void updateStatus(int linenum, int columnnum) {
                ActionEvent evt = null;//have no idel where this coming from, but need this to see value of colum and row change while typing
                //System.out.println("Line: " + linenum + " Column: " + columnnum);
                mncStatusBarActionPerformed(evt);
            }
        });

        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                saveFont();
                if (isTextHasChange == true) {//check if text change
                    int choice = JOptionPane.showConfirmDialog(null, "Do you want to save change to " + fileName,
                            "Notepad", JOptionPane.YES_NO_CANCEL_OPTION);//ask save or not
                    if (choice == JOptionPane.YES_OPTION) {
                        save();
                        if (fileName != null && fileAddress != null) {
                            System.exit(0);
                        }
                    } else if (choice == JOptionPane.NO_OPTION) {
                        System.exit(0);
                    }
                } else {
                    System.exit(0);
                }
            }
        });
    }

    void printTest(String functionName) {
        //
        System.out.println(functionName);
        System.out.println(fileName);
        System.out.println(fileAddress);
        //
    }

    void save() {
        if (isNew == true) {
            saveAs();
            printTest("save");
            if (fileName != null && fileAddress != null) {
                isNew = false;
            }
        } else {
            try {
                FileWriter fw = new FileWriter(fileAddress + fileName);
                fw.write(txtTextArea.getText());
                fw.close();
            } catch (Exception e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(this, "Sothing wrong, Save failed");
            }
        }
    }

    void saveAs() {
        //save as file + upgrade(self)
        //https://www.youtube.com/watch?v=i4NkMYB7ADU&list=PL_QPQmz5C6WUTPONMeQcEEdKax0wGsnZB&index=5
        FileDialog fd = new FileDialog(this, "Save As", FileDialog.SAVE);
        fd.setVisible(true);
        if (fd.getFile() != null) {
            fileName = fd.getFile();
            fileAddress = fd.getDirectory();
            setTitle(fileName);
            //printTest("save as");
        }
        if (fileName != null && fileAddress != null) {
            try {
                FileWriter fw = new FileWriter(fileAddress + fileName);
                fw.write(txtTextArea.getText());
                fw.close();
            } catch (Exception e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(this, "Sothing wrong, Save As failed");
            }
        }
    }

    void saveFont() {
        //save font choosen
        try {
            FileWriter fw = new FileWriter(saveFont_FileName);
            //save font - separated elements
            //https://stackoverflow.com/questions/29065965/save-and-load-font-fontcolour
            int size = txtTextArea.getFont().getSize();
            String fontName = txtTextArea.getFont().getFontName();
            int RBG = txtTextArea.getForeground().getRGB();
            fw.write(size + ";" + fontName + ";" + RBG);
            fw.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Sothing wrong, Save Font failed");
        }
    }

    void loadFont() {
        try {
            BufferedReader br = new BufferedReader(new FileReader(saveFont_FileName));
            String line = null;
            while ((line = br.readLine()) != null) {
                //load font - separated elements
                //check save font + J2.S.P0021_CoursesManagement
                String temp[] = line.split(";");
                int size = Integer.parseInt(temp[0].trim());
                String fontName = temp[1].trim();
                int RBG = Integer.parseInt(temp[2].trim());
                txtTextArea.setFont(new Font(fontName, RBG, size));
            }
            br.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "File Font Not Opened");
            e.printStackTrace();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        txtTextArea = new javax.swing.JTextArea();
        jLabelStatus = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        mniNew = new javax.swing.JMenuItem();
        mniOpen = new javax.swing.JMenuItem();
        mniSave = new javax.swing.JMenuItem();
        mniSaveAs = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        mniPrint = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        mniExit = new javax.swing.JMenuItem();
        mnEdit = new javax.swing.JMenu();
        mniUndo = new javax.swing.JMenuItem();
        mniRedo = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        mniCut = new javax.swing.JMenuItem();
        mniCopy = new javax.swing.JMenuItem();
        mniPaste = new javax.swing.JMenuItem();
        mniDelete = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        mniFind = new javax.swing.JRadioButtonMenuItem();
        Replace = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        mniSelectAll = new javax.swing.JMenuItem();
        mniTimeDate = new javax.swing.JMenuItem();
        mnFormat = new javax.swing.JMenu();
        mniChangeFont = new javax.swing.JMenuItem();
        mniResetFont = new javax.swing.JMenuItem();
        mnView = new javax.swing.JMenu();
        mniZoom = new javax.swing.JMenuItem();
        mncStatusBar = new javax.swing.JCheckBoxMenuItem();
        mnHelp = new javax.swing.JMenu();
        mniViewHelp = new javax.swing.JMenuItem();
        mniSendFeedback = new javax.swing.JMenuItem();
        jSeparator6 = new javax.swing.JPopupMenu.Separator();
        mniAboutNotepad = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        txtTextArea.setColumns(20);
        txtTextArea.setRows(5);
        jScrollPane1.setViewportView(txtTextArea);

        getContentPane().add(jScrollPane1, java.awt.BorderLayout.CENTER);
        getContentPane().add(jLabelStatus, java.awt.BorderLayout.PAGE_END);

        jMenu1.setText("New");

        mniNew.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        mniNew.setText("New");
        mniNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mniNewActionPerformed(evt);
            }
        });
        jMenu1.add(mniNew);

        mniOpen.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        mniOpen.setText("Open");
        mniOpen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mniOpenActionPerformed(evt);
            }
        });
        jMenu1.add(mniOpen);

        mniSave.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        mniSave.setText("Save");
        mniSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mniSaveActionPerformed(evt);
            }
        });
        jMenu1.add(mniSave);

        mniSaveAs.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        mniSaveAs.setText("Save As");
        mniSaveAs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mniSaveAsActionPerformed(evt);
            }
        });
        jMenu1.add(mniSaveAs);
        jMenu1.add(jSeparator2);

        mniPrint.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_MASK));
        mniPrint.setText("Print");
        mniPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mniPrintActionPerformed(evt);
            }
        });
        jMenu1.add(mniPrint);
        jMenu1.add(jSeparator5);

        mniExit.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, java.awt.event.InputEvent.ALT_MASK));
        mniExit.setText("Exit");
        mniExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mniExitActionPerformed(evt);
            }
        });
        jMenu1.add(mniExit);

        jMenuBar1.add(jMenu1);

        mnEdit.setText("Edit");
        mnEdit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                mnEditMouseClicked(evt);
            }
        });

        mniUndo.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Z, java.awt.event.InputEvent.CTRL_MASK));
        mniUndo.setText("Undo");
        mniUndo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mniUndoActionPerformed(evt);
            }
        });
        mnEdit.add(mniUndo);

        mniRedo.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Y, java.awt.event.InputEvent.CTRL_MASK));
        mniRedo.setText("Redo");
        mniRedo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mniRedoActionPerformed(evt);
            }
        });
        mnEdit.add(mniRedo);
        mnEdit.add(jSeparator1);

        mniCut.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_X, java.awt.event.InputEvent.CTRL_MASK));
        mniCut.setText("Cut");
        mniCut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mniCutActionPerformed(evt);
            }
        });
        mnEdit.add(mniCut);

        mniCopy.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_MASK));
        mniCopy.setText("Copy");
        mniCopy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mniCopyActionPerformed(evt);
            }
        });
        mnEdit.add(mniCopy);

        mniPaste.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_V, java.awt.event.InputEvent.CTRL_MASK));
        mniPaste.setText("Paste");
        mniPaste.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mniPasteActionPerformed(evt);
            }
        });
        mnEdit.add(mniPaste);

        mniDelete.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_DELETE, 0));
        mniDelete.setText("Delete");
        mniDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mniDeleteActionPerformed(evt);
            }
        });
        mnEdit.add(mniDelete);
        mnEdit.add(jSeparator4);

        mniFind.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.CTRL_MASK));
        mniFind.setText("Find");
        mniFind.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mniFindActionPerformed(evt);
            }
        });
        mnEdit.add(mniFind);

        Replace.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_H, java.awt.event.InputEvent.CTRL_MASK));
        Replace.setText("Replace");
        Replace.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ReplaceActionPerformed(evt);
            }
        });
        mnEdit.add(Replace);
        mnEdit.add(jSeparator3);

        mniSelectAll.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.CTRL_MASK));
        mniSelectAll.setText("Select All");
        mniSelectAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mniSelectAllActionPerformed(evt);
            }
        });
        mnEdit.add(mniSelectAll);

        mniTimeDate.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F5, 0));
        mniTimeDate.setText("Time/Date");
        mniTimeDate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mniTimeDateActionPerformed(evt);
            }
        });
        mnEdit.add(mniTimeDate);

        jMenuBar1.add(mnEdit);

        mnFormat.setText("Format");

        mniChangeFont.setText("Font...");
        mniChangeFont.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mniChangeFontActionPerformed(evt);
            }
        });
        mnFormat.add(mniChangeFont);

        mniResetFont.setText("Reset Font");
        mniResetFont.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mniResetFontActionPerformed(evt);
            }
        });
        mnFormat.add(mniResetFont);

        jMenuBar1.add(mnFormat);

        mnView.setText("View");

        mniZoom.setText("Zoom");
        mniZoom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mniZoomActionPerformed(evt);
            }
        });
        mnView.add(mniZoom);

        mncStatusBar.setText("Status Bar");
        mncStatusBar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mncStatusBarActionPerformed(evt);
            }
        });
        mnView.add(mncStatusBar);

        jMenuBar1.add(mnView);

        mnHelp.setText("Help");

        mniViewHelp.setText("View Help");
        mniViewHelp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mniViewHelpActionPerformed(evt);
            }
        });
        mnHelp.add(mniViewHelp);

        mniSendFeedback.setText("Send Feedback");
        mniSendFeedback.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mniSendFeedbackActionPerformed(evt);
            }
        });
        mnHelp.add(mniSendFeedback);
        mnHelp.add(jSeparator6);

        mniAboutNotepad.setText("About Notepad");
        mniAboutNotepad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mniAboutNotepadActionPerformed(evt);
            }
        });
        mnHelp.add(mniAboutNotepad);

        jMenuBar1.add(mnHelp);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void mniNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mniNewActionPerformed
        if (isTextHasChange) {
            int choice = JOptionPane.showConfirmDialog(null, "Do you want to save change to " + fileName,
                    "Notepad", JOptionPane.YES_NO_CANCEL_OPTION);
            if (choice == JOptionPane.YES_OPTION) {
                mniSaveActionPerformed(evt);
                if (fileName != null && fileAddress != null) {
                    fileName = "Untitle";
                    fileAddress = null;
                    setTitle(fileName);
                    txtTextArea.setText("");
                    isNew = true;
                }
            } else if (choice == JOptionPane.NO_OPTION) {
                fileName = "Untitle";
                fileAddress = null;
                setTitle(fileName);
                txtTextArea.setText("");
                isNew = true;
            }
        } else {
            fileName = "Untitle";
            fileAddress = null;
            setTitle(fileName);
            txtTextArea.setText("");
            isNew = true;
            isTextHasChange = false;
        }

    }//GEN-LAST:event_mniNewActionPerformed

    private void mniOpenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mniOpenActionPerformed
        if (isTextHasChange) {
            int choice = JOptionPane.showConfirmDialog(null, "Do you want to save change to " + fileName,
                    "Notepad", JOptionPane.YES_NO_CANCEL_OPTION);
            if (choice == JOptionPane.YES_OPTION) {
                mniSaveActionPerformed(evt);
                if (fileName != null && fileAddress != null) {
                    open();
                }
            } else if (choice == JOptionPane.NO_OPTION) {
                open();
            }
        } else {
            open();
        }
    }//GEN-LAST:event_mniOpenActionPerformed

    private void mniSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mniSaveActionPerformed
        //save + upgrade(self): check save as
        if (isNew == true) {
            mniSaveAsActionPerformed(evt);
            if (fileName != null && fileAddress != null) {
                isNew = false;
            }
        } else {
            try {
                FileWriter fw = new FileWriter(fileAddress + fileName);
                fw.write(txtTextArea.getText());
                fw.close();
                isNew = false;
            } catch (Exception e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(this, "Sothing wrong, Save failed");
            }
        }
    }//GEN-LAST:event_mniSaveActionPerformed

    private void mniExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mniExitActionPerformed
        saveFont();
        if (isTextHasChange == true) {//check if text change
            int choice = JOptionPane.showConfirmDialog(null, "Do you want to save change to " + fileName,
                    "Notepad", JOptionPane.YES_NO_CANCEL_OPTION);//ask save or not
            if (choice == JOptionPane.YES_OPTION) {
                mniSaveActionPerformed(evt);
                if (fileName != null && fileAddress != null) {
                    System.exit(0);
                }
            } else if (choice == JOptionPane.NO_OPTION) {
                System.exit(0);
            }
        } else {
            System.exit(0);
        }
        //save font choosen
        txtTextArea.getFont();
    }//GEN-LAST:event_mniExitActionPerformed

    private void mniSaveAsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mniSaveAsActionPerformed
        //save as file + upgrade(self)
        //https://www.youtube.com/watch?v=i4NkMYB7ADU&list=PL_QPQmz5C6WUTPONMeQcEEdKax0wGsnZB&index=5
        FileDialog fd = new FileDialog(this, "Save As", FileDialog.SAVE);
        fd.setVisible(true);
        if (fd.getFile() != null) {
            fileName = fd.getFile();
            fileAddress = fd.getDirectory();
            setTitle(fileName);
        }
        if (fileName != null && fileAddress != null) {
            try {
                FileWriter fw = new FileWriter(fileAddress + fileName);
                fw.write(txtTextArea.getText());
                fw.close();
            } catch (Exception e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(this, "Sothing wrong, Save As failed");
            }
        }
    }//GEN-LAST:event_mniSaveAsActionPerformed

    private void mniChangeFontActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mniChangeFontActionPerformed
        //ChangeFont cf = new ChangeFont(txtTextArea);

        //https://www.youtube.com/watch?v=Z3c8R7_LmbA
        JFontChooser fc = new JFontChooser();
        JOptionPane.showMessageDialog(null, fc, "Please choose font", JOptionPane.PLAIN_MESSAGE);
        txtTextArea.setFont(fc.getPreviewFont());
    }//GEN-LAST:event_mniChangeFontActionPerformed

    private void mniResetFontActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mniResetFontActionPerformed
        //reset default font
        //https://coderanch.com/t/335344/java/Reset-Default-Font
        UIDefaults defaults = UIManager.getLookAndFeelDefaults();
        Font font = defaults.getFont("TextField.font");
        txtTextArea.setFont(font);
        //when exit, the font will be save as default too
    }//GEN-LAST:event_mniResetFontActionPerformed

    private void mnEditMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mnEditMouseClicked
        checkPaste();
        if (txtTextArea.getSelectedText() != null) {
            mniCopy.setEnabled(true);
            mniCut.setEnabled(true);

            mniDelete.setEnabled(true);
        } else {
            mniCopy.setEnabled(false);
            mniCut.setEnabled(false);

            mniDelete.setEnabled(false);
        }
        if (txtTextArea.getText() != null) {
            mniSelectAll.setEnabled(true);
        } else {
            mniSelectAll.setEnabled(false);
        }
    }//GEN-LAST:event_mnEditMouseClicked

    private void mniRedoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mniRedoActionPerformed
        try {
            um.redo();
        } catch (Exception e) {
            //There are no redo steps left in memory
        }
    }//GEN-LAST:event_mniRedoActionPerformed

    private void mniUndoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mniUndoActionPerformed

        if (!justOpen_CanNotUndoYet) {
            if (countUndo > 0) {
                try {
                    um.undo();
                    //System.out.println(um.undo());
                } catch (Exception e) {
                    //There are no undo steps left in memory
                }
                countUndo--;//when we undo delete, we will insert & when we undo write, we will delete
                countUndo--;//so we need -- 2 time
            }
        }
    }//GEN-LAST:event_mniUndoActionPerformed

    private void mniSelectAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mniSelectAllActionPerformed
        txtTextArea.selectAll();
    }//GEN-LAST:event_mniSelectAllActionPerformed

    private void mniDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mniDeleteActionPerformed
        //how to remove a selected jtext
        //https://stackoverflow.com/questions/5255466/deleting-only-a-selected-text-in-a-text-area
        txtTextArea.setText(txtTextArea.getText().replace(txtTextArea.getSelectedText(), ""));
    }//GEN-LAST:event_mniDeleteActionPerformed

    private void mniPasteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mniPasteActionPerformed
        String data;
        //how to get clipboard content
        //https://stackoverflow.com/questions/7105778/get-readable-text-only-from-clipboard
        try {
            data = (String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor);
            //how to insert data into current location of jtextare
            //https://stackoverflow.com/questions/16759850/insert-text-in-jtextarea-at-caret-position
            txtTextArea.insert(data, txtTextArea.getCaretPosition());
        } catch (UnsupportedFlavorException ex) {
            //Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            //Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_mniPasteActionPerformed

    private void mniCutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mniCutActionPerformed
        //how to copy to clipboard
        //https://stackoverflow.com/questions/3591945/copying-to-the-clipboard-in-java
        StringSelection selection = new StringSelection(txtTextArea.getSelectedText());
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(selection, selection);
        //delete select
        txtTextArea.setText(txtTextArea.getText().replace(txtTextArea.getSelectedText(), ""));
    }//GEN-LAST:event_mniCutActionPerformed

    private void mniCopyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mniCopyActionPerformed
        String s = "";
        s = txtTextArea.getSelectedText();
        //how to copy to clipboard
        //https://stackoverflow.com/questions/3591945/copying-to-the-clipboard-in-java
        StringSelection selection = new StringSelection(s);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(selection, selection);
    }//GEN-LAST:event_mniCopyActionPerformed

    private void ReplaceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ReplaceActionPerformed
        FindAndReplace far = new FindAndReplace(txtTextArea);
    }//GEN-LAST:event_ReplaceActionPerformed

    private void mniFindActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mniFindActionPerformed
        Find f = new Find(txtTextArea);
    }//GEN-LAST:event_mniFindActionPerformed

    private void mniTimeDateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mniTimeDateActionPerformed
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        txtTextArea.append("" + now);
    }//GEN-LAST:event_mniTimeDateActionPerformed

    private void mniViewHelpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mniViewHelpActionPerformed
        JOptionPane.showMessageDialog(this, "View Help function is not ready");//use html file of notepad (referance khanh's)
    }//GEN-LAST:event_mniViewHelpActionPerformed

    private void mniSendFeedbackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mniSendFeedbackActionPerformed
        JOptionPane.showMessageDialog(this, "Send Feedback function is not ready");//use html file of notepad (referance khanh's)
    }//GEN-LAST:event_mniSendFeedbackActionPerformed

    private void mniAboutNotepadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mniAboutNotepadActionPerformed
        About a = new About();
    }//GEN-LAST:event_mniAboutNotepadActionPerformed

    private void mniPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mniPrintActionPerformed
        JOptionPane.showMessageDialog(this, "Print function is not ready");
    }//GEN-LAST:event_mniPrintActionPerformed

    private void mniZoomActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mniZoomActionPerformed
        //https://stackoverflow.com/questions/8675038/increasing-decreasing-font-size-inside-textarea-using-jbutton
        Font font = txtTextArea.getFont();
        float size = font.getSize() + 1.0f;
        txtTextArea.setFont(font.deriveFont(size));
    }//GEN-LAST:event_mniZoomActionPerformed

    private void mncStatusBarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mncStatusBarActionPerformed
        //status bar (reference ideal only - not same)
        //https://stackoverflow.com/questions/3035880/how-can-i-create-a-bar-in-the-bottom-of-a-java-app-like-a-status-bar
        if (!mncStatusBar.isSelected()) {//it is select so now we unselect it
            jLabelStatus.setText("Window: " + "Row: " + linenum + " - Colum: " + columnnum);
            mncStatusBar.setSelected(false);
            //System.out.println("un-select");
        } else {//it is un-select so now we select it
            jLabelStatus.setText("");
            mncStatusBar.setSelected(true);
            //System.out.println("select");
        }
    }//GEN-LAST:event_mncStatusBarActionPerformed

    public void checkPaste() {
        String data;
        //how to get clipboard content
        //https://stackoverflow.com/questions/7105778/get-readable-text-only-from-clipboard
        try {
            data = (String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor);
            if (data != null) {
                mniPaste.setEnabled(true);
            } else {
                mniPaste.setEnabled(false);
            }
        } catch (UnsupportedFlavorException ex) {
            //Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            mniPaste.setEnabled(false);
        } catch (IOException ex) {
            //Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            mniPaste.setEnabled(false);
        }
    }

    public void open() {
        //Open file GUI: https://www.youtube.com/watch?v=xCqy78msAqE
        FileDialog fd = new FileDialog(this, "Open", FileDialog.LOAD);
        fd.setVisible(true);
        if (fd.getFile() != null) {
            fileName = fd.getFile();
            fileAddress = fd.getDirectory();
            //printTest("open");
            setTitle(fileName);

            try {
                BufferedReader br = new BufferedReader(new FileReader(fileAddress + fileName));
                txtTextArea.setText("");
                String line = null;
                Reader r = br;
                //read txt char by char: http://tutorials.jenkov.com/java-io/bufferedreader.html
                //using readLine() will returns you the line String without the newline character. So dont use it: https://stackoverflow.com/questions/7537250/cant-find-the-newline-character-in-a-file-java
                int theCharNum = r.read();
                while (theCharNum != -1) {
                    char theChar = (char) theCharNum;
                    txtTextArea.append("" + theChar);//System.out.print(theChar);
                    theCharNum = r.read();
                }
                br.close();
                isNew = false;
            } catch (Exception e) {
                JOptionPane.showMessageDialog(this, "File Not Opened");
                e.printStackTrace();
            }
        }
        isTextHasChange = false;
        justOpen_CanNotUndoYet = true;
        countUndo = 0;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Main().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem Replace;
    private javax.swing.JLabel jLabelStatus;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JPopupMenu.Separator jSeparator6;
    private javax.swing.JMenu mnEdit;
    private javax.swing.JMenu mnFormat;
    private javax.swing.JMenu mnHelp;
    private javax.swing.JMenu mnView;
    private javax.swing.JCheckBoxMenuItem mncStatusBar;
    private javax.swing.JMenuItem mniAboutNotepad;
    private javax.swing.JMenuItem mniChangeFont;
    private javax.swing.JMenuItem mniCopy;
    private javax.swing.JMenuItem mniCut;
    private javax.swing.JMenuItem mniDelete;
    private javax.swing.JMenuItem mniExit;
    private javax.swing.JRadioButtonMenuItem mniFind;
    private javax.swing.JMenuItem mniNew;
    private javax.swing.JMenuItem mniOpen;
    private javax.swing.JMenuItem mniPaste;
    private javax.swing.JMenuItem mniPrint;
    private javax.swing.JMenuItem mniRedo;
    private javax.swing.JMenuItem mniResetFont;
    private javax.swing.JMenuItem mniSave;
    private javax.swing.JMenuItem mniSaveAs;
    private javax.swing.JMenuItem mniSelectAll;
    private javax.swing.JMenuItem mniSendFeedback;
    private javax.swing.JMenuItem mniTimeDate;
    private javax.swing.JMenuItem mniUndo;
    private javax.swing.JMenuItem mniViewHelp;
    private javax.swing.JMenuItem mniZoom;
    private javax.swing.JTextArea txtTextArea;
    // End of variables declaration//GEN-END:variables
}
