/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testmail;

import java.util.Date;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Admin
 */
public class TestMail {

    //Ref: https://www.youtube.com/c/NguyenVietKhoa/playlists
    public static void sendMail() throws AddressException, MessagingException {
        Properties pro = System.getProperties();
        pro.put("mail.smtp.host", "smtp.gmail.com");
        pro.put("mail.smtp.auth", "true");
        pro.put("mail.smtp.port", "465");
        pro.put("mail.smtp.socketFactory.class", javax.net.ssl.SSLSocketFactory.class.getName());

        Session session = Session.getDefaultInstance(pro, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("thcbailiet@gmail.com", "daiviet4");
            }
        });

        Message message = new MimeMessage(session);//Remember add java mail api .jar file
        message.addRecipient(Message.RecipientType.TO, new InternetAddress("nhatlqse140256@fpt.edu.vn"));
        //message.addRecipient(Message.RecipientType.CC, new InternetAddress("nhatlqse140256@fpt.edu.vn"));
        message.setSubject("Test mail - " + new Date());//title
        String content = "Mail from thcbailiet@gmail.com.. "
                + "Học chơi lmht";//content
        
        
        message.setContent(content, "text/html; charset=utf-8");//tiếng việt https://stackoverflow.com/questions/5028670/how-to-set-mimebodypart-contenttype-to-text-html

        Transport.send(message);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            sendMail();
            System.out.println("Success");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
