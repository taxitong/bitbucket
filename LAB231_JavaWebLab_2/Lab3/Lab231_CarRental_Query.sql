CREATE DATABASE Lab231_CarRental_DB
USE Lab231_CarRental_DB
CREATE TABLE tblStatus (
	statusID VARCHAR(40) PRIMARY KEY,
	statusName VARCHAR(40) NOT NULL,
);
CREATE TABLE tblAccountRole (
	roleID VARCHAR(40) PRIMARY KEY,
	roleName VARCHAR(40) NOT NULL,
);
CREATE TABLE tblAccount (
	email NVARCHAR(254) PRIMARY KEY,
	pword CHAR(64) NOT NULL,
	displayName NVARCHAR(50) NOT NULL,
	createDate Date NOT NULL,
	activeCode CHAR(6) NOT NULL,

	phoneNo varchar(15),
	addr NVARCHAR(100),
	--
	roleID VARCHAR(40) NOT NULL,
	statusID VARCHAR(40) NOT NULL,
	--
	FOREIGN KEY(roleID) REFERENCES dbo.tblAccountRole(roleID),
	FOREIGN KEY(statusID) REFERENCES dbo.tblStatus(statusID),
)
CREATE TABLE tblCategory (
	categoryID VARCHAR(40) PRIMARY KEY,
	categoryName VARCHAR(40),
);
CREATE TABLE tblType (
	typeID VARCHAR(40) PRIMARY KEY,
	typeName VARCHAR(40),
);
CREATE TABLE tblCar (
	carID VARCHAR(20) PRIMARY KEY,
	price DECIMAL(18, 2),
	img VARCHAR(200),
	--
	categoryID VARCHAR(40),
	typeID VARCHAR(40),
	statusID VARCHAR(40),
	--
	FOREIGN KEY(categoryID) REFERENCES dbo.tblCategory(categoryID),
	FOREIGN KEY(typeID) REFERENCES dbo.tblType(typeID),
	FOREIGN KEY(statusID) REFERENCES dbo.tblStatus(statusID),
)
CREATE TABLE tblOrderDetails (
	orderDetailsID CHAR(64) PRIMARY KEY,
	orderID CHAR(64),
	--
	carID VARCHAR(20),
	price_Save DECIMAL(18, 2),
	rentalDate DATE,
	returnDate DATE,
	--
	FOREIGN KEY(orderID) REFERENCES dbo.tblOrder(orderID),
	FOREIGN KEY(carID) REFERENCES dbo.tblCar(carID),
);
CREATE TABLE tblOrder (
	orderID CHAR(64) PRIMARY KEY,
	total DECIMAL(18, 2),
	dateCreate DATE,
	--
	statusID VARCHAR(40),
	--
	email NVARCHAR(254),
	displayName_Save NVARCHAR(50),
	phoneNo_Save varchar(15),
	addr_Save NVARCHAR(100),
	--
	FOREIGN KEY(statusID) REFERENCES dbo.tblStatus(statusID),
	FOREIGN KEY(email) REFERENCES dbo.tblAccount(email),
);
CREATE TABLE tblOrderDiscount (
	discountID NVARCHAR(20) PRIMARY KEY,
	orderID CHAR(64),
	percentDiscount INT NOT NULL,
	expirationDate DATE,
	FOREIGN KEY(orderID) REFERENCES dbo.tblOrder(orderID),
)
--TEST1
USE Test1
CREATE TABLE tblCar(
	carID VARCHAR(20) PRIMARY KEY,
	carName NVARCHAR(50),
)
CREATE TABLE tblRental(
	rentalID VARCHAR(20) PRIMARY KEY,
	rentalDate DATE,
	returnDate DATE,
	carID VARCHAR(20),
	FOREIGN KEY(carID) REFERENCES dbo.tblCar(carID),
)