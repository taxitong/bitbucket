<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="content">
    <jsp:include page="appHelloUs.jsp"/>
    <div class="main-text">
        <P class="title">
            Tracking Page
        </p>
        <div class="each-main-text">
            <P class="title">Search Order By: </p>
            <form action="ViewHistory">
                Date:
                <select name="tDateCreate_toViewHistory">
                    <option value="">All</option>
                    <c:forEach var="dateRange" items="${sessionScope.LIST_ALL_DATE_ORDER}">
                        <option value="${dateRange}" <c:if test="${sessionScope.tDateCreate_toViewHistory == dateRange}">selected="true"</c:if>>
                            ${dateRange}
                        </option>
                    </c:forEach>
                </select>
                Receiver: <input type="text" name="tDisplayName_Save_toViewHistory" value="${sessionScope.tDisplayName_Save_toViewHistory}" />
                <input class="controlButton" type="submit" value="Search"/></br>
            </form>
        </div>
        <c:if test="${sessionScope.LIST_O != null}">
            <c:if test="${not empty sessionScope.LIST_O}">
                <c:forEach var="order" items="${sessionScope.LIST_O}">
                    <div class="each-main-text">
                        <p class="hightlight">
                            ID: ${order.orderID}
                        </p></br>
                        Total: ${order.total}</br>
                        Date Create: ${order.dateCreate}</br>
                        Status: 
                        <p class="hightlight" style="display: inline;">
                            <c:if test="${sessionScope.suspendStatus_fromViewHistory == order.statusID}">
                                Cancel
                            </c:if>
                            <c:if test="${sessionScope.suspendStatus_fromViewHistory != order.statusID}">
                                Pending or Complete
                            </c:if>
                        </p></br></br>
                        Receiver: ${order.displayName_Save}</br>
                        Phone: ${order.phoneNo_Save}</br>
                        </br>
                        <c:if test="${sessionScope.suspendStatus_fromViewHistory != order.statusID}">
                            <form action="DeleteOrder" method="POST">
                                <input type="hidden" name="tOrderID_toDeleteOrder" value="${order.orderID}"/>
                                <input class="controlButton" type="submit" value="Delete"  onclick="return confirm('Are you sure you want to Delete?')"/ >
                            </form>
                        </c:if>
                    </div>
                </c:forEach>
                <c:forEach begin="1" end="${endPageO}" var="i">
                    <a style="color: black;" id="${i}" href="ViewHistory?indexO=${i}&tDisplayName_Save_toViewHistory=${sessionScope.tDisplayName_Save_toViewHistory}&tDateCreate_toViewHistory=${sessionScope.tDateCreate_toViewHistory}">${i}</a>
                </c:forEach>
                <script>
                    document.getElementById('${indexO}').style.background = "#ff2562";
                    document.getElementById('${indexO}').style.color = "#FFFFFF";
                </script>
            </c:if>
            <c:if test="${empty sessionScope.LIST_O}">
                <jsp:include page="usMain_SearchNoO.jsp"/>
            </c:if>
        </c:if>
    </div>
</div>