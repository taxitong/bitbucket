<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="content">
    <jsp:include page="appHelloUs.jsp"/>
    <div class="main-text">
        <P class="title">
            Cart Page
        </p>
        <c:if test="${sessionScope.CART != null}">
            <c:if test="${not empty sessionScope.CART}">
                <c:forEach var="cart" varStatus="counter" items="${sessionScope.CART.cart}">
                    <div class="each-main-text">
                        <p class="hightlight" style="display: inline;">LP: ${cart.value.carID}</p> | 
                        <c:forEach var="category" items="${sessionScope.LIST_CATEGORY}">
                            <c:if test="${cart.value.categoryID == category.categoryID}">${category.categoryName} |</c:if>
                        </c:forEach>
                        <c:forEach var="type" items="${sessionScope.LIST_TYPE}">
                            <c:if test="${cart.value.typeID == type.typeID}">${type.typeName} |</c:if>
                        </c:forEach>
                        ${cart.value.startDate} - ${cart.value.endDate} |
                        ${cart.value.dateDifference} day(s) * ${cart.value.price}$ =
                        <p class="hightlight" style="display: inline;">${cart.value.price * cart.value.dateDifference}$</p>
                        <form action="RemoveFromCart" style="display: inline;" method="POST">
                            <input type="hidden" name="tcarID_toRemoveFromCart" value="${cart.value.carID}"/>
                            <input type="submit" class="controlButton" value="Remove" onclick="return confirm('Are you sure you want to Remove?')"/>
                        </form>
                        </br>
                        <c:set var="totalOrderDetails" value="${cart.value.price * cart.value.dateDifference}"/>
                    </div>
                    <c:set var="totalOrderBase" value="${totalOrderBase + totalOrderDetails}"/>
                </c:forEach>
                <form action="CheckDiscount" method="POST">
                    <input type="hidden" id="todayDate1" name="tToday_toCheckDiscount"/>
                    Discount: 
                    <p class="hightlight" style="display: inline;">
                        <c:if test="${sessionScope.discountNotValid != null}">
                            ${sessionScope.discountNotValid}
                        </c:if>
                        <c:if test="${sessionScope.percentDiscount_fromCheckDiscount != null}">
                            Reduce: ${sessionScope.percentDiscount_fromCheckDiscount}%
                        </c:if>
                    </p>
                    </br>
                    <input class="custom-text-discount" type="text" name="tDiscountID_to_fromCheckDiscount" value="${sessionScope.tDiscountID_to_fromCheckDiscount}"/>
                    <input type="submit" class="controlButton" value="Check"/>
                </form>
                <P class="title">
                    Customer's Info:
                </p>
                <form action="CreateOrder" method="POST">
                    <c:if test="${sessionScope.percentDiscount_fromCheckDiscount != null}">
                        <input type="hidden" name="tDiscountID_toCreateOrder" value="${tDiscountID_to_fromCheckDiscount}" />
                    </c:if>
                    Name:
                    <input class="custom-text-input" type="text" name="tDisplayName_Save_toCreateOrder" minlength="2" maxlength="100" required value="${sessionScope.LOGIN_ACCOUNT.displayName}"/></br>
                    Phone: 
                    <input class="custom-text-input" type="text" name="tPhoneNo_Save_toCreateOrder" minlength="10" maxlength="11" required value="${sessionScope.LOGIN_ACCOUNT.phoneNo}"/></br>
                    Address: 
                    <input class="custom-text-input" type="text" name="tAddr_Save_toCreateOrder" minlength="2" maxlength="100" required value="${sessionScope.LOGIN_ACCOUNT.addr}"/></br>
                    <fmt:formatNumber var="totalReducedOrder" type="number" maxFractionDigits="2" value="${totalOrderBase - totalOrderBase * sessionScope.percentDiscount_fromCheckDiscount/100}"/>
                    Payment method: Direct payment at the store </br></br>
                    Order's Total: ${totalReducedOrder}$ </br></br>
                    <input type="hidden" id="todayDate2" name="tToday_toCreateOrder"/>
                    <input type="hidden" name="tTotalReducedOrder_toCreateOrder" value="${totalReducedOrder}"/>
                    <input type="submit" class="controlButton" value="Create Order"/>
                </form>
            </c:if>
        </c:if>
        <c:if test="${sessionScope.CART == null}">
            You have not selected any car(s) yet</br></br>
        </c:if>
    </div>
</div>