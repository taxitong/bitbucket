<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="each-main-text">
    <form action="SearchC" class="subject-text" method="POST">
        <P class="title">Search Car By: </p>
        LP: <input type="text" name="tCarID_toSearchC" size="10" style="text-transform: uppercase;" value="${sessionScope.tCarID_toSearchC}"/>
        Category: 
        <select name="tCategoryID_toSearchC" style="text-transform: capitalize;">
            <option value="">All</option>
            <c:forEach var="category" items="${sessionScope.LIST_CATEGORY}">
                <option value="${category.categoryID}" <c:if test="${sessionScope.tCategoryID_toSearchC == category.categoryID}">selected="true"</c:if>>
                    ${category.categoryName}
                </option>
            </c:forEach>
        </select>
        Status: 
        <select name="tStatusID_toSearchC"style="text-transform: capitalize;"">
            <option value="">All</option>
            <c:forEach var="status" varStatus="counter" items="${sessionScope.LIST_STATUS}">
                <option value="${status.statusID}" <c:if test="${sessionScope.tStatusID_toSearchC == status.statusID}">selected=</c:if>>
                    ${status.statusName}
                </option>
            </c:forEach>
        </select>
        Type: 
        <select name="tTypeID_toSearchC" style="text-transform: capitalize;">
            <option value="">All</option>
            <c:forEach var="type" items="${sessionScope.LIST_TYPE}">
                <option value="${type.typeID}" <c:if test="${sessionScope.tTypeID_toSearchC == type.typeID}">selected="true"</c:if>>
                    ${type.typeName}
                </option>
            </c:forEach>
        </select>
        <!--Car's status:--><input type="hidden" name="tStatusID_toSearchC" value="" />
        Available: <input type="date" id="startDate" name="tStartDate_toSearchC" required value="${sessionScope.tStartDate_toSearchC}">
        To: <input type="date" id="endDate" name="tEndDate_toSearchC" required value="${sessionScope.tEndDate_toSearchC}">
        <input class="controlButton" type="submit" value="Search"/></br>
        <!--<div id="currentDate"></div>-->
    </form>
</div>