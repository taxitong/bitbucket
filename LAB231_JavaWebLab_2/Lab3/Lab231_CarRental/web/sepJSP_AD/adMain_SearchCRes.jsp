<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${sessionScope.LIST_C != null}">
    <c:if test="${not empty sessionScope.LIST_C}">
        <div class="main-text-layer">
            <c:forEach var="car" items="${sessionScope.LIST_C}">
                <div class="each-main-text">
                    <p class="hightlight">
                        LP: ${car.carID} 
                    </p>
                    Price: ${car.price}$ / day</br>
                    Category: 
                    <c:forEach var="category" items="${sessionScope.LIST_CATEGORY}">
                        <c:if test="${car.categoryID == category.categoryID}">${category.categoryName}</c:if>
                    </c:forEach></br>
                    Type: 
                    <c:forEach var="type" items="${sessionScope.LIST_TYPE}">
                        <c:if test="${car.typeID == type.typeID}">${type.typeName}</c:if>
                    </c:forEach></br>
                    <p class="hightlight" style="text-transform: capitalize;">
                        Status: ${car.statusID}
                    </p>
                    </br>
                    <img class="car-img" src="${car.img}" alt=""/></br></br>
                </div>
            </c:forEach>
        </div>
        <c:forEach begin="1" end="${endPageC}" var="i">
            <a style="color: black;" id="${i}" href="SearchC?tCarID_toSearchC=${tCarID_toSearchC}&tCategoryID_toSearchC=${tCategoryID_toSearchC}&tTypeID_toSearchC=${tTypeID_toSearchC}&tStatusID_toSearchC=${tStatusID_toSearchC}&tStartDate_toSearchC=${tStartDate_toSearchC}&tEndDate_toSearchC=${tEndDate_toSearchC}&indexC=${i}">${i}</a>
        </c:forEach>
        <script>
            document.getElementById('${indexC}').style.background = "#ff2562";
            document.getElementById('${indexC}').style.color = "#FFFFFF";
        </script>
    </c:if>
    <c:if test="${empty sessionScope.LIST_C}">
        <jsp:include page="adMain_SearchNoC.jsp"/>
    </c:if>
</c:if>
