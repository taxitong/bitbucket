<%-- 
    Document   : Us
    Created on : Mar 18, 2021, 3:04:26 PM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="sharedJSP/appMainCSS.jsp"/>
        <title>Main Us</title>
    </head>
    <body>
        <section>
            <jsp:include page="sepJSP_US/appMenuBarUs.jsp"/>
            <jsp:include page="sepJSP_US/usMain_SearchC.jsp"/>
            <jsp:include page="sharedJSP/appSciAndTopButton.jsp"/>
        </section>
        <span class="menuicon" id="toggle" onclick="menuToggle()"></span>
        <script src="js/main.js" type="text/javascript"></script>
        <script src="js/date-main.js" type="text/javascript"></script>
    </body>
</html>
