<%-- 
    Document   : AdMain
    Created on : Mar 18, 2021, 2:52:34 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="sharedJSP/appMainCSS.jsp"/>
        <title>Main Ad</title>
    </head>
    <body>
        <section>
            <jsp:include page="sepJSP_AD/appMenuBarAd.jsp"/>
            <jsp:include page="sepJSP_AD/adMain_SearchC.jsp"/>
            <jsp:include page="sharedJSP/appSciAndTopButton.jsp"/>
        </section>
        <span class="menuicon" id="toggle" onclick="menuToggle()"></span>
        <script src="js/main.js" type="text/javascript"></script>
        <script src="js/date-main.js" type="text/javascript"></script>
    </body>
</html>
