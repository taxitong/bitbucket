//move between sign up/sign in
const signUpButton = document.getElementById('signUp');
const signInButton = document.getElementById('signIn');
const container = document.getElementById('container');

signUpButton.addEventListener('click', () => {
    container.classList.add("right-panel-active");
});

signInButton.addEventListener('click', () => {
    container.classList.remove("right-panel-active");
});
//check match password
function validateForm() {//https://www.javatpoint.com/confirm-password-validation-in-javascript
    //collect form data in JavaScript variables
    var pw1 = document.getElementById("pswd1").value;
    var pw2 = document.getElementById("pswd2").value;
    if (pw1 !== pw2) {
        document.getElementById("passMess").innerHTML = "(*)2 Passwords are not same";
        return false;
    }
}
//recaptcha
function recaptchaCallback() {
    document.getElementById("signInButton").disabled = false;
    document.getElementById("signInButton").style.opacity = "1";
}
