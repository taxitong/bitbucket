//menu bar when resize
function menuToggle() {
    var nav = document.getElementById("nav");
    nav.classList.toggle('active');
    var toggle = document.getElementById("toggle");
    toggle.classList.toggle('active');
}
//back to top button
window.onscroll = function () {// When the user scrolls down 20px from the top of the document, show the button
    scrollFunction();
};
function scrollFunction() {
    var mybutton = document.getElementById("myBtn");//Get the button
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        mybutton.style.display = "block";
    } else {
        mybutton.style.display = "none";
    }
}
function topFunction() {// When the user clicks on the button, scroll to the top of the document
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}
