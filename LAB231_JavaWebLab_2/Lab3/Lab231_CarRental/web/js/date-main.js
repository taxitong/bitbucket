//disable dates before today (but we don't use getElementsByName: https://stackoverflow.com/questions/38638424/html5-input-type-date-disable-dates-before-today
var today = new Date().toISOString().split('T')[0];
document.getElementById("startDate").setAttribute('min', today);
document.getElementById("endDate").setAttribute('min', today);
///*test*/alert("New: " + new Date() + "\n" + "Today: " + today + "\n" + "ISO: " + new Date().toISOString());
//UTC time^
