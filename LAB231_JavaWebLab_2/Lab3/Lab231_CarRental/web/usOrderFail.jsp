<%-- 
    Document   : usOrderFail
    Created on : Mar 26, 2021, 3:53:30 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="sharedJSP/appMainCSS.jsp"/>
        <title>Order Fail</title>
    </head>
    <body>
        <section>
            <jsp:include page="sepJSP_US/appMenuBarUs.jsp"/>
            <div class="content">
                <jsp:include page="sepJSP_US/appHelloUs.jsp"/>
                <div class="main-text">
                    <P class="title" style="color: red;">
                        Order Fail
                    </p>
                    Some of car(s) are already rented before you can make the order. Please choose again.
                </div>
            </div>
            <jsp:include page="sharedJSP/appSciAndTopButton.jsp"/>
        </section>
        <span class="menuicon" id="toggle" onclick="menuToggle()"></span>
        <script src="js/main.js" type="text/javascript"></script>
    </body>
</html>
