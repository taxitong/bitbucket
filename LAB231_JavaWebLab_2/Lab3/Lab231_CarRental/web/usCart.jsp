<%-- 
    Document   : usCart
    Created on : Mar 24, 2021, 12:02:44 AM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="sharedJSP/appMainCSS.jsp"/>
        <title>Cart</title>
    </head>
    <body>
        <section>
            <jsp:include page="sepJSP_US/appMenuBarUs.jsp"/>
            <jsp:include page="sepJSP_US/usCart_CartMain.jsp"/>
            <jsp:include page="sharedJSP/appSciAndTopButton.jsp"/>
        </section>
        <span class="menuicon" id="toggle" onclick="menuToggle()"></span>
        <script src="js/main.js" type="text/javascript"></script>
        <script src="js/date-us-cart.js" type="text/javascript"></script>
    </body>
</html>
