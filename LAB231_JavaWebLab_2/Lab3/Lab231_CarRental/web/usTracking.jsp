<%-- 
    Document   : tracking
    Created on : Mar 23, 2021, 11:15:56 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="sharedJSP/appMainCSS.jsp"/>
        <title>Tracking Page</title>
    </head>
    <body>
        <section>
            <jsp:include page="sepJSP_US/appMenuBarUs.jsp"/>
            <jsp:include page="sepJSP_US/usTracking_OrderHistory.jsp"/>
            <jsp:include page="sharedJSP/appSciAndTopButton.jsp"/>
        </section>
        <span class="menuicon" id="toggle" onclick="menuToggle()"></span>
        <script src="js/main.js" type="text/javascript"></script>
        <script src="js/date-us-main.js" type="text/javascript"></script>
    </body>
</html>
