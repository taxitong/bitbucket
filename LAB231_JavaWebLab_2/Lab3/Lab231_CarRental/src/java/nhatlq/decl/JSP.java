/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.decl;

/**
 *
 * @author Admin
 */
public class JSP {

    /*FrontEnd_Pages*/
    public static final String AD_MAIN = "adMain.jsp";
    public static final String ERROR = "error.jsp";
    public static final String START_PAGE = "startPage.jsp";
    public static final String US_CART = "usCart.jsp";
    public static final String US_MAIN = "usMain.jsp";
    public static final String US_ORDER_FAIL = "usOrderFail.jsp";
    public static final String US_TRACKING = "usTracking.jsp";
}
