/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.dtos;

import java.io.Serializable;

/**
 *
 * @author Admin
 */
public class OrderDiscountDTO implements Serializable {

    private String discountID;
    private String percentDiscount;
    private String orderID;

    public OrderDiscountDTO() {
    }

    public OrderDiscountDTO(String discountID, String percentDiscount, String orderID) {
        this.discountID = discountID;
        this.percentDiscount = percentDiscount;
        this.orderID = orderID;
    }

    public String getDiscountID() {
        return discountID;
    }

    public void setDiscountID(String discountID) {
        this.discountID = discountID;
    }

    public String getPercentDiscount() {
        return percentDiscount;
    }

    public void setPercentDiscount(String percentDiscount) {
        this.percentDiscount = percentDiscount;
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

}
