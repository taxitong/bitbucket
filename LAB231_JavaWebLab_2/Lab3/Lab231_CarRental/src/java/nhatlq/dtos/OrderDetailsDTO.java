/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.dtos;

import java.io.Serializable;

/**
 *
 * @author Admin
 */
public class OrderDetailsDTO implements Serializable {

    private String orderDetailsID;
    private String orderID;

    private String carID;
    private double price_Save;
    private String rentalDate;
    private String returnDate;

    public OrderDetailsDTO() {
    }

    public OrderDetailsDTO(String orderDetailsID, String orderID, String carID, double price_Save, String rentalDate, String returnDate) {
        this.orderDetailsID = orderDetailsID;
        this.orderID = orderID;
        this.carID = carID;
        this.price_Save = price_Save;
        this.rentalDate = rentalDate;
        this.returnDate = returnDate;
    }

    public String getOrderDetailsID() {
        return orderDetailsID;
    }

    public void setOrderDetailsID(String orderDetailsID) {
        this.orderDetailsID = orderDetailsID;
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public String getCarID() {
        return carID;
    }

    public void setCarID(String carID) {
        this.carID = carID;
    }

    public double getPrice_Save() {
        return price_Save;
    }

    public void setPrice_Save(double price_Save) {
        this.price_Save = price_Save;
    }

    public String getRentalDate() {
        return rentalDate;
    }

    public void setRentalDate(String rentalDate) {
        this.rentalDate = rentalDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }
}
