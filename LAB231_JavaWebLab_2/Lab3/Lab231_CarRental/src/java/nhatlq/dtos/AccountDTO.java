/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.dtos;

import java.io.Serializable;

/**
 *
 * @author Admin
 */
public class AccountDTO implements Serializable {

    private String email;
    private String pword;
    private String displayName;
    private String createDate;
    private String activeCode;

    private String phoneNo;
    private String addr;

    private String roleID;
    private String statusID;

    public AccountDTO() {
    }

    public AccountDTO(String email, String displayName, String createDate, String activeCode, String phoneNo, String addr, String roleID, String statusID) {
        this.email = email;
        this.displayName = displayName;
        this.createDate = createDate;
        this.activeCode = activeCode;
        this.phoneNo = phoneNo;
        this.addr = addr;
        this.roleID = roleID;
        this.statusID = statusID;
    }
    
    //for login
    public AccountDTO(String email, String displayName, String createDate, String phoneNo, String addr, String roleID, String statusID) {
        this.email = email;
        this.displayName = displayName;
        this.createDate = createDate;
        this.phoneNo = phoneNo;
        this.addr = addr;
        this.roleID = roleID;
        this.statusID = statusID;
    }
    
    

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPword() {
        return pword;
    }

    public void setPword(String pword) {
        this.pword = pword;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getActiveCode() {
        return activeCode;
    }

    public void setActiveCode(String activeCode) {
        this.activeCode = activeCode;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getRoleID() {
        return roleID;
    }

    public void setRoleID(String roleID) {
        this.roleID = roleID;
    }

    public String getStatusID() {
        return statusID;
    }

    public void setStatusID(String statusID) {
        this.statusID = statusID;
    }

}
