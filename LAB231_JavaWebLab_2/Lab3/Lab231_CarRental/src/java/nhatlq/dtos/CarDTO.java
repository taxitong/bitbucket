/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.dtos;

import java.io.Serializable;

/**
 *
 * @author Admin
 */
public class CarDTO implements Serializable {

    private String carID;
    private double price;
    private String img;

    private String categoryID;
    private String typeID;
    private String statusID;

    private String startDate;
    private String endDate;
    private int dateDifference;

    public CarDTO() {
    }

    public CarDTO(String carID, double price, String img, String categoryID, String typeID, String statusID) {
        this.carID = carID;
        this.price = price;
        this.img = img;
        this.categoryID = categoryID;
        this.typeID = typeID;
        this.statusID = statusID;
    }

    /*for store date within cart*/
    public CarDTO(String carID, double price, String img, String categoryID, String typeID, String statusID, String startDate, String endDate, int dateDifference) {
        this.carID = carID;
        this.price = price;
        this.img = img;
        this.categoryID = categoryID;
        this.typeID = typeID;
        this.statusID = statusID;
        this.startDate = startDate;
        this.endDate = endDate;
        this.dateDifference = dateDifference;
    }

    public String getCarID() {
        return carID;
    }

    public void setCarID(String carID) {
        this.carID = carID;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(String categoryID) {
        this.categoryID = categoryID;
    }

    public String getTypeID() {
        return typeID;
    }

    public void setTypeID(String typeID) {
        this.typeID = typeID;
    }

    public String getStatusID() {
        return statusID;
    }

    public void setStatusID(String statusID) {
        this.statusID = statusID;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getDateDifference() {
        return dateDifference;
    }

    public void setDateDifference(int dateDifference) {
        this.dateDifference = dateDifference;
    }

}
