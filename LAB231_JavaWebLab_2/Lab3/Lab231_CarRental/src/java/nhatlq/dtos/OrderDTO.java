/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.dtos;

import java.io.Serializable;

/**
 *
 * @author Admin
 */
public class OrderDTO implements Serializable {

    private String orderID;
    private double total;
    private String dateCreate;
    
    private String statusID;
    
    private String email;
    private String displayName_Save;
    private String phoneNo_Save;
    private String addr_Save;

    public OrderDTO() {
    }

    public OrderDTO(String orderID, double total, String dateCreate, String statusID, String email, String displayName_Save, String phoneNo_Save, String addr_Save) {
        this.orderID = orderID;
        this.total = total;
        this.dateCreate = dateCreate;
        this.statusID = statusID;
        this.email = email;
        this.displayName_Save = displayName_Save;
        this.phoneNo_Save = phoneNo_Save;
        this.addr_Save = addr_Save;
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(String dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getStatusID() {
        return statusID;
    }

    public void setStatusID(String statusID) {
        this.statusID = statusID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDisplayName_Save() {
        return displayName_Save;
    }

    public void setDisplayName_Save(String displayName_Save) {
        this.displayName_Save = displayName_Save;
    }

    public String getPhoneNo_Save() {
        return phoneNo_Save;
    }

    public void setPhoneNo_Save(String phoneNo_Save) {
        this.phoneNo_Save = phoneNo_Save;
    }

    public String getAddr_Save() {
        return addr_Save;
    }

    public void setAddr_Save(String addr_Save) {
        this.addr_Save = addr_Save;
    }

}
