/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.dtos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Admin
 */
public class CartDTO implements Serializable {

    private Map<String, CarDTO> cart;

    public CartDTO() {
    }

    public CartDTO(Map<String, CarDTO> cart) {
        this.cart = cart;
    }

    public Map<String, CarDTO> getCart() {
        return cart;
    }

    public void setCart(Map<String, CarDTO> cart) {
        this.cart = cart;
    }

    public void add(CarDTO car) {
        if (cart == null) {
            cart = new HashMap<>();
        }
        cart.put(car.getCarID(), car);
    }

    public void delete(String id) {
        if (cart == null) {
            return;
        }
        if (cart.containsKey(id)) {
            cart.remove(id);
        }
    }

    public void update(String id, CarDTO car) {
        if (cart != null) {
            if (cart.containsKey(id)) {
                cart.replace(id, car);
            }
        }
    }

    public boolean isCartEmpty() {
        return cart.isEmpty();
    }

}
