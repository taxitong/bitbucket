/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.naming.NamingException;
import nhatlq.decl.DB;
import nhatlq.dtos.OrderDTO;
import nhatlq.utils.DBConnection;

/**
 *
 * @author Admin
 */
public class OrderDAO {

    private Connection conn;
    private PreparedStatement preStm;
    private ResultSet rs;
    //attributes in the database 
    private static final String STATUS_SUSPEND = DB.STATUS_SUSPEND;

    public OrderDAO() {
        conn = null;
        preStm = null;
        rs = null;
    }

    private void closeConnection3() throws SQLException {
        if (conn != null) {
            conn.close();
        }
        if (preStm != null) {
            preStm.close();
        }
        if (rs != null) {
            rs.close();
        }
    }

    private void closeConnection2() throws SQLException {
        if (conn != null) {
            conn.close();
        }
        if (preStm != null) {
            preStm.close();
        }
    }

    public boolean insertOder(OrderDTO order) throws SQLException, NamingException {
        boolean result = false;
        try {
            conn = DBConnection.getConnection();
            String sql = "INSERT INTO tblOrder(orderID, total, dateCreate, statusID, email, displayName_Save, phoneNo_Save, addr_Save) VALUES(?,?,?,?,?,?,?,?)";
            preStm = conn.prepareStatement(sql);
            preStm.setString(1, order.getOrderID());
            preStm.setDouble(2, order.getTotal());
            preStm.setString(3, order.getDateCreate());

            preStm.setString(4, order.getStatusID());

            preStm.setString(5, order.getEmail());
            preStm.setString(6, order.getDisplayName_Save());
            preStm.setString(7, order.getPhoneNo_Save());
            preStm.setString(8, order.getAddr_Save());
            result = preStm.executeUpdate() > 0;
        } finally {
            closeConnection2();
        }
        return result;
    }

    public boolean deleteOder(String orderID) throws SQLException, NamingException {
        boolean result = false;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "DELETE FROM tblOrder WHERE orderID=?";
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, orderID);
                result = preStm.executeUpdate() > 0;
            }
        } finally {
            closeConnection2();
        }
        return result;
    }

    private String countOrder_GenerateSQL(String orderDate) {
        String condition0 = "email=? ";
        String condition1 = "displayName_Save LIKE ? ";
        String condition2 = orderDate.isEmpty() ? "dateCreate LIKE ? " : "dateCreate=? ";
        String allConditions = condition0 + "AND " + condition1 + "AND " + condition2;
        String sql = "SELECT COUNT(orderID) FROM tblOrder WHERE " + allConditions;
        return sql;
    }

    public int countOrder(String email, String displayName_Save, String orderDate) throws NamingException, SQLException {
        int result = 0;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = countOrder_GenerateSQL(orderDate);
                //System.out.println("countOrder " + sql);
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, email);
                preStm.setString(2, "%" + displayName_Save + "%");
                preStm.setString(3, orderDate.isEmpty() ? "%%" : orderDate);
                rs = preStm.executeQuery();
                if (rs.next()) {
                    result = rs.getInt(1);
                }
            }
        } finally {
            closeConnection3();
        }
        return result;
    }

    private String getAllOrder_GenerateSQL(String orderDate, int size) {
        String condition0 = "email=? ";
        String condition1 = "displayName_Save LIKE ? ";
        String condition2 = orderDate.isEmpty() ? "dateCreate LIKE ? " : "dateCreate=? ";
        String WHERE_BETWEEN = "WHERE r BETWEEN ?*x-(x-1) AND ?*x".replace("x", size + "");
        String allConditions = condition0 + "AND " + condition1 + "AND " + condition2;

        String sql = "SELECT "
                + "orderID, total, dateCreate, statusID, displayName_Save, phoneNo_Save, addr_Save "
                + "FROM (SELECT ROW_NUMBER() OVER (ORDER BY dateCreate DESC) AS r, "
                + "orderID, total, dateCreate, statusID, displayName_Save, phoneNo_Save, addr_Save "
                + "FROM tblOrder WHERE "
                + allConditions
                + ") as x " + WHERE_BETWEEN;
        return sql;
    }

    public List<OrderDTO> getAllOrder(String email, String displayName_Save, String orderDate, int index_O, int size) throws NamingException, SQLException {
        List<OrderDTO> result = new ArrayList<>();
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = getAllOrder_GenerateSQL(orderDate, size);
                //System.out.println("getAllOrder " + sql);
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, email);
                preStm.setString(2, "%" + displayName_Save + "%");
                preStm.setString(3, orderDate.isEmpty() ? "%%" : orderDate);
                preStm.setInt(4, index_O);
                preStm.setInt(5, index_O);
                rs = preStm.executeQuery();
                while (rs.next()) {
                    String orderID = rs.getString("orderID");
                    double total = rs.getFloat("total");
                    String dateCreate = rs.getString("dateCreate");

                    String statusID = rs.getString("statusID");

                    //String email;
                    String displayName_SaveTmp = rs.getString("displayName_Save");
                    String phoneNo_Save = rs.getString("phoneNo_Save");
                    String addr_Save = rs.getString("addr_Save");
                    result.add(new OrderDTO(orderID, total, dateCreate, statusID, email, displayName_SaveTmp, phoneNo_Save, addr_Save));
                }
            }
        } finally {
            closeConnection3();
        }
        return result;
    }

    public List<String> getAllDateOrder(String email) throws SQLException, NamingException {
        List<String> result = new ArrayList<>();
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "SELECT dateCreate FROM tblOrder WHERE email=? GROUP BY dateCreate ORDER BY dateCreate DESC";
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, email);
                rs = preStm.executeQuery();
                while (rs.next()) {
                    String tmp = rs.getString("dateCreate");
                    result.add(tmp);
                }
            }
        } finally {
            closeConnection3();
        }
        return result;
    }

    public boolean deleteOrderAsUpdate(String orderID) throws NamingException, SQLException {
        boolean result = false;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "UPDATE tblOrder SET statusID=? WHERE orderID=?";
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, STATUS_SUSPEND);
                preStm.setString(2, orderID);
                result = preStm.executeUpdate() > 0;
            }
        } finally {
            closeConnection2();
        }
        return result;
    }
}
