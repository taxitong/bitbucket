/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.daos;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.naming.NamingException;
import nhatlq.dtos.AccountDTO;
import nhatlq.utils.DBConnection;
import nhatlq.decl.DB;
import nhatlq.utils.Encryption;

/**
 *
 * @author Admin
 */
public class AccountDAO {

    private Connection conn;
    private PreparedStatement preStm;
    private ResultSet rs;
    //attributes in the database 
    private static final String STATUS_ACTIVE = DB.STATUS_ACTIVE;

    public AccountDAO() {
        conn = null;
        preStm = null;
        rs = null;
    }

    public void closeConnection3() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (preStm != null) {
            preStm.close();
        }
        if (conn != null) {
            conn.close();
        }
    }

    public void closeConnection2() throws SQLException {
        if (preStm != null) {
            preStm.close();
        }
        if (conn != null) {
            conn.close();
        }
    }
    
    public AccountDTO checkSignIn(String email, String password) throws SQLException, NamingException, NoSuchAlgorithmException {
        AccountDTO accountDto = null;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "SELECT displayName, createDate, phoneNo, addr, roleID, statusID "
                        + "FROM tblAccount WHERE "
                        + "email=? AND pword=? AND statusID=?";
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, email);
                Encryption en = new Encryption();
                String newPassword = en.toHexString(en.getSHA(password));
                preStm.setString(2, newPassword);
                preStm.setString(3, STATUS_ACTIVE);
                rs = preStm.executeQuery();
                if (rs.next()) {
                    String displayName = rs.getString("displayName");
                    String createDate = rs.getString("createDate");
                    String phoneNo = rs.getString("phoneNo");
                    String addr = rs.getString("addr");
                    String roleID = rs.getString("roleID");
                    String statusID = rs.getString("statusID");
                    accountDto = new AccountDTO(email, displayName, createDate, phoneNo, addr, roleID, statusID);
                }
            }
        } finally {
            closeConnection3();
        }
        return accountDto;
    }
}
