/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.naming.NamingException;
import nhatlq.decl.DB;
import nhatlq.dtos.CarDTO;
import nhatlq.utils.DBConnection;

/**
 *
 * @author Admin
 */
public class CarDAO {

    private Connection conn;
    private PreparedStatement preStm;
    private ResultSet rs;
    //attributes in the database 
    private static final String STATUS_ACTIVE = DB.STATUS_ACTIVE;

    public CarDAO() {
        conn = null;
        preStm = null;
        rs = null;
    }

    private void closeConnection3() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (preStm != null) {
            preStm.close();
        }
        if (conn != null) {
            conn.close();
        }
    }

    String countCarAvailableInRange_GenerateSQL(String categoryID, String typeID, String statusID) {
        String condition1 = "c.carID LIKE ? ";
        String condition2 = categoryID.isEmpty() ? "c.categoryID LIKE ? " : "c.categoryID = ? ";
        String condition3 = typeID.isEmpty() ? "c.typeID LIKE ? " : "c.typeID = ? ";
        String condition4 = statusID.isEmpty() ? "c.statusID LIKE ? " : "c.statusID=? ";
        String allConditions = condition1 + "AND " + condition2 + "AND " + condition3 + "AND " + condition4;
        String sql = "SELECT count(c.carID) FROM tblCar AS c "
                + "LEFT JOIN (SELECT od.carID "
                + "FROM tblOrderDetails AS od "
                + "INNER JOIN tblOrder AS o "
                + "ON od.orderID = o.orderID "
                + "WHERE od.returnDate >= ? "
                + "AND od.rentalDate <= ? "
                + "AND o.statusID=? "
                + ") AS od ON c.carID = od.carID "
                + "WHERE od.carID IS NULL "
                + "AND " + allConditions;
        return sql;
    }

    public int countCarAvailableInRange(String carID, String categoryID, String typeID, String statusID, String startDate, String endDate) throws SQLException, NamingException {
        int result = 0;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = countCarAvailableInRange_GenerateSQL(categoryID, typeID, statusID);
                //System.out.println("countCarAvailableInRange: " + sql);
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, startDate);
                preStm.setString(2, endDate);
                preStm.setString(3, STATUS_ACTIVE);

                preStm.setString(4, "%" + carID + "%");
                preStm.setString(5, categoryID.isEmpty() ? "%%" : categoryID);
                preStm.setString(6, typeID.isEmpty() ? "%%" : typeID);
                preStm.setString(7, statusID.isEmpty() ? "%%" : statusID);
                rs = preStm.executeQuery();
                if (rs.next()) {
                    result = rs.getInt(1);
                }
            }
        } finally {
            closeConnection3();
        }
        return result;
    }

    String searchCarAvailableInRange_GenerateSQL(String categoryID, String typeID, String statusID, int size) {
        String condition1 = "c.carID LIKE ? ";
        String condition2 = categoryID.isEmpty() ? "c.categoryID LIKE ? " : "c.categoryID=? ";
        String condition3 = typeID.isEmpty() ? "c.typeID LIKE ? " : "c.typeID = ? ";
        String condition4 = statusID.isEmpty() ? "c.statusID LIKE ? " : "c.statusID=? ";
        String allConditions = condition1 + "AND " + condition2 + "AND " + condition3 + "AND " + condition4;
        String WHERE_BETWEEN = "WHERE r BETWEEN ?*x-(x-1) AND ?*x".replace("x", size + "");

        String sql = "SELECT "
                + "carID, price, img, categoryID, typeID, statusID "
                + "FROM (SELECT ROW_NUMBER() OVER (ORDER BY c.carID ASC) AS r, "
                + "c.carID, c.price, c.img, c.categoryID, c.typeID, c.statusID FROM tblCar AS c "
                + "LEFT JOIN (SELECT od.carID "
                + "FROM tblOrderDetails AS od "
                + "INNER JOIN tblOrder AS o "
                + "ON od.orderID = o.orderID "
                + "WHERE od.returnDate >= ? "
                + "AND od.rentalDate <= ? "
                + "AND o.statusID=? "
                + ") AS od ON c.carID = od.carID "
                + "WHERE od.carID IS NULL "
                + "AND " + allConditions
                + ") as x " + WHERE_BETWEEN;
        return sql;
    }

    public List<CarDTO> searchCarAvailableInRange(String carID, String categoryID, String typeID, String statusID, String startDate, String endDate, int indexC, int size) throws SQLException, NamingException {
        List<CarDTO> result = new ArrayList<>();
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = searchCarAvailableInRange_GenerateSQL(categoryID, typeID, statusID, size);
                //System.out.println("searchCarAvailableInRange " + sql);
                preStm = conn.prepareStatement(sql);
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, startDate);
                preStm.setString(2, endDate);
                preStm.setString(3, STATUS_ACTIVE);

                preStm.setString(4, "%" + carID + "%");
                preStm.setString(5, categoryID.isEmpty() ? "%%" : categoryID);
                preStm.setString(6, typeID.isEmpty() ? "%%" : typeID);
                preStm.setString(7, statusID.isEmpty() ? "%%" : statusID);
                preStm.setInt(8, indexC);
                preStm.setInt(9, indexC);
                rs = preStm.executeQuery();
                while (rs.next()) {
                    String carIDTmp = rs.getString("carID");
                    double price = rs.getDouble("price");
                    String img = rs.getString("img");

                    String categoryIDTmp = rs.getString("categoryID");
                    String typeIDTmp = rs.getString("typeID");
                    String statusIDTmp = rs.getString("statusID");
                    result.add(new CarDTO(carIDTmp, price, img, categoryIDTmp, typeIDTmp, statusIDTmp));
                }
            }
        } finally {
            closeConnection3();
        }
        return result;
    }

    public CarDTO getCar(String carID) throws NamingException, SQLException {
        CarDTO result = null;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "SELECT "
                        + "price, img, categoryID, typeID, statusID "
                        + "FROM tblCar WHERE carID=?";
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, carID);
                rs = preStm.executeQuery();
                if (rs.next()) {
                    //String carID;
                    double price = rs.getDouble("price");
                    String img = rs.getString("img");

                    String categoryID = rs.getString("categoryID");
                    String typeID = rs.getString("typeID");
                    String statusID = rs.getString("statusID");
                    result = new CarDTO(carID, price, img, categoryID, typeID, statusID);
                }
            }
        } finally {
            closeConnection3();
        }
        return result;
    }

    public boolean isCarQualifiedToCreateOrder(String startDate, String endDate, String carID) throws SQLException, NamingException {
        boolean result = false;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "SELECT c.carID FROM tblCar AS c "
                        + "LEFT JOIN (SELECT od.carID "
                        + "FROM tblOrderDetails AS od "
                        + "INNER JOIN tblOrder AS o "
                        + "ON od.orderID = o.orderID "
                        + "WHERE od.returnDate >= ? "
                        + "AND od.rentalDate <= ? "
                        + "AND o.statusID=? "
                        + ") AS mix ON c.carID = mix.carID "
                        + "WHERE mix.carID IS NULL "
                        + "AND c.carID=?";
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, startDate);
                preStm.setString(2, endDate);
                preStm.setString(3, STATUS_ACTIVE);
                preStm.setString(4, carID);
                rs = preStm.executeQuery();
                if (rs.next()) {//if it has any record -> qualified to be create
                    result = true;
                }
            }
        } finally {
            closeConnection3();
        }
        return result;
    }
}
