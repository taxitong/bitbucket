/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.naming.NamingException;
import nhatlq.dtos.CategoryDTO;
import nhatlq.utils.DBConnection;

/**
 *
 * @author Admin
 */
public class CategoryDAO {

    private Connection conn;
    private PreparedStatement preStm;
    private ResultSet rs;

    public CategoryDAO() {
        conn = null;
        preStm = null;
        rs = null;
    }

    public void closeConnection3() throws SQLException {
        if (conn != null) {
            conn.close();
        }
        if (preStm != null) {
            preStm.close();
        }
        if (rs != null) {
            rs.close();
        }
    }
    
    public List<CategoryDTO> searchAllCategory_NoPaging() throws SQLException, NamingException {
        List<CategoryDTO> result = new ArrayList<>();
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "SELECT categoryID, categoryName "
                        + "FROM tblCategory ORDER BY categoryID ASC";
                preStm = conn.prepareStatement(sql);
                rs = preStm.executeQuery();
                while (rs.next()) {
                    String categoryID = rs.getString("categoryID");
                    String categoryName = rs.getString("categoryName");
                    CategoryDTO categoryDto = new CategoryDTO(categoryID, categoryName);
                    result.add(categoryDto);
                }
            }
        } finally {
            closeConnection3();
        }
        return result;
    }
}
