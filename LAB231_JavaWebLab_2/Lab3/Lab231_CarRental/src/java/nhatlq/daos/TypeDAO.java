/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.naming.NamingException;
import nhatlq.dtos.TypeDTO;
import nhatlq.utils.DBConnection;

/**
 *
 * @author Admin
 */
public class TypeDAO {

    private Connection conn;
    private PreparedStatement preStm;
    private ResultSet rs;

    public TypeDAO() {
        conn = null;
        preStm = null;
        rs = null;
    }

    public void closeConnection3() throws SQLException {
        if (conn != null) {
            conn.close();
        }
        if (preStm != null) {
            preStm.close();
        }
        if (rs != null) {
            rs.close();
        }
    }

    public List<TypeDTO> searchAllType_NoPaging() throws SQLException, NamingException {
        List<TypeDTO> result = new ArrayList<>();
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "SELECT typeID, typeName "
                        + "FROM tbltype ORDER BY typeID ASC";
                preStm = conn.prepareStatement(sql);
                rs = preStm.executeQuery();
                while (rs.next()) {
                    String typeID = rs.getString("typeID");
                    String typeName = rs.getString("typeName");
                    TypeDTO typeDTO = new TypeDTO(typeID, typeName);
                    result.add(typeDTO);
                }
            }
        } finally {
            closeConnection3();
        }
        return result;
    }
}
