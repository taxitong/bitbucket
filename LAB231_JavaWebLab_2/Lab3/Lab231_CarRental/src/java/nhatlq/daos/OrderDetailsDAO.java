/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.naming.NamingException;
import nhatlq.dtos.OrderDetailsDTO;
import nhatlq.utils.DBConnection;

/**
 *
 * @author Admin
 */
public class OrderDetailsDAO {

    private Connection conn;
    private PreparedStatement preStm;
    private ResultSet rs;

    public OrderDetailsDAO() {
        conn = null;
        preStm = null;
        rs = null;
    }

    private void closeConnection2() throws SQLException {
        if (conn != null) {
            conn.close();
        }
        if (preStm != null) {
            preStm.close();
        }
    }

    public boolean insertOrderDetails(OrderDetailsDTO od) throws SQLException, NamingException {
        boolean result = false;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "INSERT INTO "
                        + "tblOrderDetails(orderDetailsID, orderID, carID, price_Save, rentalDate, returnDate) "
                        + "VALUES (?,?,?,?,?,?)";
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, od.getOrderDetailsID());
                preStm.setString(2, od.getOrderID());

                preStm.setString(3, od.getCarID());
                preStm.setDouble(4, od.getPrice_Save());
                preStm.setString(5, od.getRentalDate());
                preStm.setString(6, od.getReturnDate());
                result = preStm.executeUpdate() > 0;
            }
        } finally {
            closeConnection2();
        }
        return result;
    }
    
    public boolean deleteOrderDetails(String orderID) throws SQLException, NamingException {
        boolean result = false;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "DELETE FROM tblOrderDetails WHERE orderID=?";
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, orderID);
                result = preStm.executeUpdate() > 0;
            }
        } finally {
            closeConnection2();
        }
        return result;
    }
}
