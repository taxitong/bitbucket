/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.naming.NamingException;
import nhatlq.utils.DBConnection;

/**
 *
 * @author Admin
 */
public class OrderDiscountDAO {

    private Connection conn;
    private PreparedStatement preStm;
    private ResultSet rs;

    public OrderDiscountDAO() {
        conn = null;
        preStm = null;
        rs = null;
    }

    private void closeConnection3() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (preStm != null) {
            preStm.close();
        }
        if (conn != null) {
            conn.close();
        }
    }

    public boolean isNotUsed(String discountID) throws NamingException, SQLException {
        boolean result = false;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "SELECT orderID FROM tblOrderDiscount WHERE discountID=?";
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, discountID);
                rs = preStm.executeQuery();
                if (rs.next()) {
                    if (rs.getString("orderID") == null) {
                        result = true;
                    }
                }
            }
        } finally {
            closeConnection3();
        }
        return result;
    }

    public String getExpirationDate(String discountID) throws NamingException, SQLException {
        String result = null;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "SELECT expirationDate FROM tblOrderDiscount WHERE discountID=?";
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, discountID);
                rs = preStm.executeQuery();
                if (rs.next()) {
                    result = rs.getString("expirationDate");
                }
            }
        } finally {
            closeConnection3();
        }
        return result;
    }

    public int getAvailableDiscountPercent(String discountID) throws SQLException, NamingException {
        int result = -1;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "SELECT percentDiscount FROM tblOrderDiscount "
                        + "WHERE discountID=? AND orderID IS NULL";
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, discountID);
                rs = preStm.executeQuery();
                if (rs.next()) {
                    result = rs.getInt(1);
                }
            }
        } finally {
            closeConnection3();
        }
        return result;
    }

    public boolean updateOrderID(String orderID, String discountID) throws NamingException, SQLException {
        boolean res = false;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "UPDATE tblOrderDiscount SET orderID=? WHERE discountID=?";
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, orderID);
                preStm.setString(2, discountID);
                res = preStm.executeUpdate() > 0;
            }
        } finally {
            closeConnection3();
        }
        return res;
    }
    
     public boolean updateOrderID_ToNull(String discountID) throws NamingException, SQLException {
        boolean res = false;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "UPDATE tblOrderDiscount SET orderID=NULL WHERE discountID=?";
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, discountID);
                res = preStm.executeUpdate() > 0;
            }
        } finally {
            closeConnection3();
        }
        return res;
    }
}
