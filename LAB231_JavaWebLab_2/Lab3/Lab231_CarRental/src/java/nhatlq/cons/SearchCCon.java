/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.cons;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nhatlq.daos.CarDAO;
import nhatlq.decl.DB;
import nhatlq.decl.JSP;
import nhatlq.decl.Others;
import nhatlq.dtos.AccountDTO;
import nhatlq.dtos.CarDTO;

/**
 *
 * @author Admin
 */
public class SearchCCon extends HttpServlet {

    //page
    private final static String ERROR = JSP.ERROR;
    private final static String SUCCESS_AD = JSP.AD_MAIN;
    private final static String SUCCESS_US = JSP.US_MAIN;
    //attributes in the database  
    private static final String ACTIVE_STATUS_ID = DB.STATUS_ACTIVE;
    private static final String ADMIN_ROLE_ID = DB.ROLE_AD;
    private static final String USER_ROLE_ID = DB.ROLE_US;
    //record each page
    private static final int CAR_EACH_PAGE = Others.CARS_EACH_PAGE;//each page has 3 product;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            HttpSession session = request.getSession(false);
            AccountDTO accountDto = (AccountDTO) session.getAttribute("LOGIN_ACCOUNT");
            String roleID = accountDto.getRoleID();
            //search
            searchCar_Paging(roleID, request, session);
            switch (roleID) {
                case ADMIN_ROLE_ID:
                    url = SUCCESS_AD;
                    break;
                case USER_ROLE_ID:
                    url = SUCCESS_US;
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    private void searchCar_Paging(String roleID, HttpServletRequest request, HttpSession session) throws SQLException, NamingException, ParseException {
        //validate when user access via browser bar
        String carID = request.getParameter("tCarID_toSearchC");
        String categoryID = request.getParameter("tCategoryID_toSearchC");
        String typeID = request.getParameter("tTypeID_toSearchC");
        String statusID = request.getParameter("tStatusID_toSearchC");
        String startDate = request.getParameter("tStartDate_toSearchC");
        String endDate = request.getParameter("tEndDate_toSearchC");

        if (startDate != null || endDate != null) {
            carID = carID.toUpperCase();
            doActionIfValidInput(roleID, request, session, carID, categoryID, typeID, statusID, startDate, endDate);
        }
    }

    private void swapDate(String startDate, String endDate) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date1 = sdf.parse(startDate);
        Date date2 = sdf.parse(endDate);
        if (date1.compareTo(date2) > 0) {//date1 > date2
            String tmpString = startDate;
            startDate = endDate;
            endDate = tmpString;
        }
    }

    private void doActionIfValidInput(String roleID, HttpServletRequest request, HttpSession session, String carID, String categoryID, String typeID, String statusID, String startDate, String endDate) throws ParseException, SQLException, NamingException {
        swapDate(startDate, endDate);
        if (statusID.isEmpty() && !ADMIN_ROLE_ID.equals(roleID)) {
            statusID = ACTIVE_STATUS_ID;
        }
        int indexC;
        try {
            indexC = Integer.parseInt(request.getParameter("indexC"));
        } catch (Exception e) {
            indexC = 1;
        }
        int size = CAR_EACH_PAGE;
        int endPageC;

        CarDAO carDao = new CarDAO();
        int count = carDao.countCarAvailableInRange(carID, categoryID, typeID, statusID, startDate, endDate);
        List<CarDTO> listC = carDao.searchCarAvailableInRange(carID, categoryID, typeID, statusID, startDate, endDate, indexC, size);

        endPageC = count / size;
        if (count % size != 0) {
            endPageC++;
        }
        session.setAttribute("endPageC", endPageC);
        session.setAttribute("LIST_C", listC);
        session.setAttribute("indexC", indexC);

        session.setAttribute("tCarID_toSearchC", carID);
        session.setAttribute("tCategoryID_toSearchC", categoryID);
        session.setAttribute("tTypeID_toSearchC", typeID);
        session.setAttribute("tStatusID_toSearchC", statusID);
        session.setAttribute("tStartDate_toSearchC", startDate);
        session.setAttribute("tEndDate_toSearchC", endDate);
        session.setAttribute("tStartDate_fromSearchC", startDate);
        session.setAttribute("tEndDate_fromSearchC", endDate);
        
        session.setAttribute("discountNotValid", null);//remove invalid discount
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
