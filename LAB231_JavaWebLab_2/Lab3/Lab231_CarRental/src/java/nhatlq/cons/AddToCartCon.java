/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.cons;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nhatlq.daos.CarDAO;
import nhatlq.decl.JSP;
import nhatlq.dtos.CarDTO;
import nhatlq.dtos.CartDTO;

/**
 *
 * @author Admin
 */
public class AddToCartCon extends HttpServlet {

    private static final String ERROR = JSP.ERROR;
    private static final String SUCCESS_US = JSP.US_MAIN;
    private static final String FAIL_US = JSP.US_MAIN;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            String carID = request.getParameter("tCarID_toAddToCart");
            String startDate = request.getParameter("tStartDate_toAddToCart");
            String endDate = request.getParameter("tEndDate_toAddToCart");

            if (carID != null || startDate != null || endDate != null) {
                HttpSession session = request.getSession();
                addToCart(session, carID, startDate, endDate);
                url = SUCCESS_US;
            } else {
                url = FAIL_US;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    private void addToCart(HttpSession session, String carID, String startDate, String endDate) throws NamingException, SQLException, ParseException {
        CarDAO carDao = new CarDAO();
        CarDTO carDto = carDao.getCar(carID);
        //String carID;
        double price = carDto.getPrice();
        String img = carDto.getImg();
        String categoryID = carDto.getCategoryID();
        String typeID = carDto.getTypeID();
        String statusID = carDto.getStatusID();
        int dateDifference = findDateDifferenc(startDate, endDate);
        CarDTO carWithDates = new CarDTO(carID, price, img, categoryID, typeID, statusID, startDate, endDate, dateDifference);
        //prepare to add to cart
        CartDTO cart = (CartDTO) session.getAttribute("CART");
        if (cart == null) {//if dont have cart, create one
            cart = new CartDTO();
        }
        cart.add(carWithDates);
        session.setAttribute("CART", cart);

        session.setAttribute("discountNotValid", null);//remove invalid discount
    }

    private int findDateDifferenc(String startDate, String endDate) throws ParseException {
        //get day(s) between startDate & endDate
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date1 = sdf.parse(startDate);
        Date date2 = sdf.parse(endDate);
        long diff = date2.getTime() - date1.getTime();
        long diffDay = diff / (24 * 60 * 60 * 1000);
        int dateDifference = 1 + (int) diffDay;
        return dateDifference;
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
