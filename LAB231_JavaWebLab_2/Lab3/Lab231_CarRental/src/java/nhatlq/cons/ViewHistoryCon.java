/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.cons;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nhatlq.daos.OrderDAO;
import nhatlq.decl.DB;
import nhatlq.decl.JSP;
import nhatlq.decl.Others;
import nhatlq.dtos.AccountDTO;
import nhatlq.dtos.OrderDTO;

/**
 *
 * @author Admin
 */
public class ViewHistoryCon extends HttpServlet {

    //DB
    private final static String STATUS_SUSPEND = DB.STATUS_SUSPEND;
    //JSP
    private final static String ERROR = JSP.ERROR;
    private final static String SUCCESS = JSP.US_TRACKING;
    //Others
    private static final int ORDER_EACH_PAGE = Others.ORDER_EACH_PAGE;//each page has n product;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            HttpSession session = request.getSession();
            AccountDTO accountDto = (AccountDTO) session.getAttribute("LOGIN_ACCOUNT");
            if (accountDto != null) {
                viewHistory(request, session, accountDto);
                url = SUCCESS;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            request.getRequestDispatcher(url).forward(request, response);

        }
    }

    private void viewHistory(HttpServletRequest request, HttpSession session, AccountDTO accountDto) throws NamingException, SQLException {
        String email = accountDto.getEmail();
        int indexO;
        try {
            indexO = Integer.parseInt(request.getParameter("indexO"));
        } catch (Exception e) {
            indexO = 1;
        }
        String displayName_Save = request.getParameter("tDisplayName_Save_toViewHistory") == null ? "" : request.getParameter("tDisplayName_Save_toViewHistory");
        String orderDate = request.getParameter("tDateCreate_toViewHistory") == null ? "" : request.getParameter("tDateCreate_toViewHistory");;
        int size = ORDER_EACH_PAGE;

        OrderDAO orderDao = new OrderDAO();
        int count = orderDao.countOrder(email, displayName_Save, orderDate);
        List<OrderDTO> listO = orderDao.getAllOrder(email, displayName_Save, orderDate, indexO, size);
        List<String> listDateRange = orderDao.getAllDateOrder(email);//get date for search funct in tracking

        int endPageO = count / size;
        if (count % size != 0) {
            endPageO++;
        }

        session.setAttribute("endPageO", endPageO);
        session.setAttribute("LIST_O", listO);
        session.setAttribute("indexO", indexO);

        session.setAttribute("suspendStatus_fromViewHistory", STATUS_SUSPEND);

        session.setAttribute("LIST_ALL_DATE_ORDER", listDateRange);
        session.setAttribute("tDateCreate_toViewHistory", orderDate);
        session.setAttribute("tDisplayName_Save_toViewHistory", displayName_Save);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
