/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.cons;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nhatlq.daos.AccountDAO;
import nhatlq.daos.CategoryDAO;
import nhatlq.daos.StatusDAO;
import nhatlq.daos.TypeDAO;
import nhatlq.dtos.AccountDTO;
import nhatlq.decl.DB;
import nhatlq.decl.JSP;
import nhatlq.dtos.CategoryDTO;
import nhatlq.dtos.StatusDTO;
import nhatlq.dtos.TypeDTO;

/**
 *
 * @author Admin
 */
public class SignInCon extends HttpServlet {

    private static final String STATUS_ACTIVE = DB.STATUS_ACTIVE;
    private static final String ROLE_AD = DB.ROLE_AD;
    private static final String ROLE_US = DB.ROLE_US;

    private static final String START_PAGE_JSP = JSP.START_PAGE;
    private static final String ERROR_JSP = JSP.ERROR;
    private static final String AD_MAIN_JSP = JSP.AD_MAIN;
    private static final String US_MAIN_JSP = JSP.US_MAIN;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR_JSP;
        try {
            String email = request.getParameter("tEmail_toSignIn");
            String password = request.getParameter("tPassword_toSignIn");

            HttpSession session = request.getSession();
            if (email != null || password != null) {
                AccountDAO accountDao = new AccountDAO();
                AccountDTO accountDto = accountDao.checkSignIn(email, password);
                if (accountDto == null || !STATUS_ACTIVE.equals(accountDto.getStatusID())) {
                    url = doIfUnauthenticated(session);
                } else {
                    url = doIfAuthenticated(request, session, accountDto);
                }
            } else {
                url = "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            response.sendRedirect(url);
        }
    }

    private String doIfUnauthenticated(HttpSession session) {
        session.setAttribute("INVALID_ACCOUNT", "Invalid email/password OR Your account hasn't been verified by admin");
        return START_PAGE_JSP;
    }

    private String doIfAuthenticated(HttpServletRequest request, HttpSession session, AccountDTO accountDto) throws SQLException, NamingException {
        session.setAttribute("LOGIN_ACCOUNT", accountDto);
        session.setAttribute("INVALID_ACCOUNT", null);
        String url = "";
        String roleID = accountDto.getRoleID();
        getAllCategory_NoPaging(session);
        getAllType_NoPaging(session);
        getAllStatus_NoPaging(session);
        switch (roleID) {
            case ROLE_AD:
                url = AD_MAIN_JSP;
                break;
            case ROLE_US:
                url = US_MAIN_JSP;
                break;
        }
        return url;
    }

    private void getAllCategory_NoPaging(HttpSession session) throws SQLException, NamingException {
        CategoryDAO categoryDao = new CategoryDAO();
        List<CategoryDTO> listCategory = categoryDao.searchAllCategory_NoPaging();
        session.setAttribute("LIST_CATEGORY", listCategory);
    }

    private void getAllType_NoPaging(HttpSession session) throws SQLException, NamingException {
        TypeDAO typeDao = new TypeDAO();
        List<TypeDTO> listType = typeDao.searchAllType_NoPaging();
        session.setAttribute("LIST_TYPE", listType);
    }

    private void getAllStatus_NoPaging(HttpSession session) throws SQLException, NamingException {
        StatusDAO statusDao = new StatusDAO();
        List<StatusDTO> listStatus = statusDao.searchAllStatus_NoPaging();
        session.setAttribute("LIST_STATUS", listStatus);
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
