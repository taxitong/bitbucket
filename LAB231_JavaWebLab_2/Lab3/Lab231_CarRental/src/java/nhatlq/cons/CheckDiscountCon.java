/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.cons;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nhatlq.daos.OrderDiscountDAO;
import nhatlq.decl.JSP;

/**
 *
 * @author Admin
 */
public class CheckDiscountCon extends HttpServlet {

    private static final String ERROR = JSP.ERROR;
    private static final String SUCCESS_US = JSP.US_CART;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            String discountID = request.getParameter("tDiscountID_to_fromCheckDiscount");
            HttpSession session = request.getSession();
            OrderDiscountDAO odDao = new OrderDiscountDAO();

            boolean check = false;
            int percentDiscount = 0;
            String errMsg = "";

            boolean isExists = odDao.getExpirationDate(discountID) != null;//boolean isOrderDiscount_Exists = odDao.getExpirationDate(discountID) == null ? false : true;
            if (isExists) {
                boolean isNotUsed = odDao.isNotUsed(discountID);
                if (isNotUsed) {
                    boolean isInDate = isInDate(discountID, odDao, request);
                    if (isInDate) {
                        percentDiscount = odDao.getAvailableDiscountPercent(discountID);
                        check = true;
                    } else {
                        errMsg = "Expired Discount";
                    }
                } else {
                    errMsg = "Used Discount";
                }
            } else {
                errMsg = "Invalid Discount";
            }
            if (check) {
                session.setAttribute("percentDiscount_fromCheckDiscount", percentDiscount);
                session.setAttribute("discountNotValid", null);
            } else {
                session.setAttribute("percentDiscount_fromCheckDiscount", null);
                session.setAttribute("discountNotValid", errMsg);
            }
            session.setAttribute("tDiscountID_to_fromCheckDiscount", discountID);
            url = SUCCESS_US;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    private boolean isInDate(String discountID, OrderDiscountDAO orderDiscountDao, HttpServletRequest request) throws NamingException, SQLException, ParseException {
        boolean result = false;
        OrderDiscountDAO odDao = new OrderDiscountDAO();
        String today = request.getParameter("tToday_toCheckDiscount");
        String expirationDate = odDao.getExpirationDate(discountID);
        if (expirationDate != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date1 = sdf.parse(today);
            Date date2 = sdf.parse(expirationDate);
            if (date1.compareTo(date2) <= 0) {//date1 > date2
                result = true;
            }
        }
        return result;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
