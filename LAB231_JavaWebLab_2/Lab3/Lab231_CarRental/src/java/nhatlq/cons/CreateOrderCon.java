/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.cons;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Random;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nhatlq.daos.CarDAO;
import nhatlq.daos.OrderDAO;
import nhatlq.daos.OrderDetailsDAO;
import nhatlq.daos.OrderDiscountDAO;
import nhatlq.decl.DB;
import nhatlq.decl.JSP;
import nhatlq.decl.Others;
import nhatlq.dtos.AccountDTO;
import nhatlq.dtos.CarDTO;
import nhatlq.dtos.CartDTO;
import nhatlq.dtos.OrderDTO;
import nhatlq.dtos.OrderDetailsDTO;
import nhatlq.utils.Encryption;

/**
 *
 * @author Admin
 */
public class CreateOrderCon extends HttpServlet {

    private static final String STATUS_ACTIVE = DB.STATUS_ACTIVE;

    private static final String ERROR = JSP.ERROR;
    private static final String US_ORDER_FAIL = JSP.US_ORDER_FAIL;

    private static final String SUCCESS_US = Others.VIEW_HISTORY_AND_SEARCH_ORDER;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            HttpSession session = request.getSession();
            CartDTO cartDTO = (CartDTO) session.getAttribute("CART");
            AccountDTO accDto = (AccountDTO) session.getAttribute("LOGIN_ACCOUNT");
            //create Order
            String orderID = generateID();
            double total = Double.parseDouble(request.getParameter("tTotalReducedOrder_toCreateOrder"));
            String dateCreate = request.getParameter("tToday_toCreateOrder");
            String statusID = STATUS_ACTIVE;
            String email = accDto.getEmail();
            String displayName_Save = request.getParameter("tDisplayName_Save_toCreateOrder");
            String phoneNo_Save = request.getParameter("tPhoneNo_Save_toCreateOrder");
            String addr_Save = request.getParameter("tAddr_Save_toCreateOrder");

            url = createOrder(request, session, cartDTO, orderID, total, dateCreate, statusID, email, displayName_Save, phoneNo_Save, addr_Save);

            resetChosenDiscount(session);
            session.setAttribute("LIST_C", null);
        } catch (Exception e) {
            //e.printStackTrace();
        } finally {
            response.sendRedirect(url);
            //request.getRequestDispatcher(url).forward(request, response);
        }
    }

    private String createOrder(HttpServletRequest request, HttpSession session, CartDTO cartDTO, String orderID, double total, String dateCreate, String statusID, String email, String displayName_Save, String phoneNo_Save, String addr_Save) throws SQLException, NamingException {
        String url = ERROR;
        OrderDTO orderDto = new OrderDTO(orderID, total, dateCreate, statusID, email, displayName_Save, phoneNo_Save, addr_Save);
        //insert the order into database
        OrderDAO orderDao = new OrderDAO();
        boolean isInsertOrder = orderDao.insertOder(orderDto);
        //check in database
        if (isInsertOrder) {//If insert order is successful, let's start looking at insert orderDetails 
            for (CarDTO carDto : cartDTO.getCart().values()) {
                String startDate = carDto.getStartDate();
                String endDate = carDto.getEndDate();
                String carID = carDto.getCarID();
                CarDAO carDao = new CarDAO();
                boolean isCarQualifiedToCreateOrder = carDao.isCarQualifiedToCreateOrder(startDate, endDate, carID);
                if (isCarQualifiedToCreateOrder) {
                    //new OD
                    String orderDetailsID = generateID();
                    //orderID
                    //carID
                    double price_Save = carDto.getPrice();
                    String rentalDate = carDto.getStartDate();
                    String returnDate = carDto.getEndDate();
                    OrderDetailsDTO orderDetailsDto = new OrderDetailsDTO(orderDetailsID, orderID, carID, price_Save, rentalDate, returnDate);
                    OrderDetailsDAO orderDetailsDao = new OrderDetailsDAO();
                    boolean isInsertOrderDetails = orderDetailsDao.insertOrderDetails(orderDetailsDto);
                    //deal with discount codes 
                    String discountID = request.getParameter("tDiscountID_toCreateOrder");
                    if (discountID != null) {
                        OrderDiscountDAO orderDiscountDao = new OrderDiscountDAO();
                        orderDiscountDao.updateOrderID(orderID, discountID);
                    }
                    if (isInsertOrderDetails) {//If insert orderDetails is successful, move to success page
                        session.setAttribute("CART", null);
                        url = SUCCESS_US;
                    }
                } else {
                    //delete OD with O id
                    OrderDetailsDAO orderDetailsDao = new OrderDetailsDAO();
                    orderDetailsDao.deleteOrderDetails(orderID);
                    //remove O id from discount so that we could delete the Order
                    OrderDiscountDAO orderDiscountDao = new OrderDiscountDAO();
                    String discountID = request.getParameter("tDiscountID_toCreateOrder");
                    orderDiscountDao.updateOrderID_ToNull(discountID);
                    //delete O
                    orderDao.deleteOder(orderID);
                    session.setAttribute("CART", null);
                    url = US_ORDER_FAIL;
                }
            }
        }
        return url;
    }

    private void resetChosenDiscount(HttpSession session) {
        session.setAttribute("tDiscountID_toCreateOrder", null);
        session.setAttribute("tDiscountID_to_fromCheckDiscount", null);
        session.setAttribute("percentDiscount_fromCheckDiscount", null);
        session.setAttribute("discountNotValid", null);
    }

    private String generateID() {
        String result = null;

        Random rd = new Random();//https://www.tutorialspoint.com/generate-random-bytes-in-java
        byte[] arr = new byte[7];
        rd.nextBytes(arr);

        String createDate = "" + new Timestamp(System.currentTimeMillis());
        Encryption en = new Encryption();
        try {
            result = en.toHexString(en.getSHA(Arrays.toString(arr) + createDate));
        } catch (NoSuchAlgorithmException e) {
        }
        return result;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
