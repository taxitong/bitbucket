USE Test1
--https://stackoverflow.com/questions/43046102/query-for-which-vehicle-is-available-on-for-a-booking
SELECT c.* FROM tblCar AS c 
LEFT JOIN (SELECT r.* 
           FROM tblRental AS r 
           WHERE r.returnDate >= '3/21/2021'--SD 
		   and r.rentalDate <= '3/22/2021'--ED
          )  AS r ON c.carID = r.carID
WHERE r.carID IS NULL
--de nhin
SELECT c.*, r.* FROM tblCar AS c 
LEFT JOIN (SELECT r.* 
           FROM tblRental AS r 
           WHERE r.returnDate >= '3/21/2021'--SD 
		   and r.rentalDate <= '3/22/2021'--ED
          )  AS r ON c.carID = r.carID
WHERE r.carID IS NULL
--giai thich select tung cai
SELECT c.*, r.* FROM tblCar AS c 
LEFT JOIN tblRental AS r 
ON c.carID = r.carID
WHERE r.returnDate >= '3/21/2021'--SD 
and r.rentalDate <= '3/22/2021'--ED
AND r.carID IS NULL
--ngc
SELECT c.*, r.* FROM tblCar AS c 
LEFT JOIN tblRental AS r 
ON c.carID = r.carID
WHERE r.returnDate > '3/22/2021'--ED
and r.rentalDate < '3/21/2021'--SD 
AND r.carID IS NULL
--tmp
SELECT c.*, r.* FROM tblCar AS c 
LEFT JOIN (SELECT r.* 
           FROM tblRental AS r 
           WHERE r.returnDate > '3/22/2021'--ED
		   and r.rentalDate  < '3/21/2021'--SD
          )  AS r ON c.carID = r.carID
WHERE r.carID IS NULL