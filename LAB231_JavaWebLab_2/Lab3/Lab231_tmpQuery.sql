--1st
SELECT c.*, od.* FROM tblCar AS c 
LEFT JOIN (SELECT od.* 
           FROM tblOrderDetails AS od 
           WHERE od.returnDate >= '3/27/2021'--SD 
		   AND od.rentalDate <= '3/28/2021'--ED
) AS od ON c.carID = od.carID
WHERE od.carID IS NULL 
AND c.carID LIKE '%%' AND c.categoryID LIKE '%%' AND c.statusID='active' 
ORDER BY c.carID ASC
--new 1st	
SELECT c.*, mix.* FROM tblCar AS c 
LEFT JOIN (SELECT od.*
           FROM tblOrderDetails AS od 
		   INNER JOIN tblOrder AS o 
		   ON od.orderID = o.orderID
           WHERE od.returnDate >= '3/27/2021'--SD 
		   AND od.rentalDate <= '3/28/2021'--ED
		   AND o.statusID='active'
) AS mix ON c.carID = mix.carID
WHERE mix.carID IS NULL 
AND c.carID LIKE '%%' AND c.categoryID LIKE '%%' AND c.statusID='active' 
ORDER BY c.carID ASC










--real
--countCar_GenerateSQL
SELECT count(c.carID) FROM tblCar AS c 
LEFT JOIN (SELECT od.carID
           FROM tblOrderDetails AS od 
		   INNER JOIN tblOrder AS o 
		   ON od.orderID = o.orderID
           WHERE od.returnDate >= '3/27/2021'--SD 
		   AND od.rentalDate <= '3/28/2021'--ED
		   AND o.statusID='active'
) AS od ON c.carID = od.carID
WHERE od.carID IS NULL 
AND c.carID LIKE '%%' AND c.categoryID LIKE '%%' AND c.statusID='active' 

--searchCar_GenerateSQL - without paging
SELECT c.carID, c.price, c.img, c.categoryID, c.statusID FROM tblCar AS c 
LEFT JOIN (SELECT od.carID
           FROM tblOrderDetails AS od 
		   INNER JOIN tblOrder AS o 
		   ON od.orderID = o.orderID
           WHERE od.returnDate >= '3/27/2021'--SD 
		   AND od.rentalDate <= '3/28/2021'--ED
		   AND o.statusID='active'
) AS od ON c.carID = od.carID
WHERE od.carID IS NULL 
AND c.carID LIKE '%%' AND c.categoryID LIKE '%%' AND c.statusID='active' ORDER BY c.carID ASC

--normal select paging
SELECT 
carID, price, img, categoryID, statusID
FROM (SELECT ROW_NUMBER() OVER (ORDER BY c.carID ASC) AS r, 
c.carID, c.price, c.img, c.categoryID, c.statusID
FROM tblCar AS c WHERE c.carID LIKE '%%' AND c.categoryID LIKE '%%' AND c.statusID='active'
) as x WHERE r BETWEEN 1 AND 6

--searchCar_GenerateSQL
SELECT carID, price, img, categoryID, statusID
FROM (SELECT ROW_NUMBER() OVER (ORDER BY c.carID ASC) AS r, 
c.carID, c.price, c.img, c.categoryID, c.statusID FROM tblCar AS c 
LEFT JOIN (SELECT od.carID
           FROM tblOrderDetails AS od 
		   INNER JOIN tblOrder AS o 
		   ON od.orderID = o.orderID
           WHERE od.returnDate >= '3/27/2021'--SD 
		   AND od.rentalDate <= '3/28/2021'--ED
		   AND o.statusID='active'
) AS od ON c.carID = od.carID
WHERE od.carID IS NULL 
AND c.carID LIKE '%%' AND c.categoryID LIKE '%%' AND c.statusID='active' 
) AS x WHERE r BETWEEN 1 AND 6  ORDER BY carID ASC


--createOrder
SELECT c.*, mix.* FROM tblCar AS c 
LEFT JOIN (SELECT od.*
           FROM tblOrderDetails AS od 
		   INNER JOIN tblOrder AS o 
		   ON od.orderID = o.orderID
           WHERE od.returnDate >= '3/28/2021'--SD 
		   AND od.rentalDate <= '3/28/2021'--ED
		   AND o.statusID='active'
) AS mix ON c.carID = mix.carID
WHERE mix.carID IS NULL 
AND c.carID='H1'
--createOrder--isQualifiedToCreateOrder: if has record-> qualified
SELECT c.carID FROM tblCar AS c 
LEFT JOIN (SELECT od.*
           FROM tblOrderDetails AS od 
		   INNER JOIN tblOrder AS o 
		   ON od.orderID = o.orderID
           WHERE od.returnDate >= '3/27/2021'--SD 
		   AND od.rentalDate <= '3/28/2021'--ED
		   AND o.statusID='active'
) AS mix ON c.carID = mix.carID
WHERE mix.carID IS NULL 
AND c.carID='H1'

