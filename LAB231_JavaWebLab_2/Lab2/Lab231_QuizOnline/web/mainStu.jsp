<%-- 
    Document   : mainStu
    Created on : Jan 27, 2021, 3:18:21 PM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="sharedJSP/appMainCSS.jsp"/>
        <title>Main Stu</title>
    </head>
    <body>
        <section>
            <jsp:include page="sepJSP_STU/appMenuBarStu.jsp"/>
            <jsp:include page="sepJSP_STU/mainStu_SearchSRes.jsp"/>
            <jsp:include page="sharedJSP/appSciAndTopButton.jsp"/>
        </section>
        <span class="menuicon" id="toggle" onclick="menuToggle()"></span>
        <script src="js/main.js" type="text/javascript"></script>
    </body>
</html>
