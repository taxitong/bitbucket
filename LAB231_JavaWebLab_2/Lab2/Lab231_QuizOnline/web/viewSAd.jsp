<%-- 
    Document   : viewSAd
    Created on : Mar 8, 2021, 7:45:34 PM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="sharedJSP/appMainCSS.jsp"/>
        <title>View Subject Ad</title>
    </head>
    <body>
        <section>
            <jsp:include page="sepJSP_AD/appMenuBarAd.jsp"/>
            <div class="content">
                <jsp:include page="sepJSP_AD/appHelloAd.jsp"/>
                <div class="main-text">
                    <P class="title">
                        All Subject:
                    </p>
                    <p class="note">Order by Subject ID ascending alphabetically (A - Z)</p>
                    <c:if test="${sessionScope.LIST_S != null}">
                        <c:if test="${not empty sessionScope.LIST_S}">
                            <c:forEach var="subject" varStatus="counter" items="${sessionScope.LIST_S}">
                                <div class="each-main-text">
                                    <p class="subject-text">${subject.subjectID} - ${subject.subjectName}</p>
                                    <p class="subject-hightlight">Status: ${subject.statusID}</p>
                                    <br><br>
                                    <form action="SearchQz?tSID_SearchQz=${subject.subjectID}&indexQz=1" style="display: inline;" class="subject-name" method="POST">
                                        <input type="hidden" name="tSID_SearchQz" value="${subject.subjectID}"/>
                                        <input type="hidden" name="tSName_SearchQz" value="${subject.subjectName}"/>
                                        <input class="controlButton" type="submit" value="View Quiz"/>
                                    </form>
                                </div>
                            </c:forEach>
                            <c:forEach begin="1" end="${endPageS}" var="i">
                                <a style="color: black;" id="${i}" href="SearchS?tSID_SearchS=&indexS=${i}">${i}</a>
                            </c:forEach>
                            <script>
                                document.getElementById('${indexS}').style.background = "#ff2562";
                                document.getElementById('${indexS}').style.color = "#FFFFFF";
                            </script>
                        </c:if>
                        <c:if test="${empty sessionScope.LIST_S}">
                            <jsp:include page="sepJSP_AD/viewSAd_SearchNoS.jsp"/>
                        </c:if>
                    </c:if>
                </div>
                <!--<a href="#">Learn More</a>-->
                <!--</div>-->
            </div>
            <jsp:include page="sharedJSP/appSciAndTopButton.jsp"/>
        </section>
        <span class="menuicon" id="toggle" onclick="menuToggle()"></span>
        <script src="js/main.js" type="text/javascript"></script>
    </body>
</html>
