<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<select class="custom-text-input" name="tQsAnswer_UpdateQs" id="quizStatus" class="select">
    <!--A-->
    <c:if test="${sessionScope.QUES_TO_UPDATE.answer == 'A'}">
        <option value="A" class="hightlight" selected>Option A (Base)</option>
    </c:if>
    <c:if test="${sessionScope.QUES_TO_UPDATE.answer != 'A'}">
        <option value="A">Option A</option>
    </c:if>
    <!--B-->
    <c:if test="${sessionScope.QUES_TO_UPDATE.answer == 'B'}">
        <option value="B" class="hightlight" selected>Option B (Base)</option>
    </c:if>
    <c:if test="${sessionScope.QUES_TO_UPDATE.answer != 'B'}">
        <option value="B">Option B</option>
    </c:if>
    <!--C-->
    <c:if test="${sessionScope.QUES_TO_UPDATE.answer == 'C'}">
        <option value="C" class="hightlight" selected>Option C (Base)</option>
    </c:if>
    <c:if test="${sessionScope.QUES_TO_UPDATE.answer != 'C'}">
        <option value="C">Option C</option>
    </c:if>
    <!--D-->
    <c:if test="${sessionScope.QUES_TO_UPDATE.answer == 'D'}">
        <option value="D" class="hightlight" selected>Option D (Base)</option>
    </c:if>
    <c:if test="${sessionScope.QUES_TO_UPDATE.answer != 'D'}">
        <option value="D">Option D</option>
    </c:if>
</select>