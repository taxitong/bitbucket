<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${sessionScope.LIST_Qs != null}">
    <c:if test="${not empty sessionScope.LIST_Qs}">
        <c:forEach var="question" varStatus="counter" items="${sessionScope.LIST_Qs}">
            <div class="each-main-text">
                <c:forEach var="quiz" varStatus="counter" items="${sessionScope.listQzID_From_SearchQs}">
                    <c:if test="${quiz.quizID == question.quizID}">
                        Subject ID: ${quiz.subjectID} - Quiz: ${quiz.quizName}</br>
                        <c:set var="tmpSubjectID" scope="page" value="${quiz.subjectID}"/>  
                    </c:if>
                </c:forEach>
                <p class="hightlight">Status: ${question.statusID}</p></br>
                <p class="subject-name">${question.content}</p>
                A. ${question.optionA}</br> 
                B. ${question.optionB}</br> 
                C. ${question.optionC}</br> 
                D. ${question.optionD}</br> 
                </br> 
                <font color='red'>Correct Answer: ${question.answer}.</font>
                </br></br>
                <form action="DeleteQs" style="display: inline;" method="POST">
                    <input type="hidden" name="tQsID_DeleteQs" value="${question.quesID}"/>
                    <!--for delect respond-->
                    <input type="hidden" name="tSID_DeleteQs" value="${tSID_SearchQs}"/>
                    <input type="hidden" name="tQzID_DeleteQs" value="${tQzID_SearchQs}"/>
                    <input type="hidden" name="tQsStatusID_DeleteQs" value="${tQsStatusID_SearchQs}"/>
                    <input type="hidden" name="tQsContent_DeleteQs" value="${tQsContent_SearchQs}"/>
                    <!--for delect respond-->
                    <input class="controlButton" type="submit" onclick="return confirm('Delete question. Are you sure?')" value="Delete"/>
                </form>
                <form action="PreUpdateQs?tQsID_PreUpdateQs=${question.quesID}" style="display: inline;" method="POST">
                    <input type="hidden" name="tQsID_PreUpdateQs" value="${question.quesID}"/>
                    <input class="controlButton" type="submit" value="Update"/>
                </form>
                <form action="PreMoveQs" style="display: inline;" method="POST">
                    <input type="hidden" name="tSID_PreMoveQs" value="${tmpSubjectID}"/>
                    <input type="hidden" name="tQzID_PreMoveQs" value="${question.quizID}"/>
                    <input type="hidden" name="tQsID_PreMoveQs" value="${question.quesID}"/>
                    <input class="controlButton" type="submit" value="Move to..."/>
                </form>
            </div>
        </c:forEach>
        <c:forEach begin="1" end="${endPageQs}" var="i">
            <a style="color: black;" id="${i}" href="SearchQs?tSID_SearchQs=${tSID_SearchQs}&tQzID_SearchQs=${tQzID_SearchQs}&tQsStatusID_SearchQs=${tQsStatusID_SearchQs}&tQsContent_SearchQs=${tQsContent_SearchQs}&indexQs=${i}">${i}</a>
        </c:forEach>
        <script>
            document.getElementById('${indexQs}').style.background = "#ff2562";
            document.getElementById('${indexQs}').style.color = "#FFFFFF";
        </script>
    </c:if>
    <c:if test="${empty sessionScope.LIST_Qs}">
        <jsp:include page="mainAd_SearchNoQs.jsp"/>
    </c:if>
</c:if>