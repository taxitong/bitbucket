<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="content">
    <jsp:include page="appHelloAd.jsp"/>
    <div class="main-text">
        <P class="title">
            Home Page
        </p>
        <jsp:include page="mainAd_SearchQsBar.jsp"/>
        <p class="note">Order by subjectID ascending alphabetically (A - Z)</p>
        <jsp:include page="mainAd_SearchQsRes.jsp"/>
    </div>
</div>