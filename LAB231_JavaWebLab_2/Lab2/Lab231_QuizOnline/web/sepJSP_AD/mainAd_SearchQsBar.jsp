<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="each-main-text">
    <form action="GetSToQz" class="subject-text" id="SearchQzForm" method="POST">
        <!--for testing, change these 3hidden input back to text-->
        <!--<P class="title">In searching: </p>-->
        <input type="hidden" name="tQzStatusID_GetSToQz" id="sStatusID2" value="${sessionScope.tQsStatusID_SearchQs}"/>
        <input type="hidden" name="tQsContent_GetSToQz" id="sContent2" value="${sessionScope.tQsContent_SearchQs}"/>
        <!--</br></br>-->
        <!--for testing, change these 3hidden input back to text-->
        <P class="title">Search Question: </p>
        Subject: 
        <select name="tSID_GetSToQz" style="text-transform: capitalize;" id="tSID_SearchQz" onchange="autoSubmitSubjectID()">
            <option value="">All</option>
            <c:forEach var="subject" varStatus="counter" items="${sessionScope.LIST_ALLSUBJECT}">
                <option value="${subject.subjectID}" <c:if test="${sessionScope.tSID_SearchQs == subject.subjectID}">selected="true"</c:if>>
                    ${subject.subjectID} - ${subject.subjectName} (${subject.statusID})
                </option>
            </c:forEach>
        </select>
    </form>
    <form action="SearchQs" class="subject-text" method="POST">
        <!--for testing, change this hidden input  back to text-->
        <input type="hidden" name="tSID_SearchQs" value="${sessionScope.tSID_SearchQs}"/>
        <!--for testing, change this hidden input  back to text-->
        Quiz:
        <c:if test="${not empty sessionScope.listQzID_From_GetSToQz__SearchQs_SignIn}">
            <select name="tQzID_SearchQs" style="text-transform: capitalize;" <c:if test="${empty sessionScope.tSID_SearchQs}">disabled</c:if>>
                <option value="" <c:if test="${empty sessionScope.tSID_SearchQs}">selected</c:if>>All</option>          
                <c:forEach var="quiz" varStatus="counter" items="${sessionScope.listQzID_From_GetSToQz__SearchQs_SignIn}">
                    <option value="${quiz.quizID}" <c:if test="${sessionScope.tQzID_SearchQs == quiz.quizID}">selected</c:if>>
                        ${quiz.quizName}
                    </option>
                </c:forEach>
            </select> 
        </c:if>
        <c:if test="${empty sessionScope.listQzID_From_GetSToQz__SearchQs_SignIn || empty sessionScope.tSID_SearchQs}">
            <input type="hidden" name="tQzID_SearchQs" value="" />
        </c:if>
        <c:if test="${empty sessionScope.listQzID_From_GetSToQz__SearchQs_SignIn}">
            <select name="tQzID_SearchQs" style="color: red;" disabled>
                <option value="">No Quiz</option>
            </select>
        </c:if>
        <c:if test="${empty sessionScope.tSID_SearchQs}">
        </c:if>
        Question's status: 
        <select name="tQsStatusID_SearchQs" id="sStatusID" style="text-transform: capitalize;" onchange="rememberStatusID()">
            <option value="">All</option>
            <c:forEach var="statusID" varStatus="counter" items="${sessionScope.LIST_STATUS_ID}">
                <option value="${statusID}" <c:if test="${sessionScope.tQsStatusID_SearchQs == statusID}">selected=</c:if>>
                    ${statusID}
                </option>
            </c:forEach>
        </select></br></br>
        <textarea class="custom-text-input" id="sContent" style="resize: none;" name="tQsContent_SearchQs" onchange="rememberContent()" maxlength="3000" rows="4" cols="50" placeholder="What you want to look for in the CONTENT of the question is . . . .">${sessionScope.tQsContent_SearchQs}</textarea></br>
        <input class="controlButton" type="submit" <c:if test="${empty sessionScope.listQzID_From_GetSToQz__SearchQs_SignIn}">disabled style="opacity: 0.5;"</c:if> value="Search"/>
    </form>
</div>