<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="content">
    <jsp:include page="appHelloStu.jsp"/>
    <div class="main-text">
        <P class="title">
            All Subject: 
        </p>
        <p class="note">Order by Subject ID ascending alphabetically (A - Z)</p>
        <c:forEach var="subject" varStatus="counter" items="${sessionScope.LIST_S}">
            <div class="each-main-text">
                <p class="subject-text">${subject.subjectID} - ${subject.subjectName}</p><br><br>
                <form action="SearchQz" class="subject-name" method="POST">
                    <input type="hidden" name="tSID_SearchQz" value="${subject.subjectID}"/>
                    <input type="hidden" name="tSName_SearchQz" value="${subject.subjectName}"/>
                    <input class="controlButton" type="submit" value="View Subject"/>
                </form>
            </div>
        </c:forEach>
        <c:forEach begin="1" end="${endPageS}" var="i">
            <a style="color: black;" id="${i}" href="SearchS?tSID_SearchS=&indexS=${i}">${i}</a>
        </c:forEach>
        <script>
            document.getElementById('${indexS}').style.background = "#ff2562";
            document.getElementById('${indexS}').style.color = "#FFFFFF";
        </script>
    </div>
    <!--<a href="#">Learn More</a>-->
    <!--</div>-->
</div>