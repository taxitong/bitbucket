<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<form action="SubmitE" id="myEForm" method="POST">
    <c:if test="${sessionScope.LIST_QUES_FOR_EXAMINATION != null}">
        <c:if test="${not empty sessionScope.LIST_QUES_FOR_EXAMINATION}">
            <c:forEach var="ques" varStatus="counter" items="${sessionScope.LIST_QUES_FOR_EXAMINATION}">
                <div class="each-main-text">
                    <p class="subject-name">Q${counter.count}: ${ques.content}</p></br>
                    A. ${ques.optionA}</br> 
                    B. ${ques.optionB}</br> 
                    C. ${ques.optionC}</br> 
                    D. ${ques.optionD}</br></br> 
                    Answer:
                    <select name="tChosenAnswer_SubmitE${counter.count}" class="select">
                        <option value="">Pending..</option>
                        <option value="A">Option A</option>
                        <option value="B">Option B</option>
                        <option value="C">Option C</option>
                        <option value="D">Option D</option>
                    </select></br></br>
                </div> 
            </c:forEach>
            <input type="hidden" name="tEmail_SubmitE" value="${sessionScope.tEmail_SubmitE}"/>
            <input type="hidden" name="tQzID_SubmitE" value="${sessionScope.tQzID_SubmitE}"/>
            <input type="hidden" name="tStartTime_SubmitE" value="${sessionScope.tStartTime_SubmitE}"/>
            <input type="hidden" name="tSecondsToDoQuiz_SubmitE" value="${sessionScope.tSecondsToDoQuiz_SubmitE}"/>
            <input type="hidden" name="tQuesEachExam_SubmitE" value="${sessionScope.tQuesEachExam_SubmitE}"/>
            <input class="controlButton" type="submit" value="Submit all"/>
        </c:if>
        <c:if test="${empty sessionScope.LIST_QUES_FOR_EXAMINATION}">
            This subject has no quiz yet!!
        </c:if>
    </c:if>
    <c:if test="${sessionScope.LIST_QUES_FOR_EXAMINATION == null}">
        This subject has no quiz yet2!!
    </c:if>
</form>