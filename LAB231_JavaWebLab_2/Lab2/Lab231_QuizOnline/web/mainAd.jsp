<%-- 
    Document   : mainUs
    Created on : Jan 27, 2021, 3:18:21 PM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="sharedJSP/appMainCSS.jsp"/>
        <title>Main Ad</title>
    </head>
    <body>
        <section>
            <jsp:include page="sepJSP_AD/appMenuBarAd.jsp"/>
            <jsp:include page="sepJSP_AD/mainAd_SearchQs.jsp"/>
            <jsp:include page="sharedJSP/appSciAndTopButton.jsp"/>
        </section>
        <span class="menuicon" id="toggle" onclick="menuToggle()"></span>
        <script src="js/main.js" type="text/javascript"></script>
        <script src="js/find-qz.js" type="text/javascript"></script>
    </body>
</html>
