<%-- 
    Document   : viewHistory
    Created on : Mar 3, 2021, 6:16:36 AM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="sharedJSP/appMainCSS.jsp"/>
        <title>View History</title>
    </head>
    <body>
        <section>
            <jsp:include page="sepJSP_STU/appMenuBarStu.jsp"/>
            <div class="content">
                <jsp:include page="sepJSP_STU/appHelloStu.jsp"/>
                <div class="main-text">
                    <P class="title">
                        View History: 
                    </p>
                    <div class="each-main-text">
                        <P class="title">
                            Search Exam: 
                        </p>
                        <form action="ViewHistory" class="subject-name" method="POST">
                            <select name="tSID_ViewHistory" class="select">
                                <option value="">All</option>
                                <c:forEach var="subject" varStatus="counter" items="${sessionScope.LIST_ALLSUBJECT_FROM_ViewHistory}">
                                    <option value="${subject.subjectID}"  <c:if test="${sessionScope.tSID_ViewHistory == subject.subjectID}">selected</c:if>>
                                        ${subject.subjectID} - ${subject.subjectName}
                                    </option>
                                </c:forEach>
                            </select>
                            <input class="controlButton" type="submit" value="Search"/>
                        </form>
                    </div>
                    <p class="note">Latest exam display first !!</p>
                    <c:if test="${sessionScope.LIST_EXAM != null}">
                        <c:if test="${not empty sessionScope.LIST_EXAM}">
                            <c:forEach var="exam" varStatus="counter" items="${sessionScope.LIST_EXAM}">
                                <div class="each-main-text">
                                    <p class="subject-text">Subject <!--ExamID-->: ${exam.examID}</p></br>
                                    <p class="subject-text">Quiz's name<!--QuizID-->: ${exam.quizID}</p></br>
                                    </br>
                                    <p class="subject-highlight">Start Time: ${exam.startTime}</p></br>
                                    <p class="subject-highlight">Submit Time: ${exam.submitTime}</p></br>
                                    </br>
                                    <p class="subject-text">Total Ques: ${exam.totalQues}</p></br>
                                    <p class="subject-text">Total Correct: ${exam.totalCorrect}</p></br>
                                    <p class="subject-text">Grade: ${exam.result}/100</p></br>
                                </div>
                            </c:forEach>
                            <c:forEach begin="1" end="${endPageE}" var="i">
                                <a style="color: black;" id="${i}" href="ViewHistory?indexE=${i}&tSID_ViewHistory=${tSID_ViewHistory}">${i}</a>
                            </c:forEach>
                            <script>
                                document.getElementById('${indexE}').style.background = "#ff2562";
                                document.getElementById('${indexE}').style.color = "#FFFFFF";
                            </script>
                        </c:if>
                        <c:if test="${empty sessionScope.LIST_EXAM}">
                            <div class="each-main-text">
                                <div class="title">
                                    <font color='red'>You have not done any exam (with search info) yet!!</font></br>
                                </div>
                            </div>
                        </c:if>
                    </c:if>
                </div>
                <!--<a href="#">Learn More</a>-->
                <!--</div>-->
            </div>
            <jsp:include page="sharedJSP/appSciAndTopButton.jsp"/>
        </section>
        <span class="menuicon" id="toggle" onclick="menuToggle()"></span>
</html>
