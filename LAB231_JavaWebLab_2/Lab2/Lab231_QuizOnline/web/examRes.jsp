<%-- 
    Document   : examRes
    Created on : Mar 3, 2021, 5:11:58 AM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="sharedJSP/appMainCSS.jsp"/>
        <title>Exam Recent</title>
    </head>
    <body>
        <section>
            <jsp:include page="sepJSP_STU/appMenuBarStu.jsp"/>
            <div class="content">
                <jsp:include page="sepJSP_STU/appHelloStu.jsp"/>
                <div class="main-text">
                    <P class="title">
                        The lastest exam you completed's result:
                    </p>
                    </br>
                    <c:if test="${sessionScope.totalCorrect_EResPage != null && sessionScope.totalQues_EResPage != null && sessionScope.grade_EResPage != null}">
                        <%--<c:if test="${not empty sessionScope.totalCorrect_EResPage || not empty sessionScope.totalQues_EResPage || not empty sessionScope.grade_EResPage}">--%> 
                        <p class="subject-text">Subject: ${sessionScope.SID_SName_EResPage}</p></br>
                        <p class="subject-text">Quiz's name: ${sessionScope.quizName_EResPage}</p></br>

                        <p class="subject-text">Total: ${sessionScope.totalCorrect_EResPage}</p></br>
                        <p class="subject-text">Correct: ${sessionScope.totalQues_EResPage}</p></br>
                        <p class="subject-highlight">Final result: ${sessionScope.grade_EResPage}/100</p></br>
                        <%--</c:if>--%>
                    </c:if>
                    <c:if test="${sessionScope.totalCorrect_EResPage == null || sessionScope.totalQues_EResPage == null || sessionScope.grade_EResPage == null}">
                        <p class="subject-text">No quiz result recently</p> 
                    </c:if>
                </div>
                <!--<a href="#">Learn More</a>-->
                <!--</div>-->
            </div>
            <jsp:include page="sharedJSP/appSciAndTopButton.jsp"/>
        </section>
        <span class="menuicon" id="toggle" onclick="menuToggle()"></span>
    </body>
</html>
