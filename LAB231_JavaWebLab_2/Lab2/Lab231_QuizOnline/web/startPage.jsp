<%-- 
    Document   : loginn
    Created on : Jan 24, 2021, 9:46:50 PM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
        <link href="css/login.css" rel="stylesheet" type="text/css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Start Page</title>
    </head>
    <body>
        <h1>Quiz Online</h1></br>
        <div class="container" id="container">
            <div class="form-container sign-in-container">
                <form action="SignIn" method="POST">
                    <h1>Sign in</h1>
                    <!--<div class="social-container">
                        <a href="#" class="social"><i class="fab fa-facebook-f"></i></a>
                        <a href="#" class="social"><i class="fab fa-google-plus-g"></i></a>
                        <a href="#" class="social"><i class="fab fa-linkedin-in"></i></a>
                    </div>
                    <span>or use your account</span>-->
                    <input type="email" name="tEmail_SignIn" placeholder="Email" />
                    <input type="password" name="tPassword_SignIn" placeholder="Password" />
                    <!--<a href="#">Forgot your password?</a>-->
                    <c:if test="${sessionScope.INVALID_ACCOUNT != null}">
                        <p style="text-align: center; color: red;">${sessionScope.INVALID_ACCOUNT}</p>
                    </c:if>
                    </br>
                    <button class="login100-form-btn">Sign In</button>
                </form>
            </div>
            <div class="form-container sign-up-container">
                <form action="SignUp" onsubmit="return validateForm()" method="POST">
                    <h1>Create Account</h1>
                    <!--<div class="social-container">
                        <a href="#" class="social"><i class="fab fa-facebook-f"></i></a>
                        <a href="#" class="social"><i class="fab fa-google-plus-g"></i></a>
                        <a href="#" class="social"><i class="fab fa-linkedin-in"></i></a>
                    </div>
                    <span>or use your email for registration</span>-->
                    <input type="text" name="tDisplayName_SignUp" required="true" maxlength="50" placeholder="Name (Up to 50 characters)" value="${requestScope.tDisplayName_SignUp}"/>
                    <input type="email" name="tEmail_SignUp" required="true" placeholder="Email" value="${requestScope.tEmail_SignUp}"/>
                    <input type="password" name="tPassword_SignUp" id="pswd1" required="true" minlength="6" placeholder="Password (6+ characters)" />
                    <input type="password" name="tRePassword_SignUp"id="pswd2" required="true" minlength="6" placeholder="Re-enter password" />
                    <span id="passMess" style="color:red"></span>
                    </br>
                    <button class="login100-form-btn">Sign Up</button>
                </form>
            </div>
            <div class="overlay-container">
                <div class="overlay">
                    <div class="overlay-panel overlay-left">
                        <h1>Welcome Back!</h1>
                        <p>To keep connected with us please login with your personal info</p>
                        <button class="ghost" id="signIn">Sign In</button>
                    </div>
                    <div class="overlay-panel overlay-right">
                        <h1>Hello, Friend!</h1>
                        <p>Enter your personal details and start journey with us</p>
                        <button class="ghost" id="signUp">Sign Up</button>
                    </div>
                </div>
            </div>
        </div>
        <!--<footer>
            <p>
                Created with <i class="fa fa-heart"></i> by
                <a target="_blank" href="https://florin-pop.com">Florin Pop</a>
                - Read how I created this and how you can join the challenge
                <a target="_blank" href="https://www.florin-pop.com/blog/2019/03/double-slider-sign-in-up-form/">here</a>.
            </p>
        </footer>-->
        <script src="js/login.js" type="text/javascript"></script>
        <c:if test="${requestScope.duplicateEmail != null}">
            <script>
                alert("Email already exists, please use another email");
            </script>
        </c:if>
    </body>
</html>
