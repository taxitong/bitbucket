<%-- 
    Document   : viewQzStu
    Created on : Mar 8, 2021, 9:13:29 PM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="sharedJSP/appMainCSS.jsp"/>
        <title>View Subject Stu</title>
    </head>
    <body>
        <section>
            <jsp:include page="sepJSP_STU//appMenuBarStu.jsp"/>
            <div class="content">
                <jsp:include page="sepJSP_STU/appHelloStu.jsp"/>
                <div class="main-text">
                    <P class="title">
                        All Quiz of '${sessionScope.thisSID_From_SearchQz}':
                    </p>
                    <p class="note">- Order by Quiz Name ascending alphabetically (A - Z)</p>
                    <p class="note">- You must submit the Exam you are working on first before you can do a new Exam </p>
                    <c:if test="${sessionScope.LIST_QZ != null}">
                        <c:if test="${not empty sessionScope.LIST_QZ}">
                            <c:forEach var="quiz" varStatus="counter" items="${sessionScope.LIST_QZ}">
                                <div class="each-main-text">
                                    <p class="subject-text">${quiz.quizName}</p>
                                    </br></br>
                                    <p class="subject-text">Amount: ${quiz.quesEachExam} ques</p></br>
                                    <p class="subject-text">
                                        <c:if test="${quiz.noOfAllowedAttempt != 0}">
                                            Attempts allowed: ${quiz.noOfAllowedAttempt}
                                        </c:if>
                                        <c:if test="${quiz.noOfAllowedAttempt == 0}">
                                            Attempts allowed: Unlimited
                                        </c:if>
                                    </p>
                                    </br>
                                    <p class="hightlight">
                                        <c:if test="${quiz.secondsToDoQuiz != 0}">
                                            Time limit: ${quiz.secondsToDoQuiz} second(s)
                                        </c:if>
                                        <c:if test="${quiz.secondsToDoQuiz == 0}">
                                            Time limit: Unlimited
                                        </c:if>
                                    </p>
                                    </br>
                                    <p class="subject-text">Note: ${quiz.comment}</p></br>
                                    <br>
                                    <form action="GetQzToE" class="subject-name" method="POST">
                                        <input type="hidden" name="tNowSName_GetQzToE" value="${sessionScope.thisSID_From_SearchQz}"/>
                                        <input type="hidden" name="tNowQzName_GetQzToE" value="${quiz.quizName}"/>

                                        <input type="hidden" name="tQzID_GetQzToE" value="${quiz.quizID}"/>
                                        <input type="hidden" name="tSecondsToDoQuiz_GetQzToE" value="${quiz.secondsToDoQuiz}"/>
                                        <input type="hidden" name="tQuesEachExam_GetQzToE" value="${quiz.quesEachExam}"/>
                                        <input class="controlButton" type="submit" value="Do Exam"/>
                                    </form>
                                </div>
                            </c:forEach>
                            <c:forEach begin="1" end="${endPageQz}" var="i">
                                <a style="color: black;" id="${i}" href="SearchQz?indexQz=${i}&tSubjectID_SearchQz=${thisSID_From_SearchQz}">${i}</a>
                            </c:forEach>
                            <script>
                                document.getElementById('${indexQz}').style.background = "#ff2562";
                                document.getElementById('${indexQz}').style.color = "#FFFFFF";
                            </script>
                        </c:if>
                        <c:if test="${empty sessionScope.LIST_QZ}">
                            <jsp:include page="sepJSP_AD/viewQzAd_SearchNoQz.jsp"/>
                        </c:if>
                    </c:if>
                </div>
                <!--<a href="#">Learn More</a>-->
                <!--</div>-->
            </div>
            <jsp:include page="sharedJSP/appSciAndTopButton.jsp"/>
        </section>
        <span class="menuicon" id="toggle" onclick="menuToggle()"></span>
        <script src="js/main.js" type="text/javascript"></script>
    </body>
</html>
