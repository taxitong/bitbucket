<%-- 
    Document   : moveQs
    Created on : Mar 9, 2021, 11:10:07 PM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="sharedJSP/appMainCSS.jsp"/>
        <title>Move Qs</title>
    </head>
    <body>
        <section>
            <jsp:include page="sepJSP_AD/appMenuBarAd.jsp"/>
            <div class="content">
                <jsp:include page="sepJSP_AD/appHelloAd.jsp"/>
                <div class="main-text">
                    <P class="title">
                        Move this Question to: 
                    </p>
                    <div class="each-main-text">
                        <form action="PreMoveQs" class="subject-text" id="SearchQzForm" method="POST">
                            <!--<P class="title">Search Question: </p>-->
                            Subject: 
                            <select name="tSID_PreMoveQs" style="text-transform: capitalize;" id="tSID_SearchQz" onchange="autoSubmitSubjectID()">
                                <c:forEach var="subject" varStatus="counter" items="${sessionScope.LIST_ALLSUBJECT}">
                                    <option value="${subject.subjectID}" <c:if test="${sessionScope.tSID_From_PreMoveQs == subject.subjectID}">selected="true"</c:if>>
                                        ${subject.subjectID} - ${subject.subjectName} (${subject.statusID})
                                    </option>
                                </c:forEach>
                            </select>
                            <input type="hidden" name="tQsID_MoveQs" value="${sessionScope.tQsID_From_PreMoveQs}"/><!--???-->
                        </form>
                        <form action="MoveQs" class="subject-text" method="POST">
                            <!--for testing, change this hidden input  back to text-->
                            <input type="hidden" name="tSID_MoveQs" value="${sessionScope.tSID_From_PreMoveQs}"/>
                            <!--for testing, change this hidden input  back to text-->
                            <input type="hidden" name="tQsID_MoveQs" value="${sessionScope.tQsID_From_PreMoveQs}"/><!--???-->
                            Quiz:
                            <c:if test="${not empty sessionScope.listQzID_From_PreMoveQs}">
                                <select name="tQzID_MoveQs" style="text-transform: capitalize;" <c:if test="${empty sessionScope.tSID_From_PreMoveQs}">disabled</c:if>>         
                                    <c:forEach var="quiz" varStatus="counter" items="${sessionScope.listQzID_From_PreMoveQs}">
                                        <option value="${quiz.quizID}" <c:if test="${sessionScope.tQzID_From_PreMoveQs == quiz.quizID}">selected</c:if>>
                                            ${quiz.quizName}
                                        </option>
                                    </c:forEach>
                                </select> 
                            </c:if>
                            <%--<c:if test="${empty sessionScope.listQzID_From_PreMoveQs || empty sessionScope.tSID_From_PreMoveQs}">--%>
                            <!--<input type="hidden" name="tQzID_MoveQsQs" value="" />-->
                            <%--</c:if>--%>
                            <c:if test="${empty sessionScope.listQzID_From_PreMoveQs}">
                                <select style="color: red;" disabled>
                                    <option>No Quiz</option>
                                </select>
                            </c:if>
                            </br></br>
                            <input class="controlButton" type="submit" <c:if test="${empty sessionScope.listQzID_From_PreMoveQs}">disabled style="opacity: 0.5;"</c:if> onclick="return confirm('Move question. Are you sure?')" value="Move"/>
                            </form>
                        </div>
                    </div>
                </div>
            <jsp:include page="sharedJSP/appSciAndTopButton.jsp"/>
        </section>
        <span class="menuicon" id="toggle" onclick="menuToggle()"></span>
        <script src="js/main.js" type="text/javascript"></script>
        <script src="js/find-qz.js" type="text/javascript"></script>
    </body>
</html>
