<%-- 
    Document   : exam
    Created on : Feb 28, 2021, 8:44:46 PM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="sharedJSP/appMainCSS.jsp"/>
        <title>Exam</title>
    </head>
    <body>
        <section>
            <jsp:include page="sepJSP_STU/appMenuBarStu.jsp"/>
            <div class="content">
                <jsp:include page="sepJSP_STU/appHelloStu.jsp"/>
                <div class="main-text">
                    <P class="title">
                        Exam '${sessionScope.tNowSName_From_GetQzToE} - ${sessionScope.tNowQzName_From_GetQzToE}'
                    </p>
                    <p style="color: red; display: inline;" id="demo"></p></br>
                    <c:if test="${sessionScope.isExamining != null}">
                        <jsp:include page="sepJSP_STU/exam_EDetails.jsp"/>
                    </c:if>
                    <c:if test="${sessionScope.isExamining == null}">
                        <c:redirect url="mainStu.jsp"/>
                    </c:if>
                </div>
                <!--<a href="#">Learn More</a>-->
                <!--</div>-->
            </div>
            <jsp:include page="sharedJSP/appSciAndTopButton.jsp"/>
        </section>
        <span class="menuicon" id="toggle" onclick="menuToggle()"></span>
        <script>
            // Set the date we're counting down to
            var countDownDate = new Date().getTime() + ${sessionScope.tSecondsToDoQuiz_SubmitE} * 1000;
            // Update the count down every 1 second
            var x = setInterval(function () {
                // Get today's date and time
                var now = new Date().getTime();
                // Find the distance between now and the count down date
                var distance = countDownDate - now;
                // Time calculations for days, hours, minutes and seconds
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                // Output the result in an element with id="demo"
                document.getElementById("demo").innerHTML = "Time left: " + hours + "h "
                        + minutes + "m " + seconds + "s ";
                // If the count down is over, write some text 
                if (distance < 0) {
                    clearInterval(x);
                    document.getElementById("demo").innerHTML = "EXPIRED";
                    document.getElementById("myEForm").submit();
                }
            }, 1000);
        </script>
    </body>
</html>