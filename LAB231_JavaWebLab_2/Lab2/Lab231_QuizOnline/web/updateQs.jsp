<%-- 
    Document   : updateQ
    Created on : Feb 26, 2021, 9:49:36 AM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="sharedJSP/appMainCSS.jsp"/>
        <title>Update Ques</title>
    </head>
    <body>
        <section>
            <jsp:include page="sepJSP_AD/appMenuBarAd.jsp"/>
            <div class="content">
                <jsp:include page="sepJSP_AD/appHelloAd.jsp"/>
                <div class="main-text">
                    <P class="title">
                        Update question
                    </p>
                    <div class="each-main-text">
                        <form action="UpdateQs" onsubmit="return checkDuplicateOption()" method="POST">
                            <input type="hidden" name="tQsID_UpdateQs" value="${sessionScope.QUES_TO_UPDATE.quesID}"/>
                            Question's status: 
                            <select name="tQsStatusID_UpdateQs" class="select">
                                <c:forEach var="quizStatusID" varStatus="counter" items="${sessionScope.LIST_STATUS_ID}">
                                    <c:if test="${sessionScope.QUES_TO_UPDATE.statusID == quizStatusID}">
                                        <option value="${quizStatusID}" class="hightlight" selected="true">${quizStatusID} (Base)</option>
                                    </c:if>
                                    <c:if test="${sessionScope.QUES_TO_UPDATE.statusID != quizStatusID}">
                                        <option value="${quizStatusID}">${quizStatusID}</option>
                                    </c:if>
                                </c:forEach>
                            </select>
                            </br></br>
                            Content:
                            <textarea class="custom-text-input" style="resize: vertical;" name="tQsContent_UpdateQs" maxlength="3000" rows="4" cols="50" required>${sessionScope.QUES_TO_UPDATE.content}</textarea></br>
                            Option A: <span id="duplicateOptionA" style="color:red"></span> 
                            <input id="optA" class="custom-text-input" name="tQsOptionA_UpdateQs" type="text" maxlength="3000" required value="${sessionScope.QUES_TO_UPDATE.optionA}"/></br>
                            Option B: <span id="duplicateOptionB" style="color:red"></span>
                            <input id="optB" class="custom-text-input" name="tQsOptionB_UpdateQs" type="text" maxlength="3000" required value="${sessionScope.QUES_TO_UPDATE.optionB}"/></br>
                            Option C: <span id="duplicateOptionCD" style="color:red"></span>
                            <input class="custom-text-input" id="optC" name="tQsOptionC_UpdateQs" type="text" maxlength="3000" required value="${sessionScope.QUES_TO_UPDATE.optionC}"/></br>
                            Option D:  
                            <input class="custom-text-input" id="optD" name="tQsOptionD_UpdateQs" type="text" maxlength="3000" required value="${sessionScope.QUES_TO_UPDATE.optionD}"/></br>
                            Correct answer:
                            <jsp:include page="sepJSP_AD/updateQ_SelectAnswer.jsp"/></br>
                            <p class="note"> (*) If update successful return to Home page</p></br>
                            <input class="controlButton" type="submit" name="btnAction" onclick="return confirm('Update quiz. Are you sure?')" value="Update Quiz"/>
                        </form>
                    </div>
                </div>
            </div>
            <jsp:include page="sharedJSP/appSciAndTopButton.jsp"/>
            <script src="js/validate-ques.js" type="text/javascript"></script>
        </section>
        <span class="menuicon" id="toggle" onclick="menuToggle()"></span>
    </body>
</html>
