function checkDuplicateOption() {//https://www.javatpoint.com/confirm-password-validation-in-javascript
    //collect form data in JavaScript variables
    var optA = document.getElementById("optA").value;
    var optB = document.getElementById("optB").value;
    var optC = document.getElementById("optC").value;
    var optD = document.getElementById("optD").value;
    var res = true;
    //Option A
    if (optA == optB || optA == optC || optA == optD) {
        if (optA == optB) {
            document.getElementById("duplicateOptionA").innerHTML = "Option A and B is the same";
        }
        if (optA == optC) {
            document.getElementById("duplicateOptionA").innerHTML = "Option A and C is the same";
        }
        if (optA == optD) {
            document.getElementById("duplicateOptionA").innerHTML = "Option A and D is the same";
        }
        res = false;
    } else {
        document.getElementById("duplicateOptionA").innerHTML = "";
    }
    //Option B
    if (optB == optC || optB == optD) {
        if (optB == optC) {
            document.getElementById("duplicateOptionB").innerHTML = "Option B and C is the same";
        }
        if (optB == optD) {
            document.getElementById("duplicateOptionB").innerHTML = "Option B and D is the same";
        }
        res = false;
    } else {
        document.getElementById("duplicateOptionB").innerHTML = "";
    }
    //Option C
    if (optC == optD) {
        document.getElementById("duplicateOptionCD").innerHTML = "Option C and D is the same";
        res = false;
    } else {
        document.getElementById("duplicateOptionCD").innerHTML = "";
    }
    if (res == false) {
        return false;
    }
}