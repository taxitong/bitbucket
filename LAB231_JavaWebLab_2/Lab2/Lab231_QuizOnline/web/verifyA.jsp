<%-- 
    Document   : mainUs
    Created on : Jan 27, 2021, 3:18:21 PM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="sharedJSP/appMainCSS.jsp"/>
        <title>Verify Acc</title>
    </head>
    <body>
        <section>
            <jsp:include page="sepJSP_AD/appMenuBarAd.jsp"/>
            <div class="content">
                <jsp:include page="sepJSP_AD/appHelloAd.jsp"/>
                <div class="main-text">
                    <P class="title">
                        Account Verification 
                    </p>
                    <p class="note">Order by account's name ascending  alphabetically (A - Z)</p>
                    <c:forEach var="acc" varStatus="counter" items="${sessionScope.LIST_ACCOUNT}">
                        <div class="each-main-text">
                            Email: ${acc.email}</br>
                            Name: ${acc.displayName}</br>
                            <form action="VerifyA" class="subject-name" method="GET">
                                <input type="hidden" name="tAccEmail" value="${acc.email}"/>
                                Account's status:
                                <select name="tQStatusID" class="select">
                                    <c:forEach var="accStatus" varStatus="counter" items="${sessionScope.LIST_STATUS}">
                                        <option value="${accStatus}"<c:if test="${accStatus == acc.statusID}">class="hightlight" selected</c:if> >
                                            ${accStatus}
                                        </option>
                                    </c:forEach>
                                </select>
                                <input class="controlButton" type="submit" value="Change" onclick="return confirm('Change Email: ${acc.email} status?')"/>
                            </form>
                        </div>
                    </c:forEach>
                    <c:forEach begin="1" end="${endPageA}" var="i">
                        <a style="color: black;" id="${i}" href="PreVerifyA?indexA=${i}">${i}</a>
                    </c:forEach>
                    <script>
                        document.getElementById('${indexA}').style.background = "#ff2562";
                        document.getElementById('${indexA}').style.color = "#FFFFFF";
                    </script>
                    <jsp:include page="sharedJSP/appSciAndTopButton.jsp"/>
                </div>
        </section>
        <span class="menuicon" id="toggle" onclick="menuToggle()"></span>
    </body>
</html>
