<%-- 
    Document   : createQs
    Created on : Feb 26, 2021, 1:50:59 PM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="sharedJSP/appMainCSS.jsp"/>
        <title>Create Quiz</title>
    </head>
    <body>
        <section>
            <jsp:include page="sepJSP_AD/appMenuBarAd.jsp"/>
            <div class="content">
                <jsp:include page="sepJSP_AD/appHelloAd.jsp"/>
                <div class="main-text">
                    <P class="title">
                        Create question:
                    </p>
                    <div class="each-main-text">
                        <form action="CreateQs" onsubmit="return checkDuplicateOption()" method="POST">
                            <c:if test="${param.tSID_createQsJSP != null}">
                                <!--(Param)</br>-->
                                Subject: ${param.tSID_createQsJSP} (${param.tSName_createQsJSP}) - Quiz: ${param.tQzName_createQsJSP}</br>
                                <input type="hidden" name="tSID_CreateQs" value="${param.tSID_createQsJSP}"/>
                                <input type="hidden" name="tSName_CreateQs" value="${param.tSName_createQsJSP}"/>
                                <input type="hidden" name="tQzID_CreateQs" value="${param.tQzID_createQsJSP}"/>
                                <input type="hidden" name="tQzName_CreateQs" value="${param.tQzName_createQsJSP}"/>
                            </c:if>
                            <c:if test="${param.tSID_createQsJSP == null}">
                                <!--(Session)</br>-->
                                Subject: ${sessionScope.tSID_From_CreateQs} (${sessionScope.tSName_From_CreateQs}) - Quiz: ${sessionScope.tQzName_From_CreateQs}</br>
                                <input type="hidden" name="tSID_CreateQs" value="${sessionScope.tSID_From_CreateQs}"/>
                                <input type="hidden" name="tSName_CreateQs" value="${sessionScope.tSName_From_CreateQs}"/>
                                <input type="hidden" name="tQzID_CreateQs" value="${sessionScope.tQzID_From_CreateQs}"/>
                                <input type="hidden" name="tQzName_CreateQs" value="${sessionScope.tQzName_From_CreateQs}"/>
                            </c:if>
                            </br>
                            Question's status: 
                            <select name="tQsStatusID_CreateQs" class="select">
                                <c:forEach var="quizStatusID" varStatus="counter" items="${sessionScope.LIST_STATUS_ID}">
                                    <option value="${quizStatusID}">${quizStatusID}</option>
                                </c:forEach>
                            </select>
                            </br></br>
                            Content:
                            <textarea class="custom-text-input" style="resize: vertical;" name="tQsContent_CreateQs" maxlength="3000" rows="4" cols="50" required></textarea></br>
                            Option A: <span id="duplicateOptionA" style="color:red"></span> 
                            <input id="optA" class="custom-text-input" name="tQsOptionA_CreateQs" type="text" maxlength="3000" required/></br>
                            Option B: <span id="duplicateOptionB" style="color:red"></span>
                            <input id="optB" class="custom-text-input" name="tQsOptionB_CreateQs" type="text" maxlength="3000" required/></br>
                            Option C: <span id="duplicateOptionCD" style="color:red"></span>
                            <input class="custom-text-input" id="optC" name="tQsOptionC_CreateQs" type="text" maxlength="3000" required/></br>
                            Option D:  
                            <input class="custom-text-input" id="optD" name="tQsOptionD_CreateQs" type="text" maxlength="3000" required/></br>
                            Correct answer:
                            <select class="custom-text-input" name="tQsAnswer_CreateQs" class="select">
                                <option value="A">Option A</option>
                                <option value="B">Option B</option>
                                <option value="C">Option C</option>
                                <option value="D">Option D</option>
                            </select></br>
                            <p class="note"> (*) If create successful, refresh data and prepare to create a new question </p></br>
                            <input class="controlButton" type="submit" name="btnAction" value="Create Quiz"/>
                        </form>
                    </div>
                </div>
            </div>
            <jsp:include page="sharedJSP/appSciAndTopButton.jsp"/>
        </section>
        <span class="menuicon" id="toggle" onclick="menuToggle()"></span>
        <script src="js/main.js" type="text/javascript"></script>
        <script src="js/validate-ques.js" type="text/javascript"></script>
    </body>
</html>
