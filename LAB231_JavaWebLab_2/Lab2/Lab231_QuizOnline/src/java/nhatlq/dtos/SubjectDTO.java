/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.dtos;

import java.io.Serializable;

/**
 *
 * @author Admin
 */
public class SubjectDTO implements Serializable {

    private String subjectID;
    private String subjectName;
    private String statusID;

    public SubjectDTO() {
    }

    public SubjectDTO(String subjectID, String subjectName, String statusID) {
        this.subjectID = subjectID;
        this.subjectName = subjectName;
        this.statusID = statusID;
    }

    public String getSubjectID() {
        return subjectID;
    }

    public void setSubjectID(String subjectID) {
        this.subjectID = subjectID;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getStatusID() {
        return statusID;
    }

    public void setStatusID(String statusID) {
        this.statusID = statusID;
    }

}
