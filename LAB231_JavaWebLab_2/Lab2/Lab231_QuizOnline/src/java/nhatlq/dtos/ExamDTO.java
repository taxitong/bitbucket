/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.dtos;

import java.io.Serializable;

/**
 *
 * @author Admin
 */
public class ExamDTO implements Serializable {

//    private String examID;
//    private String subjectID;
//    private String startTime;
//    private String submitTime;
//
//    private String email;
//    private int totalQues;
//    private int totalCorrect;
    private String examID;
    private String quizID;
    private String startTime;
    private String submitTime;

    private String email;
    private int totalQues;
    private int totalCorrect;
    private int result;

    public ExamDTO() {
    }

    public ExamDTO(String examID, String quizID, String startTime, String submitTime, String email, int totalQues, int totalCorrect, int result) {
        this.examID = examID;
        this.quizID = quizID;
        this.startTime = startTime;
        this.submitTime = submitTime;
        this.email = email;
        this.totalQues = totalQues;
        this.totalCorrect = totalCorrect;
        this.result = result;
    }

    public String getExamID() {
        return examID;
    }

    public void setExamID(String examID) {
        this.examID = examID;
    }

    public String getQuizID() {
        return quizID;
    }

    public void setQuizID(String quizID) {
        this.quizID = quizID;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getSubmitTime() {
        return submitTime;
    }

    public void setSubmitTime(String submitTime) {
        this.submitTime = submitTime;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getTotalQues() {
        return totalQues;
    }

    public void setTotalQues(int totalQues) {
        this.totalQues = totalQues;
    }

    public int getTotalCorrect() {
        return totalCorrect;
    }

    public void setTotalCorrect(int totalCorrect) {
        this.totalCorrect = totalCorrect;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    
}
