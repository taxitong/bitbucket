/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.dtos;

import java.io.Serializable;

/**
 *
 * @author Admin
 */
public class QuizDTO implements Serializable {

    private String quizID;
    private String subjectID;
    private String quizName;
    
    private int secondsToDoQuiz;
    private int quesEachExam;
    private int noOfAllowedAttempt;
    private String comment;

    private String statusID;

    public QuizDTO() {
    }

    public QuizDTO(String quizID, String subjectID, String quizName, int secondsToDoQuiz, int quesEachExam, int noOfAllowedAttempt, String comment, String statusID) {
        this.quizID = quizID;
        this.subjectID = subjectID;
        this.quizName = quizName;
        this.secondsToDoQuiz = secondsToDoQuiz;
        this.quesEachExam = quesEachExam;
        this.noOfAllowedAttempt = noOfAllowedAttempt;
        this.comment = comment;
        this.statusID = statusID;
    }

    public String getQuizID() {
        return quizID;
    }

    public void setQuizID(String quizID) {
        this.quizID = quizID;
    }

    public String getSubjectID() {
        return subjectID;
    }

    public void setSubjectID(String subjectID) {
        this.subjectID = subjectID;
    }

    public String getQuizName() {
        return quizName;
    }

    public void setQuizName(String quizName) {
        this.quizName = quizName;
    }

    public int getSecondsToDoQuiz() {
        return secondsToDoQuiz;
    }

    public void setSecondsToDoQuiz(int secondsToDoQuiz) {
        this.secondsToDoQuiz = secondsToDoQuiz;
    }

    public int getQuesEachExam() {
        return quesEachExam;
    }

    public void setQuesEachExam(int quesEachExam) {
        this.quesEachExam = quesEachExam;
    }

    public int getNoOfAllowedAttempt() {
        return noOfAllowedAttempt;
    }

    public void setNoOfAllowedAttempt(int noOfAllowedAttempt) {
        this.noOfAllowedAttempt = noOfAllowedAttempt;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getStatusID() {
        return statusID;
    }

    public void setStatusID(String statusID) {
        this.statusID = statusID;
    }

    
}
