/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.dtos;

import java.io.Serializable;

/**
 *
 * @author Admin
 */
public class AccountDTO implements Serializable {

    private String email;
    private String displayName;
    private String password;
    private String roleID;
    private String statusID;

    public AccountDTO() {
    }

    //for everything except sign in
    public AccountDTO(String email, String displayName, String password, String roleID, String statusID) {
        this.email = email;
        this.displayName = displayName;
        this.password = password;
        this.roleID = roleID;
        this.statusID = statusID;
    }

    //for sign in
    public AccountDTO(String email, String displayName, String roleID, String statusID) {
        this.email = email;
        this.displayName = displayName;
        this.roleID = roleID;
        this.statusID = statusID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRoleID() {
        return roleID;
    }

    public void setRoleID(String roleID) {
        this.roleID = roleID;
    }

    public String getStatusID() {
        return statusID;
    }

    public void setStatusID(String statusID) {
        this.statusID = statusID;
    }
}
