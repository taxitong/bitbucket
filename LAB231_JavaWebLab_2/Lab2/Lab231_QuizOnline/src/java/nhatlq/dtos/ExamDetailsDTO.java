/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.dtos;

import java.io.Serializable;

/**
 *
 * @author Admin
 */
public class ExamDetailsDTO implements Serializable {

    private String examDetailsID;
    private String examID;

    private String quesID;
    private String quizID_Save;
    private String content_Save;
    private String optionA_Save;
    private String optionB_Save;
    private String optionC_Save;
    private String optionD_Save;
    private String correctAnswer_Save;

    private String chosenAnswer;
    private boolean isCorrect;

    public ExamDetailsDTO() {
    }

    public ExamDetailsDTO(String examDetailsID, String examID, String quesID, String quizID_Save, String content_Save, String optionA_Save, String optionB_Save, String optionC_Save, String optionD_Save, String correctAnswer_Save, String chosenAnswer, boolean isCorrect) {
        this.examDetailsID = examDetailsID;
        this.examID = examID;
        this.quesID = quesID;
        this.quizID_Save = quizID_Save;
        this.content_Save = content_Save;
        this.optionA_Save = optionA_Save;
        this.optionB_Save = optionB_Save;
        this.optionC_Save = optionC_Save;
        this.optionD_Save = optionD_Save;
        this.correctAnswer_Save = correctAnswer_Save;
        this.chosenAnswer = chosenAnswer;
        this.isCorrect = isCorrect;
    }

    public String getExamDetailsID() {
        return examDetailsID;
    }

    public void setExamDetailsID(String examDetailsID) {
        this.examDetailsID = examDetailsID;
    }

    public String getExamID() {
        return examID;
    }

    public void setExamID(String examID) {
        this.examID = examID;
    }

    public String getQuesID() {
        return quesID;
    }

    public void setQuesID(String quesID) {
        this.quesID = quesID;
    }

    public String getQuizID_Save() {
        return quizID_Save;
    }

    public void setQuizID_Save(String quizID_Save) {
        this.quizID_Save = quizID_Save;
    }

    public String getContent_Save() {
        return content_Save;
    }

    public void setContent_Save(String content_Save) {
        this.content_Save = content_Save;
    }

    public String getOptionA_Save() {
        return optionA_Save;
    }

    public void setOptionA_Save(String optionA_Save) {
        this.optionA_Save = optionA_Save;
    }

    public String getOptionB_Save() {
        return optionB_Save;
    }

    public void setOptionB_Save(String optionB_Save) {
        this.optionB_Save = optionB_Save;
    }

    public String getOptionC_Save() {
        return optionC_Save;
    }

    public void setOptionC_Save(String optionC_Save) {
        this.optionC_Save = optionC_Save;
    }

    public String getOptionD_Save() {
        return optionD_Save;
    }

    public void setOptionD_Save(String optionD_Save) {
        this.optionD_Save = optionD_Save;
    }

    public String getCorrectAnswer_Save() {
        return correctAnswer_Save;
    }

    public void setCorrectAnswer_Save(String correctAnswer_Save) {
        this.correctAnswer_Save = correctAnswer_Save;
    }

    public String getChosenAnswer() {
        return chosenAnswer;
    }

    public void setChosenAnswer(String chosenAnswer) {
        this.chosenAnswer = chosenAnswer;
    }

    public boolean isIsCorrect() {
        return isCorrect;
    }

    public void setIsCorrect(boolean isCorrect) {
        this.isCorrect = isCorrect;
    }

   
}
