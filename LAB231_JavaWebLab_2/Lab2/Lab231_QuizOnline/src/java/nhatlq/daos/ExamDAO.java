/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.naming.NamingException;
import nhatlq.dtos.ExamDTO;
import nhatlq.utils.DBConnection;

/**
 *
 * @author Admin
 */
public class ExamDAO {

    private Connection conn;
    private PreparedStatement preStm;
    private ResultSet rs;

    public ExamDAO() {
        conn = null;
        preStm = null;
        rs = null;
    }

    private void closeConnection3() throws SQLException {
        if (conn != null) {
            conn.close();
        }
        if (preStm != null) {
            preStm.close();
        }
        if (rs != null) {
            rs.close();
        }
    }

    private void closeConnection2() throws SQLException {
        if (conn != null) {
            conn.close();
        }
        if (preStm != null) {
            preStm.close();
        }
    }

    public boolean insertExam(ExamDTO exam) throws SQLException, NamingException {
        boolean result = false;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "INSERT INTO tblExam(examID, quizID, startTime, submitTime, email, totalQues, totalCorrect, result) VALUES(?,?,?,?,?,?,?,?)";
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, exam.getExamID());
                preStm.setString(2, exam.getQuizID());
                preStm.setString(3, exam.getStartTime());
                preStm.setString(4, exam.getSubmitTime());
                preStm.setString(5, exam.getEmail());
                preStm.setInt(6, exam.getTotalQues());
                preStm.setInt(7, exam.getTotalCorrect());
                preStm.setInt(8, exam.getResult());
                result = preStm.executeUpdate() != 0;
            }
        } finally {
            closeConnection2();
        }
        return result;
    }

    public boolean updateAvailableExam(String examID, int totalCorrect, int result) throws SQLException, NamingException {
        boolean res = false;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "UPDATE tblExam SET totalCorrect=?, result=? WHERE examID=?";
                preStm = conn.prepareStatement(sql);
                preStm.setInt(1, totalCorrect);
                preStm.setInt(2, result);
                preStm.setString(3, examID);
                res = preStm.executeUpdate() > 0;
            }
        } finally {
            closeConnection3();
        }
        return res;
    }

    private String countExam_GenerateSQL(String subjectID) {
        return subjectID.isEmpty()
                ? "SELECT COUNT(examID) FROM tblExam ed INNER JOIN tblQuiz q ON ed.quizID = q.quizID WHERE email=? "
                + "AND q.subjectID LIKE ?"
                : "SELECT COUNT(examID) FROM tblExam ed INNER JOIN tblQuiz q ON ed.quizID = q.quizID WHERE email=? "
                + "AND q.subjectID=?";
    }

    public int countExam(String email, String subjectID) throws SQLException, NamingException {
        int result = 0;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = countExam_GenerateSQL(subjectID);
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, email);
                preStm.setString(2, subjectID.isEmpty() ? ("%%" + subjectID + "%%") : subjectID);
                rs = preStm.executeQuery();
                if (rs.next()) {
                    result = rs.getInt(1);
                }
            }
        } finally {
            closeConnection3();
        }
        return result;
    }

    private String getAllExam_GenerateSQL(String subjectID) {
        return subjectID.isEmpty()
                ? "SELECT "
                + "examID, quizID, startTime, submitTime, totalQues, totalCorrect, result "
                + "FROM (SELECT ROW_NUMBER() OVER (ORDER BY startTime DESC) AS r, "
                + "examID, ed.quizID, startTime, submitTime, totalQues, totalCorrect, result "
                + "FROM tblExam ed INNER JOIN tblQuiz q ON ed.quizID = q.quizID WHERE email=? AND q.subjectID LIKE ?) as x "
                + "WHERE r BETWEEN ?*3-2 AND ?*3"
                : "SELECT "
                + "examID, quizID, startTime, submitTime, totalQues, totalCorrect, result "
                + "FROM (SELECT ROW_NUMBER() OVER (ORDER BY startTime DESC) AS r, "
                + "examID, ed.quizID, startTime, submitTime, totalQues, totalCorrect, result "
                + "FROM tblExam ed INNER JOIN tblQuiz q ON ed.quizID = q.quizID WHERE email=? AND q.subjectID=?) as x "
                + "WHERE r BETWEEN ?*3-2 AND ?*3";
    }

    public List<ExamDTO> getAllExam(String email, String subjectID, int indexE) throws SQLException, NamingException {
        List<ExamDTO> res = new ArrayList<>();
        try {
            conn = DBConnection.getConnection();
            String sql = getAllExam_GenerateSQL(subjectID);
            preStm = conn.prepareStatement(sql);
            preStm.setString(1, email);
            preStm.setString(2, subjectID.isEmpty() ? ("%%" + subjectID + "%%") : subjectID);
            preStm.setInt(3, indexE);
            preStm.setInt(4, indexE);
            rs = preStm.executeQuery();
            while (rs.next()) {
                String examID = rs.getString("examID");
                String quizID = rs.getString("quizID");
                String startTime = rs.getString("startTime");
                String submitTime = rs.getString("submitTime");

                int totalQues = rs.getInt("totalQues");
                int totalCorrect = rs.getInt("totalCorrect");
                int result = rs.getInt("result");

                res.add(new ExamDTO(examID, quizID, startTime, submitTime, email, totalQues, totalCorrect, result));
            }
        } finally {
            closeConnection3();
        }
        return res;
    }
}
