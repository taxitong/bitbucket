/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.daos;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.naming.NamingException;
import nhatlq.dtos.AccountDTO;
import nhatlq.utils.DBConnection;
import nhatlq.utils.Encryption;

/**
 *
 * @author Admin
 */
public class AccountDAO {

    private Connection conn;
    private PreparedStatement preStm;
    private ResultSet rs;
    //attributes in the database 
    private static final String ACTIVE_STATUS_ID = "active";

    public AccountDAO() {
        conn = null;
        preStm = null;
        rs = null;
    }

    public void closeConnection3() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (preStm != null) {
            preStm.close();
        }
        if (conn != null) {
            conn.close();
        }
    }

    public void closeConnection2() throws SQLException {
        if (preStm != null) {
            preStm.close();
        }
        if (conn != null) {
            conn.close();
        }
    }

    public AccountDTO checkLogin(String email, String password) throws SQLException, NamingException, NoSuchAlgorithmException {
        AccountDTO accountDto = null;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "SELECT displayName, roleID "
                        + "FROM tblAccount WHERE "
                        + "email=? AND password=? AND statusID=?";
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, email);
                Encryption en = new Encryption();
                String newPassword = en.toHexString(en.getSHA(password));
                preStm.setString(2, newPassword);
                preStm.setString(3, ACTIVE_STATUS_ID);
                rs = preStm.executeQuery();
                if (rs.next()) {
                    //String email;
                    String displayName = rs.getString("displayName");
                    //String password;
                    String roleID = rs.getString("roleID");
                    String statusID = ACTIVE_STATUS_ID;
                    accountDto = new AccountDTO(email, displayName, roleID, statusID);
                }
            }
        } finally {
            closeConnection3();
        }
        return accountDto;
    }

    public boolean insert(AccountDTO acc) throws SQLException, NamingException, NoSuchAlgorithmException {
        boolean check = false;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "INSERT INTO tblAccount(email, displayName, password, roleID, statusID) VALUES(?,?,?,?,?)";
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, acc.getEmail());
                preStm.setString(2, acc.getDisplayName());
                Encryption en = new Encryption();
                String newPassword = en.toHexString(en.getSHA(acc.getPassword()));
                preStm.setString(3, newPassword);
                preStm.setString(4, acc.getRoleID());
                preStm.setString(5, acc.getStatusID());
                check = preStm.executeUpdate() != 0;
            }
        } finally {
            closeConnection3();
        }
        return check;
    }

    public boolean checkExistMail(String email) throws SQLException, NamingException {
        boolean result = false;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "SELECT email FROM tblAccount WHERE email=?";
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, email);
                rs = preStm.executeQuery();
                if (rs.next()) {
                    result = true;
                }
            }
        } finally {
            closeConnection3();
        }
        return result;
    }

    /*generate*/
    String generateSQL_SearchAccount(String email, int size) {
        //declare
        String condition1 = "email LIKE ? ";
        String allConditions;
        //replace
        String WB = "WHERE r BETWEEN ?*x-(x-1) AND ?*x";
        String WHERE_BETWEEN = WB.replace("x", size + "");
        //check condition
        //
        allConditions = condition1;
        //form sql
        String sql
                = "SELECT "
                + "email, displayName, roleID, statusID "
                + "FROM (SELECT ROW_NUMBER() OVER (ORDER BY displayName ASC) AS r, "
                + "email, displayName, roleID, statusID "
                + "FROM tblAccount WHERE "
                + allConditions
                + ") as x " + WHERE_BETWEEN;
        return sql;
    }

    public List<AccountDTO> searchAccount(String email, int index_A, int size) throws SQLException, NamingException {
        List<AccountDTO> result = new ArrayList<>();
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = generateSQL_SearchAccount(email, size);
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, "%" + email + "%");
                preStm.setInt(2, index_A);
                preStm.setInt(3, index_A);
                rs = preStm.executeQuery();
                while (rs.next()) {
                    String emailTmp = rs.getString("email");
                    String displayName = rs.getString("displayName");
                    //String password = rs.getString("password");
                    String roleID = rs.getString("roleID");
                    String statusID = rs.getString("statusID");
                    result.add(new AccountDTO(emailTmp, displayName, roleID, statusID));
                }
            }
        } finally {
            closeConnection3();
        }
        return result;
    }

    /*generate*/
    String generateSQL_CountAccount() {
        return "SELECT count(email) FROM tblAccount WHERE email LIKE ?";
    }

    public int countAccount(String email) throws SQLException, NamingException {
        int result = 0;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = generateSQL_CountAccount();
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, "%" + email + "%");
                rs = preStm.executeQuery();
                if (rs.next()) {
                    result = rs.getInt(1);
                }
            }
        } finally {
            closeConnection3();
        }
        return result;
    }

    public boolean updateAccStatus(String email, String newStatus) throws SQLException, NamingException {
        boolean check = false;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "UPDATE tblAccount SET statusID=? WHERE email=?";
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, newStatus);
                preStm.setString(2, email);
                check = preStm.executeUpdate() != 0;
            }
        } finally {
            closeConnection2();
        }
        return check;
    }
}
