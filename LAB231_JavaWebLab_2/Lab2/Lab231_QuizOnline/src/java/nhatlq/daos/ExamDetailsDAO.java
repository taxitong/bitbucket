/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.naming.NamingException;
import nhatlq.dtos.ExamDetailsDTO;
import nhatlq.utils.DBConnection;

/**
 *
 * @author Admin
 */
public class ExamDetailsDAO {

    private Connection conn;
    private PreparedStatement preStm;
    private ResultSet rs;

    public ExamDetailsDAO() {
        conn = null;
        preStm = null;
        rs = null;
    }

    private void closeConnection2() throws SQLException {
        if (conn != null) {
            conn.close();
        }
        if (preStm != null) {
            preStm.close();
        }
    }

    public boolean insertExamDetails(ExamDetailsDTO examDetails) throws SQLException, NamingException {
        boolean result = false;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql
                        = "INSERT INTO tblExamDetails(examDetailsID, examID, quesID, quizID_Save, content_Save, optionA_Save, optionB_Save, optionC_Save, optionD_Save, correctAnswer_Save, chosenAnswer, isCorrect) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, examDetails.getExamDetailsID());
                preStm.setString(2, examDetails.getExamID());

                preStm.setString(3, examDetails.getQuesID());
                preStm.setString(4, examDetails.getQuizID_Save());
                preStm.setString(5, examDetails.getContent_Save());
                preStm.setString(6, examDetails.getOptionA_Save());
                preStm.setString(7, examDetails.getOptionB_Save());
                preStm.setString(8, examDetails.getOptionC_Save());
                preStm.setString(9, examDetails.getOptionD_Save());
                preStm.setString(10, examDetails.getCorrectAnswer_Save());

                preStm.setString(11, examDetails.getChosenAnswer());
                preStm.setBoolean(12, examDetails.isIsCorrect());
                result = preStm.executeUpdate() != 0;
            }
        } finally {
            closeConnection2();
        }
        return result;
    }
}
