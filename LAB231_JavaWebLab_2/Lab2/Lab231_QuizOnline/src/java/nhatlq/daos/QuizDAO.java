/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.naming.NamingException;
import nhatlq.dtos.QuizDTO;
import nhatlq.utils.DBConnection;

/**
 *
 * @author Admin
 */
public class QuizDAO {

    private Connection conn;
    private PreparedStatement preStm;
    private ResultSet rs;
    //attributes in the database 
    private static final String ACTIVE_STATUS_ID = "active";
    private static final String SUSPEND_STATUS_ID = "suspend";
    private static final String ADMIN_ROLE_ID = "admin";
    private static final String STUDENT_ROLE_ID = "student";

    public QuizDAO() {
        conn = null;
        preStm = null;
        rs = null;
    }

    private void closeConnection3() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (preStm != null) {
            preStm.close();
        }
        if (conn != null) {
            conn.close();
        }
    }

    private void closeConnection2() throws SQLException {
        if (conn != null) {
            conn.close();
        }
        if (preStm != null) {
            preStm.close();
        }
    }

    private String countQuiz_GenerateSQL(String subjectID, String roleID) {
        String condition1 = "subjectID=? ";
        String allConditions;
        String conditionE1 = "statusID=? ";
        allConditions = roleID.equals(ADMIN_ROLE_ID) ? condition1 : (condition1 + "AND " + conditionE1);
        String sql = "SELECT count(quizID) FROM tblQuiz WHERE " + allConditions;
        return sql;
    }

    public int countQuiz(String subjectID, String roleID) throws SQLException, NamingException {
        int result = 0;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = countQuiz_GenerateSQL(subjectID, roleID);
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, subjectID);
                if (!roleID.equals(ADMIN_ROLE_ID)) {
                    preStm.setString(2, ACTIVE_STATUS_ID);
                }
                rs = preStm.executeQuery();
                if (rs.next()) {
                    result = rs.getInt(1);
                }
            }
        } finally {
            closeConnection3();
        }
        return result;
    }

    private String searchQuiz_GenerateSQL(String subjectID, int size, String roleID) {
        //declare
        String condition1 = "subjectID=? ";
        String allConditions;
        String conditionE1 = "statusID=? ";
        //replace
        String WHERE_BETWEEN = "WHERE r BETWEEN ?*x-(x-1) AND ?*x".replace("x", size + "");
        //check condition
        allConditions = condition1;
        if (!roleID.equals(ADMIN_ROLE_ID)) {
            allConditions = condition1 + "AND " + conditionE1;
        }
        //form sql
        String sql
                = "SELECT "
                + "quizID, quizName, secondsToDoQuiz, quesEachExam, noOfAllowedAttempt, comment, statusID "
                + "FROM (SELECT ROW_NUMBER() OVER (ORDER BY quizName ASC) AS r, "
                + "quizID, quizName, secondsToDoQuiz, quesEachExam, noOfAllowedAttempt, comment, statusID "
                + "FROM tblQuiz WHERE " + allConditions
                + ") as x " + WHERE_BETWEEN;
        return sql;
    }

    public List<QuizDTO> searchQuiz(String subjectID, int indexQz, int size, String roleID) throws SQLException, NamingException {
        List<QuizDTO> result = new ArrayList<>();
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = searchQuiz_GenerateSQL(subjectID, size, roleID);
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, subjectID);
                if (!roleID.equals(ADMIN_ROLE_ID)) {
                    preStm.setString(2, ACTIVE_STATUS_ID);
                    preStm.setInt(3, indexQz);
                    preStm.setInt(4, indexQz);
                } else {
                    preStm.setInt(2, indexQz);
                    preStm.setInt(3, indexQz);
                }
                rs = preStm.executeQuery();
                while (rs.next()) {
                    String quizID = rs.getString("quizID");
                    //String subjectID = rs.getString("subjectID");
                    String quizName = rs.getString("quizName");
                    int secondsToDoQuiz = rs.getInt("secondsToDoQuiz");
                    int quesEachExam = rs.getInt("quesEachExam");
                    int noOfAllowedAttempt = rs.getInt("noOfAllowedAttempt");
                    String comment = rs.getString("comment");
                    String statusID = rs.getString("statusID");
                    result.add(new QuizDTO(quizID, subjectID, quizName, secondsToDoQuiz, quesEachExam, noOfAllowedAttempt, comment, statusID));
                }
            }
        } finally {
            closeConnection3();
        }
        return result;
    }

//    public boolean insertQuiz(QuizDTO quiz) throws SQLException, NamingException {
//        boolean result = false;
//        try {
//            conn = DBConnection.getConnection();
//            if (conn != null) {
//                String sql = "INSERT INTO tblQuiz(quizID, subjectID, statusID, content, optionA, optionB, optionC, optionD, correctAnswer) VALUES(?,?,?,?,?,?,?,?,?)";
//                preStm = conn.prepareStatement(sql);
//                preStm.setString(1, quiz.getQuizID());
//                preStm.setString(2, quiz.getSubjectID());
//                preStm.setString(3, quiz.getStatusID());
//                preStm.setString(4, quiz.getContent());
//                preStm.setString(5, quiz.getOptionA());
//                preStm.setString(6, quiz.getOptionB());
//                preStm.setString(7, quiz.getOptionC());
//                preStm.setString(8, quiz.getOptionD());
//                preStm.setString(9, quiz.getCorrectAnswer());
//                result = preStm.executeUpdate() > 0;
//            }
//        } finally {
//            closeConnection2();
//        }
//        return result;
//    }
    public QuizDTO getQuiz(String quizID) throws SQLException, NamingException {
        QuizDTO result = null;
//        try {
//            conn = DBConnection.getConnection();
//            if (conn != null) {
//                String sql = "SELECT "
//                        + "quizID, subjectID, statusID, content, optionA, optionB, optionC, optionD, correctAnswer "
//                        + "FROM tblQuiz WHERE quizID=?";
//                preStm = conn.prepareStatement(sql);
//                preStm.setString(1, quizID);
//                rs = preStm.executeQuery();
//                if (rs.next()) {
//                    //String quizID;
//                    String subjectID = rs.getString("subjectID");
//                    String statusID = rs.getString("statusID");
//                    String content = rs.getString("content");
//                    String optionA = rs.getString("optionA");
//                    String optionB = rs.getString("optionB");
//                    String optionC = rs.getString("optionC");
//                    String optionD = rs.getString("optionD");
//                    String correctAnswer = rs.getString("correctAnswer");
//                    result = new QuizDTO(quizID, subjectID, statusID, content, optionA, optionB, optionC, optionD, correctAnswer);
//                }
//            }
//        } finally {
//            closeConnection3();
//        }
        return result;
    }

    public boolean updateAvailableQuiz(QuizDTO quiz) throws SQLException, NamingException {
        boolean result = false;
//        try {
//            conn = DBConnection.getConnection();
//            if (conn != null) {
//                String sql = "UPDATE tblQuiz SET "
//                        + "subjectID=?, statusID=?, content=?, optionA=?, optionB=?, optionC=?, optionD=?, correctAnswer=? "
//                        + "WHERE quizID=?";
//                preStm = conn.prepareStatement(sql);
//                preStm.setString(1, quiz.getSubjectID());
//                preStm.setString(2, quiz.getStatusID());
//                preStm.setString(3, quiz.getContent());
//
//                preStm.setString(4, quiz.getOptionA());
//                preStm.setString(5, quiz.getOptionB());
//                preStm.setString(6, quiz.getOptionC());
//                preStm.setString(7, quiz.getOptionD());
//                preStm.setString(8, quiz.getCorrectAnswer());
//
//                preStm.setString(9, quiz.getQuizID());
//                result = preStm.executeUpdate() > 0;
//            }
//        } finally {
//            closeConnection3();
//        }
        return result;
    }

    public List<QuizDTO> searchRandomForExam(int size, String subjectID) throws SQLException, NamingException {
        List<QuizDTO> result = new ArrayList<>();
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "SELECT TOP (?) "
                        + "quizID, content, optionA, optionB, optionC, optionD "
                        + "FROM tblQuiz WHERE statusID='active' AND subjectID=? "
                        + "ORDER BY NEWID()";

                //quizID, subjectID, statusID, content, optionA, optionB, optionC, optionD, correctAnswer
                preStm = conn.prepareStatement(sql);
                preStm.setInt(1, size);
                preStm.setString(2, subjectID);
                rs = preStm.executeQuery();
                while (rs.next()) {
                    String quizID = rs.getString("quizID");
                    //String subjectIDTmp = rs.getString("subjectID");
                    //String statusIDTmp = rs.getString("statusID");
                    String content = rs.getString("content");
                    String optionA = rs.getString("optionA");
                    String optionB = rs.getString("optionB");
                    String optionC = rs.getString("optionC");
                    String optionD = rs.getString("optionD");
                    //String correctAnswer = rs.getString("correctAnswer");
                    //result.add(new QuizDTO(quizID, subjectID, "active", content, optionA, optionB, optionC, optionD, ""));
//                    result.add(new QuizDTO(quizID, subjectID, "active", content, optionA, optionB, optionC, optionD, ""));
                }
            }
        } finally {
            closeConnection3();
        }
        return result;
    }

    public String getCorrectAnswer(String quizID) throws NamingException, SQLException {
        String result = null;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "SELECT correctAnswer FROM tblQuiz WHERE quizID=?";
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, quizID);
                rs = preStm.executeQuery();
                if (rs.next()) {
                    result = rs.getString("correctAnswer");
                }
            }
        } finally {
            closeConnection3();
        }
        return result;
    }

    private String getQuizIDBySubjectID_GenerateSQL(String subjectID) {
        String allConditions = subjectID.isEmpty() ? "subjectID LIKE ? " : "subjectID=?　";
        String sql = "SELECT quizID, subjectID, quizName FROM tblQuiz "
                + "WHERE " + allConditions
                + "ORDER BY subjectID ASC";
        return sql;
    }

    public List<QuizDTO> getQuizIDQuizNameBySubjectID(String subjectID) throws SQLException, NamingException {
        List<QuizDTO> listQuiz = new ArrayList<>();
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = getQuizIDBySubjectID_GenerateSQL(subjectID);
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, ACTIVE_STATUS_ID);
                preStm.setString(1, subjectID.isEmpty() ? ("%%") : (subjectID));
                rs = preStm.executeQuery();
                while (rs.next()) {
                    String quizID = rs.getString("quizID");
                    String subjectIDTmp = rs.getString("subjectID");
                    String quizName = rs.getString("quizName");
                    QuizDTO quizDto = new QuizDTO(quizID, subjectIDTmp, quizName, 0, 0, 0, "", "");
                    listQuiz.add(quizDto);
                }
            }
        } finally {
            closeConnection3();
        }
        return listQuiz;
    }

    //for exam / view history
    public String replaceQzIDByQzName(String quizID) throws NamingException, SQLException {//?? maybe later on, I will look for another better method 
        String res = null;
        try {
            conn = DBConnection.getConnection();
            String sql = "SELECT quizName FROM tblQuiz WHERE quizID=?";
            preStm = conn.prepareStatement(sql);
            preStm.setString(1, quizID);
            rs = preStm.executeQuery();
            if (rs.next()) {
                res = rs.getString(1);
            }
        } finally {
            closeConnection3();
        }
        return res;
    }
}
