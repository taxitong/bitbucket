/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.naming.NamingException;
import nhatlq.dtos.QuestionDTO;
import nhatlq.utils.DBConnection;

/**
 *
 * @author Admin
 */
public class QuestionDAO {

    private Connection conn;
    private PreparedStatement preStm;
    private ResultSet rs;
    //attributes in the database 
    private static final String ACTIVE_STATUS_ID = "active";
    private static final String SUSPEND_STATUS_ID = "suspend";

    public QuestionDAO() {
        conn = null;
        preStm = null;
        rs = null;
    }

    private void closeConnection3() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (preStm != null) {
            preStm.close();
        }
        if (conn != null) {
            conn.close();
        }
    }

    private void closeConnection2() throws SQLException {
        if (conn != null) {
            conn.close();
        }
        if (preStm != null) {
            preStm.close();
        }
    }

    //
    private String countQuestion_GenerateSQL(String subjectID, String quizID, String statusID, String content) {
        String condition1 = subjectID.isEmpty() ? "qz.subjectID LIKE ? " : "qz.subjectID=? ";
        String condition2 = quizID.isEmpty() ? "qs.quizID LIKE ? " : "qs.quizID=? ";
        String condition3 = statusID.isEmpty() ? "qs.statusID LIKE ? " : "qs.statusID=? ";
        String condition4 = "content LIKE ? ";
        String allConditions = condition1 + "AND " + condition2 + "AND " + condition3 + "AND " + condition4;
        //form sql
        String sql = "SELECT count(quesID) FROM tblQuestion qs JOIN tblQuiz qz ON qs.quizID = qz.quizID AND " + allConditions;
        return sql;
    }

    public int countQuestion(String subjectID, String quizID, String statusID, String content) throws SQLException, NamingException {
        int result = 0;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = countQuestion_GenerateSQL(subjectID, quizID, statusID, content);
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, subjectID.isEmpty() ? ("%%") : (subjectID));
                preStm.setString(2, quizID.isEmpty() ? ("%%") : (quizID));
                preStm.setString(3, statusID.isEmpty() ? ("%%") : (statusID));
                preStm.setString(4, "%" + content + "%");
                rs = preStm.executeQuery();
                if (rs.next()) {
                    result = rs.getInt(1);
                }
            }
        } finally {
            closeConnection3();
        }
        return result;
    }

    private String searchQuestion_GenerateSQL(String subjectID, String quizID, String statusID, String content, int size) {
        String condition1 = subjectID.isEmpty() ? "subjectID LIKE ? " : "subjectID=? ";
        String condition2 = quizID.isEmpty() ? "qs.quizID LIKE ? " : "qs.quizID=? ";
        String condition3 = statusID.isEmpty() ? "qs.statusID LIKE ? " : "qs.statusID=? ";
        String condition4 = "content LIKE ? ";
        String allConditions = condition1 + "AND " + condition2 + "AND " + condition3 + "AND " + condition4;
        //replace
        String WB = "WHERE r BETWEEN ?*x-(x-1) AND ?*x";
        String WHERE_BETWEEN = WB.replace("x", size + "");
        //form sql
        String sql
                = "SELECT "
                + "quesID, quizID, statusID, content, optionA, optionB, optionC, optionD, answer "
                + "FROM (SELECT ROW_NUMBER() OVER (ORDER BY subjectID ASC) AS r, "
                + "quesID, qs.quizID, qs.statusID, content, optionA, optionB, optionC, optionD, answer "
                + "FROM tblQuestion qs JOIN tblQuiz qz ON qs.quizID = qz.quizID AND " + allConditions
                + ") as x " + WHERE_BETWEEN;
        return sql;
    }

    public List<QuestionDTO> searchQuestion(String subjectID, String quizID, String statusID, String content, int indexQs, int size) throws SQLException, NamingException {
        List<QuestionDTO> result = new ArrayList<>();
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = searchQuestion_GenerateSQL(subjectID, quizID, statusID, content, size);
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, subjectID.isEmpty() ? "%%" : subjectID);
                preStm.setString(2, quizID.isEmpty() ? "%%" : quizID);
                preStm.setString(3, statusID.isEmpty() ? "%%" : statusID);
                preStm.setString(4, "%" + content + "%");
                preStm.setInt(5, indexQs);
                preStm.setInt(6, indexQs);
                rs = preStm.executeQuery();
                while (rs.next()) {
                    String quesID = rs.getString("quesID");
                    String quizIDTmp = rs.getString("quizID");
                    String statusIDTmp = rs.getString("statusID");
                    String contentTmp = rs.getString("content");

                    String optionA = rs.getString("optionA");
                    String optionB = rs.getString("optionB");
                    String optionC = rs.getString("optionC");
                    String optionD = rs.getString("optionD");
                    String answer = rs.getString("answer");
                    result.add(new QuestionDTO(quesID, quizIDTmp, statusIDTmp, contentTmp, optionA, optionB, optionC, optionD, answer));
                }
            }
        } finally {
            closeConnection3();
        }
        return result;
    }
    //

    public boolean deleteByUpdateQsStatusID(String questionID) throws SQLException, NamingException {
        boolean result = false;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "UPDATE tblQuestion SET statusID=? WHERE quesID=?";
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, SUSPEND_STATUS_ID);
                preStm.setString(2, questionID);
                result = preStm.executeUpdate() > 0;
            }
        } finally {
            closeConnection3();
        }
        return result;
    }

    public boolean insertQuiz(QuestionDTO ques) throws SQLException, NamingException {
        boolean result = false;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "INSERT INTO tblQuestion(quesID, quizID, statusID, content, "
                        + "optionA, optionB, optionC, optionD, answer) VALUES(?,?,?,?,?,?,?,?,?)";
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, ques.getQuesID());
                preStm.setString(2, ques.getQuizID());
                preStm.setString(3, ques.getStatusID());
                preStm.setString(4, ques.getContent());

                preStm.setString(5, ques.getOptionA());
                preStm.setString(6, ques.getOptionB());
                preStm.setString(7, ques.getOptionC());
                preStm.setString(8, ques.getOptionD());
                preStm.setString(9, ques.getAnswer());
                result = preStm.executeUpdate() > 0;
            }
        } finally {
            closeConnection2();
        }
        return result;
    }

    public boolean changeQuizID(String quizID, String quesID) throws SQLException, NamingException {
        boolean result = false;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "UPDATE tblQuestion SET quizID=? WHERE quesID=?";
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, quizID);
                preStm.setString(2, quesID);
                result = preStm.executeUpdate() > 0;
            }
        } finally {
            closeConnection3();
        }
        return result;
    }

    public QuestionDTO getQuestion(String quesID) throws SQLException, NamingException {
        QuestionDTO result = null;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "SELECT "
                        + "statusID, content, optionA, optionB, optionC, optionD, answer "
                        + "FROM tblQuestion WHERE quesID=?";
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, quesID);
                rs = preStm.executeQuery();
                if (rs.next()) {
                    String statusID = rs.getString("statusID");
                    String content = rs.getString("content");

                    String optionA = rs.getString("optionA");
                    String optionB = rs.getString("optionB");
                    String optionC = rs.getString("optionC");
                    String optionD = rs.getString("optionD");
                    String answer = rs.getString("answer");
                    result = new QuestionDTO(quesID, "", statusID, content, optionA, optionB, optionC, optionD, answer);
                }
            }
        } finally {
            closeConnection3();
        }
        return result;
    }

    public boolean changeContent(QuestionDTO question) throws SQLException, NamingException {
        boolean result = false;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "UPDATE tblQuestion SET "
                        + "statusID=?, content=?, optionA=?, optionB=?, optionC=?, optionD=?, answer=? "
                        + "WHERE quesID=?";
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, question.getStatusID());
                preStm.setString(2, question.getContent());

                preStm.setString(3, question.getOptionA());
                preStm.setString(4, question.getOptionB());
                preStm.setString(5, question.getOptionC());
                preStm.setString(6, question.getOptionD());
                preStm.setString(7, question.getAnswer());
                preStm.setString(8, question.getQuesID());
                result = preStm.executeUpdate() > 0;
            }
        } finally {
            closeConnection3();
        }
        return result;
    }

    public List<QuestionDTO> searchRandomForExam(int quesEachExam, String quizID) throws SQLException, NamingException {
        List<QuestionDTO> result = new ArrayList<>();
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "SELECT TOP (?) "
                        + "quesID, content, optionA, optionB, optionC, optionD "
                        + "FROM tblQuestion WHERE statusID='active' AND quizID=? "
                        + "ORDER BY NEWID()";
                preStm = conn.prepareStatement(sql);
                preStm.setInt(1, quesEachExam);
                preStm.setString(2, quizID);
                rs = preStm.executeQuery();
                while (rs.next()) {
                    String quesID = rs.getString("quesID");
                    String content = rs.getString("content");
                    String optionA = rs.getString("optionA");
                    String optionB = rs.getString("optionB");
                    String optionC = rs.getString("optionC");
                    String optionD = rs.getString("optionD");
                    result.add(new QuestionDTO(quesID, quizID, "active", content, optionA, optionB, optionC, optionD, ""));
                }
            }
        } finally {
            closeConnection3();
        }
        return result;
    }

    public String getCorrectAnswer(String quesID) throws NamingException, SQLException {
        String result = null;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "SELECT answer FROM tblQuestion WHERE quesID=?";
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, quesID);
                rs = preStm.executeQuery();
                if (rs.next()) {
                    result = rs.getString("answer");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection3();
        }
        return result;
    }
}
