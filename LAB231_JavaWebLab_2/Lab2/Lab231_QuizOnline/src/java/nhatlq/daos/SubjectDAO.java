/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.naming.NamingException;
import nhatlq.dtos.SubjectDTO;
import nhatlq.utils.DBConnection;

/**
 *
 * @author Admin
 */
public class SubjectDAO {

    private Connection conn;
    private PreparedStatement preStm;
    private ResultSet rs;
    //attributes in the database 
    private static final String ACTIVE_STATUS_ID = "active";
    private static final String ADMIN_ROLE_ID = "admin";
    private static final String STUDENT_ROLE_ID = "student";

    public SubjectDAO() {
        conn = null;
        preStm = null;
        rs = null;
    }

    private void closeConnection3() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (preStm != null) {
            preStm.close();
        }
        if (conn != null) {
            conn.close();
        }
    }

    /*generate*/
    String countSubject_GenerateSQL(String subjectID, String role) {
        String condition1 = subjectID.isEmpty() ? "subjectID LIKE ? " : "subjectID=? ";
        String allConditions;
        String conditionE1 = "statusID=? ";
        allConditions = role.equals(ADMIN_ROLE_ID) ? condition1 : (condition1 + "AND " + conditionE1);
        String sql = "SELECT count(subjectID) FROM tblSubject WHERE " + allConditions;
        return sql;
    }

    public int countSubject(String subjectID, String role) throws SQLException, NamingException {
        int result = 0;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = countSubject_GenerateSQL(subjectID, role);
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, subjectID.isEmpty() ? "%%" : subjectID);
                if (!role.equals(ADMIN_ROLE_ID)) {
                    preStm.setString(2, ACTIVE_STATUS_ID);
                }
                rs = preStm.executeQuery();
                if (rs.next()) {
                    result = rs.getInt(1);
                }
            }
        } finally {
            closeConnection3();
        }
        return result;
    }

    /*generate*/
    String searchSubject_GenerateSQL(String subjectID, int size, String roleID) {
        //declare
        String condition1 = "subjectID=? ";
        String allConditions;
        String conditionE1 = "statusID=? ";
        //replace
        String WHERE_BETWEEN = "WHERE r BETWEEN ?*x-(x-1) AND ?*x".replace("x", size + "");
        //check condition
        if (subjectID.isEmpty()) {
            condition1 = "subjectID LIKE ? ";
        }
        allConditions = condition1;
        if (!roleID.equals(ADMIN_ROLE_ID)) {
            allConditions = allConditions + "AND " + conditionE1;
        }
        //form sql
        String sql = "SELECT "
                + "subjectID, subjectName, statusID "
                + "FROM (SELECT ROW_NUMBER() OVER (ORDER BY subjectID ASC) AS r, "
                + "subjectID, subjectName, statusID "
                + "FROM tblSubject WHERE " + allConditions
                + ") as x " + WHERE_BETWEEN;
        return sql;
    }

    public List<SubjectDTO> searchSubject(String subjectID, int indexS, int size, String roleID) throws SQLException, NamingException {
        List<SubjectDTO> result = new ArrayList<>();
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = searchSubject_GenerateSQL(subjectID, size, roleID);
                preStm = conn.prepareStatement(sql);
                String s1 = subjectID.isEmpty() ? ("%" + subjectID + "%") : (subjectID);
                preStm.setString(1, s1);
                if (!roleID.equals(ADMIN_ROLE_ID)) {
                    preStm.setString(2, ACTIVE_STATUS_ID);
                    preStm.setInt(3, indexS);
                    preStm.setInt(4, indexS);
                } else {
                    preStm.setInt(2, indexS);
                    preStm.setInt(3, indexS);
                }
                rs = preStm.executeQuery();
                while (rs.next()) {
                    String subjectID_Tmp = rs.getString("subjectID");
                    String subjectName = rs.getString("subjectName");
                    String status = rs.getString("statusID");
                    result.add(new SubjectDTO(subjectID_Tmp, subjectName, status));
                }
            }
        } finally {
            closeConnection3();
        }
        return result;
    }

    public int countSubjectAD(String search, String role) throws SQLException, NamingException {
        int result = 0;
//        try {
//            conn = DBConnection.getConnection();
//            if (conn != null) {
//                String sql = generateSQL_CountSubject(search, role);
//                preStm = conn.prepareStatement(sql);
//                preStm.setString(1, search);
//                if (search.isEmpty()) {
//                    preStm.setString(1, "%" + search + "%");
//                }
//                rs = preStm.executeQuery();
//                if (rs.next()) {
//                    result = rs.getInt(1);
//                }
//            }
//        } finally {
//            closeConnection3();
//        }
        return result;
    }

    public List<SubjectDTO> getAllSubjectIdAndName_NoPaging() throws SQLException, NamingException {
        List<SubjectDTO> result = new ArrayList<>();
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "SELECT subjectID, subjectName, statusID "
                        + "FROM tblSubject WHERE subjectID LIKE '%%' ORDER BY subjectID ASC";
                preStm = conn.prepareStatement(sql);
                rs = preStm.executeQuery();
                while (rs.next()) {
                    String subjectID = rs.getString("subjectID");
                    String subjectName = rs.getString("subjectName");
                    String statusID = rs.getString("statusID");
                    result.add(new SubjectDTO(subjectID, subjectName, statusID));
                }
            }
        } finally {
            closeConnection3();
        }
        return result;
    }

    //for exam / view history
    public String replaceExamIDBySIDAndSName(String quizID) throws NamingException, SQLException {//?? maybe later on, I will look for another better method 
        String res = null;
        try {
            conn = DBConnection.getConnection();
            String sql = "SELECT s.subjectID, subjectName FROM tblSubject s INNER JOIN tblQuiz q "
                    + "ON s.subjectID = q.subjectID AND q.quizID=?";
            preStm = conn.prepareStatement(sql);
            preStm.setString(1, quizID);
            rs = preStm.executeQuery();
            if (rs.next()) {
                String subjectID = rs.getString("subjectID");
                String subjectName = rs.getString("subjectName");
                res = subjectID + " - " + subjectName;
            }
        } finally {
            closeConnection3();
        }
        return res;
    }
}
