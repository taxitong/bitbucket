/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.cons;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nhatlq.daos.AccountDAO;
import nhatlq.dtos.AccountDTO;

/**
 *
 * @author Admin
 */
public class SignUpCon extends HttpServlet {

    private static final String ERROR = "startPage.jsp";
    private static final String SUCCESS_JSP = "startPage.jsp";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            String displayName = request.getParameter("tDisplayName_SignUp");
            String email = request.getParameter("tEmail_SignUp");
            String password = request.getParameter("tPassword_SignUp");
            //String rePassword = request.getParameter("tRePassword_SignUp");
            if (displayName != null && email != null && password != null) {//Block false data when accessing directly from the address bar 
                String roleID = "student";
                String statusID = "new";
                AccountDAO accountDao = new AccountDAO();
                boolean check = true;
                if (accountDao.checkExistMail(email)) {
                    request.setAttribute("duplicateEmail", "Email has already exist");
                    request.setAttribute("tDisplayName_SignUp", displayName);
                    request.setAttribute("tEmail_SignUp", email);
                    check = false;
                }
                if (check) {
                    AccountDTO acc = new AccountDTO(email, displayName, password, roleID, statusID);
                    accountDao.insert(acc);
                    request.setAttribute("duplicateEmail", null);
                    url = SUCCESS_JSP;
                    HttpSession session = request.getSession(false);
                    session.setAttribute("INVALID_ACCOUNT", "");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
