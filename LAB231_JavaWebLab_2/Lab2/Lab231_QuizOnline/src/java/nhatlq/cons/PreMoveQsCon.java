/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.cons;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nhatlq.daos.QuizDAO;
import nhatlq.dtos.QuizDTO;

/**
 *
 * @author Admin
 */
public class PreMoveQsCon extends HttpServlet {

    private final static String ERROR = "error.jsp";
    private final static String SUCCESS_MOVE_QS_JSP = "moveQs.jsp";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            HttpSession session = request.getSession();
            getQzIDBySID(request, session);
            url = SUCCESS_MOVE_QS_JSP;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            response.sendRedirect(url);
        }
    }

    private void getQzIDBySID(HttpServletRequest request, HttpSession session) throws SQLException, NamingException {
        String subjectID = request.getParameter("tSID_PreMoveQs");
        String quizID = request.getParameter("tQzID_PreMoveQs");
        String quesID = request.getParameter("tQsID_PreMoveQs");
        if (quesID == null) {
            quesID = request.getParameter("tQsID_MoveQs");
        }
        
        QuizDAO quizDao = new QuizDAO();
        List<QuizDTO> listQuiz = quizDao.getQuizIDQuizNameBySubjectID(subjectID);
        session.setAttribute("listQzID_From_PreMoveQs", listQuiz);
        
        session.setAttribute("tSID_From_PreMoveQs", subjectID);
        session.setAttribute("tQzID_From_PreMoveQs", quizID);
        session.setAttribute("tQsID_From_PreMoveQs", quesID);
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
