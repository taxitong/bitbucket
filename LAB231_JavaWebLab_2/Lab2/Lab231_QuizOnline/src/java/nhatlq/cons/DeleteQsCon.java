/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.cons;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nhatlq.daos.QuestionDAO;

/**
 *
 * @author Admin
 */
public class DeleteQsCon extends HttpServlet {

    private static final String ERROR = "error.jsp";
    private static final String SUCCESS_PAGE = "SearchQsCon";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        
        
        
        try {
            String questionID = request.getParameter("tQsID_DeleteQs");
            QuestionDAO QuestionDao = new QuestionDAO();
            boolean check = QuestionDao.deleteByUpdateQsStatusID(questionID);

            String tSID_SearchQs = request.getParameter("tSID_DeleteQs");
            String tQzID_SearchQs = request.getParameter("tQzID_DeleteQs");
            String tQsStatusID_SearchQs = request.getParameter("tQsStatusID_DeleteQs");
            String tQsContent_SearchQs = request.getParameter("tQsContent_DeleteQs");
            int indexQs = findIndex(request);

            if (check) {
                url = SUCCESS_PAGE
                        + "?" + "tSID_SearchQs=" + tSID_SearchQs
                        + "&" + "tQzID_SearchQs=" + tQzID_SearchQs
                        + "&" + "tQsStatusID_SearchQs=" + tQsStatusID_SearchQs
                        + "&" + "tQsContent_SearchQs=" + tQsContent_SearchQs
                        + "&" + "indexQs=" + indexQs
                        ;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
//            response.sendRedirect(url);
        }
    }

    private int findIndex(HttpServletRequest request) {
        int indexQs;
        HttpSession session = request.getSession(false);
        try {
            indexQs = (int) session.getAttribute("indexQs");
        } catch (Exception e) {
            indexQs = 1;
        }
        return indexQs;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
