/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.cons;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nhatlq.daos.QuestionDAO;
import nhatlq.dtos.AccountDTO;
import nhatlq.dtos.QuestionDTO;

/**
 *
 * @author Admin
 */
public class GetQzToECon extends HttpServlet {

    private static final String ERROR = "error.jsp";
    private static final String SUCCESS = "exam.jsp";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            HttpSession session = request.getSession(false);
            String isExamining = (String) session.getAttribute("isExamining");
            if (isExamining == null) {
                String subjectName = request.getParameter("tNowSName_GetQzToE");
                String quizName = request.getParameter("tNowQzName_GetQzToE");
                String quizID = request.getParameter("tQzID_GetQzToE");
                int secondsToDoQuiz = Integer.parseInt(request.getParameter("tSecondsToDoQuiz_GetQzToE"));
                int quesEachExam = Integer.parseInt(request.getParameter("tQuesEachExam_GetQzToE"));

                QuestionDAO questionDao = new QuestionDAO();
                List<QuestionDTO> listQues = questionDao.searchRandomForExam(quesEachExam, quizID);
                session.setAttribute("LIST_QUES_FOR_EXAMINATION", listQues);

                //prepare the properties for submit E
                AccountDTO accountDto = (AccountDTO) session.getAttribute("LOGIN_ACCOUNT");
                String email = accountDto.getEmail();
                String startTime = "" + new Timestamp(System.currentTimeMillis());

                session.setAttribute("tEmail_SubmitE", email);
                session.setAttribute("tStartTime_SubmitE", startTime);
                session.setAttribute("tQzID_SubmitE", quizID);
                session.setAttribute("tSecondsToDoQuiz_SubmitE", secondsToDoQuiz);
                session.setAttribute("tQuesEachExam_SubmitE", quesEachExam);

                session.setAttribute("tNowSName_From_GetQzToE", subjectName);
                session.setAttribute("tNowQzName_From_GetQzToE", quizName);

                session.setAttribute("isExamining", "yes");
            }
            //url redirect
            url = SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            response.sendRedirect(url);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
