/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.cons;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nhatlq.daos.QuizDAO;
import nhatlq.dtos.AccountDTO;
import nhatlq.dtos.QuizDTO;

/**
 *
 * @author Admin
 */
public class SearchQzCon extends HttpServlet {

    //page
    private final static String ERROR = "error.jsp";
    private final static String SUCCESS_AD_VIEW_QUIZ_AD_JSP = "viewQzAd.jsp";
    private final static String SUCCESS_STU_VIEW_QUIZ_STU_JSP = "viewQzStu.jsp";
    //attributes in the database  
    private static final String ACTIVE_STATUS_ID = "active";
    private static final String ADMIN_ROLE_ID = "admin";
    private static final String STUDENT_ROLE_ID = "student";
    //record each page
    private static final int QUIZ_EACH_PAGE = 3;//each page has 3 product;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            HttpSession session = request.getSession(false);
            AccountDTO accountDto = (AccountDTO) session.getAttribute("LOGIN_ACCOUNT");
            String roleID = accountDto.getRoleID();
            searchQuiz_Paging(request, session, roleID);
            switch (roleID) {
                case ADMIN_ROLE_ID:
                    url = SUCCESS_AD_VIEW_QUIZ_AD_JSP;
                    break;
                case STUDENT_ROLE_ID:
                    url = SUCCESS_STU_VIEW_QUIZ_STU_JSP;
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //please keep forward (don't use sendRedirect)
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    private void searchQuiz_Paging(HttpServletRequest request, HttpSession session, String roleID) throws SQLException, NamingException {
        QuizDAO quizDao = new QuizDAO();
        String subjectID = request.getParameter("tSID_SearchQz");
        String subjectName = request.getParameter("tSName_SearchQz");
        if (subjectName == null) {
            subjectName = (String) session.getAttribute("thisSName_From_SearchQz");
        }

        int indexQz;
        try {
            indexQz = Integer.parseInt(request.getParameter("indexQz"));
        } catch (Exception e) {
            indexQz = 1;
        }
                
        int size = QUIZ_EACH_PAGE;
        int endPageQz;

        int count = quizDao.countQuiz(subjectID, roleID);
        List<QuizDTO> ListQz = quizDao.searchQuiz(subjectID, indexQz, size, roleID);

        endPageQz = count / size;
        if (count % size != 0) {
            endPageQz++;
        }
        session.setAttribute("endPageQz", endPageQz);
        session.setAttribute("LIST_QZ", ListQz);
        session.setAttribute("indexQz", indexQz);
        
        session.setAttribute("thisSID_From_SearchQz", subjectID);
        session.setAttribute("thisSName_From_SearchQz", subjectName);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
