/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.cons;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nhatlq.daos.ExamDAO;
import nhatlq.daos.ExamDetailsDAO;
import nhatlq.daos.QuestionDAO;
import nhatlq.daos.QuizDAO;
import nhatlq.daos.SubjectDAO;
import nhatlq.dtos.ExamDTO;
import nhatlq.dtos.ExamDetailsDTO;
import nhatlq.dtos.QuestionDTO;
import nhatlq.utils.Encryption;

/**
 *
 * @author Admin
 */
public class SubmitECon extends HttpServlet {

    private static final String ERROR = "error.jsp";
    private static final String SUCCESS = "examRes.jsp";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            HttpSession session = request.getSession();
            List<QuestionDTO> list = (List<QuestionDTO>) session.getAttribute("LIST_QUES_FOR_EXAMINATION");
            //create E first, then EDetails in for loop
            String examID = generateID();
            String quizID = request.getParameter("tQzID_SubmitE");
            String startTime = request.getParameter("tStartTime_SubmitE");
            String submitTime = "" + new Timestamp(System.currentTimeMillis());
            String email = request.getParameter("tEmail_SubmitE");
            int totalQues = Integer.parseInt(request.getParameter("tQuesEachExam_SubmitE"));
            //int totalCorrect=
            ExamDTO examDto = new ExamDTO(examID, quizID, startTime, submitTime, email, totalQues, 0, 0);
            ExamDAO examDao = new ExamDAO();
            boolean checkInsertExam = examDao.insertExam(examDto);
            int i = 1;//for iterative list submit quiz
            int totalCorrect = 0;
            for (QuestionDTO quesDto : list) {
                String examDetailsID = generateID();
                String quesID = quesDto.getQuesID();
                String subjectID_Save = quesDto.getQuizID();
                String content_Save = quesDto.getContent();
                String optionA_Save = quesDto.getOptionA();
                String optionB_Save = quesDto.getOptionB();
                String optionC_Save = quesDto.getOptionC();
                String optionD_Save = quesDto.getOptionD();
                String chosenAnswer = request.getParameter("tChosenAnswer_SubmitE" + i);
                //check if correct answer
                boolean isCorrect = false;
                QuestionDAO questionDao = new QuestionDAO();
                String correctAnswer = questionDao.getCorrectAnswer(quesID);//tmp var to check chosen ans with correct ans
                if (correctAnswer.equals(chosenAnswer)) {
                    isCorrect = true;
                    totalCorrect++;//for check % correct ans/total ans
                }
                i++;
                if (checkInsertExam) {
                    ExamDetailsDTO examDetailsDto = new ExamDetailsDTO(examDetailsID, examID, quesID, subjectID_Save, content_Save, optionA_Save, optionB_Save, optionC_Save, optionD_Save, correctAnswer, chosenAnswer, isCorrect);
                    ExamDetailsDAO examDetailsDao = new ExamDetailsDAO();
                    examDetailsDao.insertExamDetails(examDetailsDto);
                }
            }
            if (checkInsertExam) {
                double tc = totalCorrect;
                double tq = totalQues;
                int result = (int) (tc / tq * 100);

                examDao.updateAvailableExam(examID, totalCorrect, result);
                //get subjectID and Name to display in examRes result 
                SubjectDAO subjectDao = new SubjectDAO();
                String SID_SName = subjectDao.replaceExamIDBySIDAndSName(quizID);
                //get quizName to display in examRes result 
                QuizDAO quizDao = new QuizDAO();
                String quizName = quizDao.replaceQzIDByQzName(quizID);

                session.setAttribute("SID_SName_EResPage", SID_SName);
                session.setAttribute("quizName_EResPage", quizName);
                session.setAttribute("totalCorrect_EResPage", totalCorrect);
                session.setAttribute("totalQues_EResPage", totalQues);
                session.setAttribute("grade_EResPage", result);
                //submit exam
                session.setAttribute("isExamining", null);

                url = SUCCESS;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //request.getRequestDispatcher(url).forward(request, response);
            response.sendRedirect(url);
        }
    }

    private String generateID() {
        String result = null;

        Random rd = new Random();//https://www.tutorialspoint.com/generate-random-bytes-in-java
        byte[] arr = new byte[7];
        rd.nextBytes(arr);

        String createDate = "" + new Timestamp(System.currentTimeMillis());
        Encryption en = new Encryption();
        try {
            result = en.toHexString(en.getSHA(Arrays.toString(arr) + createDate));
        } catch (NoSuchAlgorithmException e) {
        }
        return result;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
