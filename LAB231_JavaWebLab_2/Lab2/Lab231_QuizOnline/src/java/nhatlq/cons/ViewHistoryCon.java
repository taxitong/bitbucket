/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.cons;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nhatlq.daos.ExamDAO;
import nhatlq.daos.QuizDAO;
import nhatlq.daos.SubjectDAO;
import nhatlq.dtos.AccountDTO;
import nhatlq.dtos.ExamDTO;
import nhatlq.dtos.SubjectDTO;

/**
 *
 * @author Admin
 */
public class ViewHistoryCon extends HttpServlet {

    private final static String ERROR = "error.jsp";
    private final static String SUCCESS = "viewHistory.jsp";
    //record each page
    private static final int EXAM_EACH_PAGE = 3;//each page has 3 product;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            //prepare list subject to search
            HttpSession session = request.getSession();
            searchAllSubjectIdAndName_NoPaging(session);
            AccountDTO accountDto = (AccountDTO) session.getAttribute("LOGIN_ACCOUNT");

            //actually search exam
            int indexE;
            try {
                indexE = Integer.parseInt(request.getParameter("indexE"));
            } catch (Exception e) {
                indexE = 1;
            }

            String subjectID = request.getParameter("tSID_ViewHistory");
            if (subjectID == null) {
                subjectID = "";
            }

            String email = accountDto.getEmail();
            ExamDAO examDao = new ExamDAO();

            int count = examDao.countExam(email, subjectID);
            List<ExamDTO> listE = examDao.getAllExam(email, subjectID, indexE);

            //replace quizID by quizName
            QuizDAO quizDao = new QuizDAO();
            SubjectDAO subjectDao = new SubjectDAO();
            for (int i = 0; i < listE.size(); i++) {
                String quizID = listE.get(i).getQuizID();
                String SID_SName = subjectDao.replaceExamIDBySIDAndSName(quizID);//sacrifice examID, get SID + " - " SName for user
                listE.get(i).setExamID(SID_SName);
                
                String quizName = quizDao.replaceQzIDByQzName(quizID);//sacrifice quizID, get quiz name for user
                listE.get(i).setQuizID(quizName);
            }
            //

            int size = EXAM_EACH_PAGE;
            int endPageE = count / size;
            if (count % size != 0) {
                endPageE++;
            }

            session.setAttribute("endPageE", endPageE);
            session.setAttribute("LIST_EXAM", listE);
            session.setAttribute("indexE", indexE);

            session.setAttribute("tSID_ViewHistory", subjectID);

            searchAllSubjectIdAndName_NoPaging(session);

            url = SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    private void searchAllSubjectIdAndName_NoPaging(HttpSession session) throws SQLException, NamingException {
        SubjectDAO subjectDao = new SubjectDAO();
        List<SubjectDTO> list = subjectDao.getAllSubjectIdAndName_NoPaging();
        session.setAttribute("LIST_ALLSUBJECT_FROM_ViewHistory", list);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
