/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.cons;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nhatlq.daos.QuestionDAO;
import nhatlq.daos.QuizDAO;
import nhatlq.dtos.AccountDTO;
import nhatlq.dtos.QuestionDTO;
import nhatlq.dtos.QuizDTO;

/**
 *
 * @author Admin
 */
public class SearchQsCon extends HttpServlet {

    private final static String ERROR = "error.jsp";
    private final static String SUCCESS_MAIN_AD_JSP = "mainAd.jsp";
    //record each page
    private static final int QUESTION_EACH_PAGE = 3;//each page has 3 product;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            HttpSession session = request.getSession();
            AccountDTO accountDto = (AccountDTO) session.getAttribute("LOGIN_ACCOUNT");
            String role = accountDto.getRoleID();
            searchQues(request, session);
            getQIDQNameBySID(request, session);
            url = SUCCESS_MAIN_AD_JSP;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    private void searchQues(HttpServletRequest request, HttpSession session) throws SQLException, NamingException {
        QuestionDAO questionDao = new QuestionDAO();
        String subjectID = request.getParameter("tSID_SearchQs");
        String quizID = request.getParameter("tQzID_SearchQs");
        String statusID = request.getParameter("tQsStatusID_SearchQs");
        String content = request.getParameter("tQsContent_SearchQs");
        int indexQs;
        try {
            indexQs = Integer.parseInt(request.getParameter("indexQs"));
        } catch (NumberFormatException e) {
            indexQs = 1;
        }

        int size = QUESTION_EACH_PAGE;
        int endPageQs;

        int count = questionDao.countQuestion(subjectID, quizID, statusID, content);
        List<QuestionDTO> listQs = questionDao.searchQuestion(subjectID, quizID, statusID, content, indexQs, size);

        endPageQs = count / size;
        if (count % size != 0) {
            endPageQs++;
        }
        session.setAttribute("endPageQs", endPageQs);
        session.setAttribute("LIST_Qs", listQs);
        session.setAttribute("indexQs", indexQs);

        session.setAttribute("tSID_SearchQs", subjectID);
        session.setAttribute("tQzID_SearchQs", quizID);
        session.setAttribute("tQsStatusID_SearchQs", statusID);
        session.setAttribute("tQsContent_SearchQs", content);
    }
    
    private void getQIDQNameBySID(HttpServletRequest request, HttpSession session) throws SQLException, NamingException {
        String subjectID = request.getParameter("tSID_SearchQs");
        //String subjectID = "";
        QuizDAO quizDao = new QuizDAO();
        List<QuizDTO> listQuiz = quizDao.getQuizIDQuizNameBySubjectID(subjectID);
        session.setAttribute("listQzID_From_GetSToQz__SearchQs_SignIn", listQuiz);
        //session.setAttribute("tSID_From_SearchQs", subjectID);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */ 
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
