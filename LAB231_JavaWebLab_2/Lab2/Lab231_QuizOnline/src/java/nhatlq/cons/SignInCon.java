/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.cons;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nhatlq.daos.AccountDAO;
import nhatlq.daos.QuizDAO;
import nhatlq.daos.StatusDAO;
import nhatlq.daos.SubjectDAO;
import nhatlq.dtos.AccountDTO;
import nhatlq.dtos.QuizDTO;
import nhatlq.dtos.SubjectDTO;

/**
 *
 * @author Admin
 */
public class SignInCon extends HttpServlet {

    //fail page
    private static final String ERROR = "error.jsp";
    private static final String INVALID_ACCOUNT_JSP = "startPage.jsp";
    //success page
    private static final String SUCCESS_MAIN_AD_JSP = "mainAd.jsp";
    private static final String SUCCESS_MAIN_STU_JSP = "mainStu.jsp";
    //attributes in the database  
    private static final String ACTIVE_STATUS_ID = "active";
    private static final String ADMIN_ROLE_ID = "admin";
    private static final String STUDENT_ROLE_ID = "student";
    //record each page
    private static final int SUBJECT_EACH_PAGE = 3;//each page has 3 product;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            String email = request.getParameter("tEmail_SignIn");
            String password = request.getParameter("tPassword_SignIn");
            if (email == null || password == null) {
                email = "";
                password = "";
            }
            AccountDAO accountDao = new AccountDAO();
            AccountDTO accountDto = accountDao.checkLogin(email, password);
            HttpSession session = request.getSession();
            if (accountDto == null || !ACTIVE_STATUS_ID.equals(accountDto.getStatusID())) {
                url = doIfUnauthenticated(session);
            } else {
                url = doIfAuthenticated(request, session, accountDto);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //request.getRequestDispatcher(url).forward(request, response);
            response.sendRedirect(url);
        }
    }

    private String doIfUnauthenticated(HttpSession session) {
        session.setAttribute("INVALID_ACCOUNT", "Invalid email/password OR Your account hasn't been verified by admin");
        return INVALID_ACCOUNT_JSP;
    }

    private String doIfAuthenticated(HttpServletRequest request, HttpSession session, AccountDTO accountDto) throws SQLException, NamingException {
        //FOUND acc
        session.setAttribute("LOGIN_ACCOUNT", accountDto);
        String roleID = accountDto.getRoleID();
        String url = "";
        switch (roleID) {
            case ADMIN_ROLE_ID:
//                searchAllQuiz_Paging(request, session);
                getQIDQNameBySID(request, session);
                searchAllSubjectIdAndName_NoPaging(session);
                searchAllStatus_NoPaging(session);
                url = SUCCESS_MAIN_AD_JSP;
                break;
            case STUDENT_ROLE_ID:
                searchAllSubject_Paging(request, session, roleID);
                url = SUCCESS_MAIN_STU_JSP;
                break;
        }
        session.setAttribute("INVALID_ACCOUNT", null);
        return url;
    }

    private void searchAllSubject_Paging(HttpServletRequest request, HttpSession session, String roleID) throws SQLException, NamingException {
        SubjectDAO subjectDao = new SubjectDAO();
        String subjectID = "";

        int indexS = 1;
        int size = SUBJECT_EACH_PAGE;
        int endPageS;

        int count = subjectDao.countSubject(subjectID, roleID);
        List<SubjectDTO> listS = subjectDao.searchSubject(subjectID, indexS, size, roleID);

        endPageS = count / size;
        if (count % size != 0) {
            endPageS++;
        }
        session.setAttribute("endPageS", endPageS);
        session.setAttribute("LIST_S", listS);
        session.setAttribute("indexS", indexS);
    }

    private void searchAllQuiz_Paging(HttpServletRequest request, HttpSession session) throws SQLException, NamingException {
//        QuizDAO quizDao = new QuizDAO();
//        String content = "";
//        String subjectID = "";
//        String statusID = "";
//
//        int indexQ = 1;
//        int size = 3;//each page has 3 product;
//        int endPageQ;
//
//        int count = quizDao.countQuiz(content, subjectID, statusID);
//        List<QuizDTO> listQ = quizDao.searchQuiz(content, subjectID, statusID, indexQ, size);
//
//        endPageQ = count / size;
//        if (count % size != 0) {
//            endPageQ++;
//        }
//        session.setAttribute("endPageQ", endPageQ);
//        session.setAttribute("LIST_Q", listQ);
//        session.setAttribute("indexQ", indexQ);
    }

    private void searchAllSubjectIdAndName_NoPaging(HttpSession session) throws SQLException, NamingException {
        SubjectDAO subjectDao = new SubjectDAO();
        List<SubjectDTO> list = subjectDao.getAllSubjectIdAndName_NoPaging();
        session.setAttribute("LIST_ALLSUBJECT", list);
    }

    private void searchAllStatus_NoPaging(HttpSession session) throws SQLException, NamingException {
        StatusDAO statusDao = new StatusDAO();
        List<String> listStatusID = statusDao.getAllStatus_NoPaging();
        session.setAttribute("LIST_STATUS_ID", listStatusID);
    }

    private void getQIDQNameBySID(HttpServletRequest request, HttpSession session) throws SQLException, NamingException {
        //String subjectID = request.getParameter("tSID_GetSToQ");
        String subjectID = "";
        QuizDAO quizDao = new QuizDAO();
        List<QuizDTO> listQuizID = quizDao.getQuizIDQuizNameBySubjectID(subjectID);
        session.setAttribute("listQzID_From_GetSToQz__SearchQs_SignIn", listQuizID);
        session.setAttribute("tSID_From_GetSToQ", subjectID);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
