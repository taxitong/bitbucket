/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.cons;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nhatlq.daos.AccountDAO;
import nhatlq.daos.StatusDAO;
import nhatlq.dtos.AccountDTO;

/**
 *
 * @author Admin
 */
public class PreVerifyACon extends HttpServlet {

    private static final String ERROR = "error.jsp";
    private static final String SUCCESS = "verifyA.jsp";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            HttpSession session = request.getSession();
            searchAcc(request, session);//for display list of accounts
            searchAllStatus_NoPaging(session);//for display list of status next to each accounts, and then use it to update the acc status
            url = SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    private void searchAcc(HttpServletRequest request, HttpSession session) throws SQLException, NamingException {
        AccountDAO accountDao = new AccountDAO();
        String email = "";
        int indexA = Integer.parseInt(request.getParameter("indexA"));

        int size = 3;
        int endPageA;

        int count = accountDao.countAccount(email);
        List<AccountDTO> list = accountDao.searchAccount(email, indexA, size);

        endPageA = count / size;
        if (count % size != 0) {
            endPageA++;
        }
        session.setAttribute("endPageA", endPageA);
        session.setAttribute("LIST_ACCOUNT", list);
        session.setAttribute("indexA", indexA);
    }

    private void searchAllStatus_NoPaging(HttpSession session) throws SQLException, NamingException {
        StatusDAO statusDao = new StatusDAO();
        List<String> listStatus = statusDao.getAllStatus_NoPaging();
        session.setAttribute("LIST_STATUS", listStatus);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
