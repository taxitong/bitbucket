/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.cons;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nhatlq.daos.QuizDAO;
import nhatlq.dtos.AccountDTO;
import nhatlq.dtos.QuizDTO;

/**
 *
 * @author Admin
 */
public class GetSToQzCon extends HttpServlet {

    private final static String ERROR = "error.jsp";
    private final static String SUCCESS_MAIN_AD_JSP = "mainAd.jsp";

    //attributes in the database  
    private static final String ADMIN_ROLE_ID = "admin";
    private static final String STUDENT_ROLE_ID = "student";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            HttpSession session = request.getSession();
            AccountDTO accountDto = (AccountDTO) session.getAttribute("LOGIN_ACCOUNT");
            String roleID = accountDto.getRoleID();
            switch (roleID) {
                case ADMIN_ROLE_ID:
                    getQzIDBySID(request, session);
                    url = SUCCESS_MAIN_AD_JSP;
                    break;
                case STUDENT_ROLE_ID:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            response.sendRedirect(url);
        }
    }

    private void getQzIDBySID(HttpServletRequest request, HttpSession session) throws SQLException, NamingException {
        String subjectID = request.getParameter("tSID_GetSToQz");
        String statusID = request.getParameter("tQzStatusID_GetSToQz");
        String content = request.getParameter("tQsContent_GetSToQz");
        
        QuizDAO quizDao = new QuizDAO();
        List<QuizDTO> listQuiz = quizDao.getQuizIDQuizNameBySubjectID(subjectID);
        session.setAttribute("listQzID_From_GetSToQz__SearchQs_SignIn", listQuiz);
        
        session.setAttribute("tSID_SearchQs", subjectID);
        session.setAttribute("tQsStatusID_SearchQs", statusID);
        session.setAttribute("tQsContent_SearchQs", content);
        //refresh list searched question
        session.setAttribute("LIST_Qs", null);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
