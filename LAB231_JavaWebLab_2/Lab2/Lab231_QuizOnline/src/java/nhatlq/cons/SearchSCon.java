/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.cons;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nhatlq.daos.SubjectDAO;
import nhatlq.dtos.AccountDTO;
import nhatlq.dtos.SubjectDTO;

/**
 *
 * @author Admin
 */
public class SearchSCon extends HttpServlet {

    //page
    private final static String ERROR = "error.jsp";
    private final static String SUCCESS_AD_SEARCH_SUBJECT_JSP = "viewSAd.jsp";
    private final static String SUCCESS_STU_MAIN_STU_JSP = "mainStu.jsp";
    //attributes in the database  
    private static final String ADMIN_ROLE_ID = "admin";
    private static final String STUDENT_ROLE_ID = "student";
    //record each page
    private static final int SUBJECT_EACH_PAGE = 3;//each page has 3 product;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            HttpSession session = request.getSession(false);
            AccountDTO accountDto = (AccountDTO) session.getAttribute("LOGIN_ACCOUNT");
            String roleID = accountDto.getRoleID();
            searchSubject_Paging(request, session, roleID);
            switch (roleID) {
                case ADMIN_ROLE_ID:
                    url = SUCCESS_AD_SEARCH_SUBJECT_JSP;
                    break;
                case STUDENT_ROLE_ID:
                    url = SUCCESS_STU_MAIN_STU_JSP;
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    private void searchSubject_Paging(HttpServletRequest request, HttpSession session, String roleID) throws SQLException, NamingException {
        SubjectDAO subjectDao = new SubjectDAO();
        String subjectName = request.getParameter("tSID_SearchS");

        int indexS = Integer.parseInt(request.getParameter("indexS"));
        int size = SUBJECT_EACH_PAGE;
        int endPageS;

        int count = subjectDao.countSubject(subjectName, roleID);
        List<SubjectDTO> listS = subjectDao.searchSubject(subjectName, indexS, size, roleID);

        endPageS = count / size;
        if (count % size != 0) {
            endPageS++;
        }
        session.setAttribute("endPageS", endPageS);
        session.setAttribute("LIST_S", listS);
        session.setAttribute("indexS", indexS);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
