/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.cons;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Random;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nhatlq.daos.QuestionDAO;
import nhatlq.dtos.QuestionDTO;
import nhatlq.utils.Encryption;

/**
 *
 * @author Admin
 */
public class CreateQsCon extends HttpServlet {

    private final static String ERROR = "error.jsp";
    private final static String SUCCESS = "createQs.jsp";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            HttpSession session = request.getSession(false);
            url = createQuiz(request, session);
            rePrepareCreateQues(request, session);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //request.getRequestDispatcher(url).forward(request, response);
            response.sendRedirect(url);
        }
    }

    private String createQuiz(HttpServletRequest request, HttpSession session) throws SQLException, NamingException {
        String result = ERROR;
        String quizID = request.getParameter("tQzID_CreateQs");
        String statusID = request.getParameter("tQsStatusID_CreateQs");
        String content = request.getParameter("tQsContent_CreateQs");

        String optionA = request.getParameter("tQsOptionA_CreateQs");
        String optionB = request.getParameter("tQsOptionB_CreateQs");
        String optionC = request.getParameter("tQsOptionC_CreateQs");
        String optionD = request.getParameter("tQsOptionD_CreateQs");
        String answer = request.getParameter("tQsAnswer_CreateQs");

        String quesID = generateQuesID();
        QuestionDTO questionDto = new QuestionDTO(quesID, quizID, statusID, content, optionA, optionB, optionC, optionD, answer);
        QuestionDAO questionDao = new QuestionDAO();
        boolean check = questionDao.insertQuiz(questionDto);
        if (check) {
            result = SUCCESS;
        }
        return result;
    }

    private void rePrepareCreateQues(HttpServletRequest request, HttpSession session) {
        String subjectID = request.getParameter("tSID_CreateQs");
        String subjectName = request.getParameter("tSName_CreateQs");
        String quizID = request.getParameter("tQzID_CreateQs");
        String quizName = request.getParameter("tQzName_CreateQs");

        session.setAttribute("tSID_From_CreateQs", subjectID);
        session.setAttribute("tSName_From_CreateQs", subjectName);
        session.setAttribute("tQzID_From_CreateQs", quizID);
        session.setAttribute("tQzName_From_CreateQs", quizName);
    }

    private String generateQuesID() {
        String result = null;

        Random rd = new Random();//https://www.tutorialspoint.com/generate-random-bytes-in-java
        byte[] arr = new byte[7];
        rd.nextBytes(arr);

        String createDate = "" + new Timestamp(System.currentTimeMillis());
        Encryption en = new Encryption();
        try {
            result = en.toHexString(en.getSHA(Arrays.toString(arr) + createDate));
        } catch (NoSuchAlgorithmException e) {
        }
        return result;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
