/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.cons;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nhatlq.daos.QuestionDAO;
import nhatlq.dtos.QuestionDTO;

/**
 *
 * @author Admin
 */
public class UpdateQsCon extends HttpServlet {

    private static final String SUCCESS = "mainAd.jsp";
    private static final String ERROR = "error.jsp";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            String quizID = request.getParameter("tQsID_UpdateQs");
            String statusID = request.getParameter("tQsStatusID_UpdateQs");
            String content = request.getParameter("tQsContent_UpdateQs");

            String optionA = request.getParameter("tQsOptionA_UpdateQs");
            String optionB = request.getParameter("tQsOptionB_UpdateQs");
            String optionC = request.getParameter("tQsOptionC_UpdateQs");
            String optionD = request.getParameter("tQsOptionD_UpdateQs");
            String answer = request.getParameter("tQsAnswer_UpdateQs");
            QuestionDTO questionDto = new QuestionDTO(quizID, "", statusID, content, optionA, optionB, optionC, optionD, answer);
            QuestionDAO questionDao = new QuestionDAO();
            boolean check = questionDao.changeContent(questionDto);
            if (check) {
                url = SUCCESS;
            }
            //refresh list searched question
            HttpSession session = request.getSession();
            session.setAttribute("LIST_Qs", null);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //request.getRequestDispatcher(url).forward(request, response);
            response.sendRedirect(url);
        }
    }

    private int findIndex(HttpServletRequest request) {
        int indexQ;
        HttpSession session = request.getSession(false);
        try {
            indexQ = (int) session.getAttribute("indexQ");
        } catch (Exception e) {
            indexQ = 1;
        }
        return indexQ;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
