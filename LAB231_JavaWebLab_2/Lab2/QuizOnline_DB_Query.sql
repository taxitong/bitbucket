CREATE DATABASE QuizOnline_DB
CREATE TABLE tblStatus (
	statusID VARCHAR(40) PRIMARY KEY,
	statusName NVARCHAR(40) NOT NULL,
)
CREATE TABLE tblAccountRole (
	roleID VARCHAR(40) PRIMARY KEY,
	roleName NVARCHAR(40) NOT NULL,
);
CREATE TABLE tblAccount (
	email NVARCHAR(254) PRIMARY KEY,
	displayName NVARCHAR(50) NOT NULL,
	password CHAR(64) NOT NULL,
	
	roleID VARCHAR(40) NOT NULL,
	statusID VARCHAR(40) NOT NULL,

	FOREIGN KEY(roleID) REFERENCES dbo.tblAccountRole(roleID),
	FOREIGN KEY(statusID) REFERENCES dbo.tblStatus(statusID),
);
CREATE TABLE tblSubject (
	subjectID NVARCHAR(10) PRIMARY KEY,
	subjectName NVARCHAR(100) NOT NULL,
	secondsToDoQuiz int,
	quizzesEachExam int,

	statusID VARCHAR(40) NOT NULL,
	FOREIGN KEY(statusID) REFERENCES dbo.tblStatus(statusID)
)
CREATE TABLE tblQuiz(
	quizID CHAR(64) PRIMARY KEY,
	subjectID NVARCHAR(10),
	statusID VARCHAR(40),
	content NVARCHAR(3000),
	optionA NVARCHAR(3000),
	optionB NVARCHAR(3000),
	optionC NVARCHAR(3000),
--2
CREATE DATABASE QuizOnline_DB
CREATE TABLE tblStatus (
	statusID VARCHAR(40) PRIMARY KEY,
	statusName NVARCHAR(40) NOT NULL,
)
CREATE TABLE tblAccountRole (
	roleID VARCHAR(40) PRIMARY KEY,
	roleName NVARCHAR(40) NOT NULL,
);
CREATE TABLE tblAccount (
	email NVARCHAR(254) PRIMARY KEY,
	displayName NVARCHAR(50) NOT NULL,
	password CHAR(64) NOT NULL,
	
	roleID VARCHAR(40) NOT NULL,
	statusID VARCHAR(40) NOT NULL,

	FOREIGN KEY(roleID) REFERENCES dbo.tblAccountRole(roleID),
	FOREIGN KEY(statusID) REFERENCES dbo.tblStatus(statusID),
);
CREATE TABLE tblSubject (
	subjectID NVARCHAR(10) PRIMARY KEY,
	subjectName NVARCHAR(100) NOT NULL,

	statusID VARCHAR(40) NOT NULL,
	FOREIGN KEY(statusID) REFERENCES dbo.tblStatus(statusID)
)
CREATE TABLE tblQuiz(
	quizID CHAR(64) PRIMARY KEY,
	subjectID NVARCHAR(10),
	quizName NVARCHAR(10),

	secondsToDoQuiz int,
	quesEachExam int,
	noOfAllowedAttempt int,
	comment NVARCHAR(1000),

	statusID VARCHAR(40) NOT NULL,
	FOREIGN KEY(subjectID) REFERENCES dbo.tblSubject(subjectID),
	FOREIGN KEY(statusID) REFERENCES dbo.tblStatus(statusID)
)
CREATE TABLE tblQuestion(
	quesID CHAR(64) PRIMARY KEY,
	quizID CHAR(64),
	statusID VARCHAR(40),
	content NVARCHAR(3000),

	optionA NVARCHAR(3000),
	optionB NVARCHAR(3000),
	optionC NVARCHAR(3000),
	optionD NVARCHAR(3000),
	answer NVARCHAR(1),

	FOREIGN KEY(quizID) REFERENCES dbo.tblQuiz(quizID),
	FOREIGN KEY(statusID) REFERENCES dbo.tblStatus(statusID)
);
CREATE TABLE tblExam(
	examID CHAR(64) PRIMARY KEY,
	quizID CHAR(64),
	startTime DATETIME,
	submitTime DATETIME,

	email NVARCHAR(254),
	totalQues INT,
	totalCorrect INT,
	result INT,
	FOREIGN KEY(email) REFERENCES dbo.tblAccount(email),
	FOREIGN KEY(quizID) REFERENCES dbo.tblQuiz(quizID),
);
CREATE TABLE tblExamDetails(
	examDetailsID CHAR(64) PRIMARY KEY,
	examID CHAR(64),

	quesID CHAR(64),
	quizID_Save NVARCHAR(10),
	content_Save NVARCHAR(3000),
	optionA_Save NVARCHAR(3000),
	optionB_Save NVARCHAR(3000),
	optionC_Save NVARCHAR(3000),
	optionD_Save NVARCHAR(3000),
	correctAnswer_Save NVARCHAR(1),

	chosenAnswer NVARCHAR(1),  
	isCorrect bit,
	FOREIGN KEY(examID) REFERENCES dbo.tblExam(examID),
	FOREIGN KEY(quesID) REFERENCES dbo.tblQuestion(quesID),
);
USE Lab231_QuizOnline_DB