/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.dtos;

import java.io.Serializable;

/**
 *
 * @author Admin
 */
public class ProductDTO implements Serializable{

    private String productID;
    private String productName;
    private float price;
    private int quantityInStock;
    private String createDate;
    private String image;
    private String description;
    //
    private String categoryID;
    private String statusID;
    //
    int quantityToBuy;

    public ProductDTO() {}

    //for get prodcut value, insert
    public ProductDTO(String productID, String productName, float price, int quantityInStock, String createDate, String image, String description, String categoryID, String statusID) {
        this.productID = productID;
        this.productName = productName;
        this.price = price;
        this.quantityInStock = quantityInStock;
        this.createDate = createDate;
        this.image = image;
        this.description = description;
        this.categoryID = categoryID;
        this.statusID = statusID;
    }
    //for cart
    public ProductDTO(String productID, String productName, float price, int quantityInStock, String createDate, String image, String description, String categoryID, String statusID, int quantityToBuy) {
        this.productID = productID;
        this.productName = productName;
        this.price = price;
        this.quantityInStock = quantityInStock;
        this.createDate = createDate;
        this.image = image;
        this.description = description;
        this.categoryID = categoryID;
        this.statusID = statusID;
        this.quantityToBuy = quantityToBuy;
    }
    //for update
    public ProductDTO(String productID, String productName, float price, int quantityInStock, String image, String description, String categoryID, String statusID) {
        this.productID = productID;
        this.productName = productName;
        this.price = price;
        this.quantityInStock = quantityInStock;
        this.image = image;
        this.description = description;
        this.categoryID = categoryID;
        this.statusID = statusID;
    }
    //for update
    public ProductDTO(String productID, String productName, float price, int quantityInStock, String description, String categoryID, String statusID) {
        this.productID = productID;
        this.productName = productName;
        this.price = price;
        this.quantityInStock = quantityInStock;
        this.description = description;
        this.categoryID = categoryID;
        this.statusID = statusID;
    }
    
    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getQuantityInStock() {
        return quantityInStock;
    }

    public void setQuantityInStock(int quantityInStock) {
        this.quantityInStock = quantityInStock;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(String categoryID) {
        this.categoryID = categoryID;
    }

    public String getStatusID() {
        return statusID;
    }

    public void setStatusID(String statusID) {
        this.statusID = statusID;
    }

    public int getQuantityToBuy() {
        return quantityToBuy;
    }

    public void setQuantityToBuy(int quantityToBuy) {
        this.quantityToBuy = quantityToBuy;
    }

}