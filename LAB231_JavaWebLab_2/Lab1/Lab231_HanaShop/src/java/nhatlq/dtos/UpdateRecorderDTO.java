/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.dtos;

/**
 *
 * @author Admin
 */
public class UpdateRecorderDTO {

    String updateRecorderID;
    String productID;
    int accountID;
    String dateUpdate;

    public UpdateRecorderDTO() {
    }

    public UpdateRecorderDTO(String updateRecorderID, String productID, int accountID, String dateUpdate) {
        this.updateRecorderID = updateRecorderID;
        this.productID = productID;
        this.accountID = accountID;
        this.dateUpdate = dateUpdate;
    }

    public String getUpdateRecorderID() {
        return updateRecorderID;
    }

    public void setUpdateRecorderID(String updateRecorderID) {
        this.updateRecorderID = updateRecorderID;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public int getAccountID() {
        return accountID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    public String getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(String dateUpdate) {
        this.dateUpdate = dateUpdate;
    }
    
}
