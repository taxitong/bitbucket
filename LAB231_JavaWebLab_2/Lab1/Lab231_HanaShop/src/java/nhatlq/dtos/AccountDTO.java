/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.dtos;

import java.io.Serializable;

/**
 *
 * @author Admin
 */
public class AccountDTO implements Serializable{
    private int accountID;
    private String username;
    //private String password;
    private String displayName;
    private String phoneNo;
    private String address;
    private String roleID;
    private String statusID;

    public AccountDTO() {}

    public AccountDTO(int accountID, String username, String displayName, String phoneNo, String address, String roleID, String statusID) {
        this.accountID = accountID;
        this.username = username;
        this.displayName = displayName;
        this.phoneNo = phoneNo;
        this.address = address;
        this.roleID = roleID;
        this.statusID = statusID;
    }

    public int getAccountID() {
        return accountID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRoleID() {
        return roleID;
    }

    public void setRoleID(String roleID) {
        this.roleID = roleID;
    }

    public String getStatusID() {
        return statusID;
    }

    public void setStatusID(String statusID) {
        this.statusID = statusID;
    }
    
}
