/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.dtos;

import java.io.Serializable;

/**
 *
 * @author Admin
 */
public class OrderDTO implements Serializable{

    private int orderID;
    private float total;
    private String dateCreate;

    private int accountID;
    private String username_tblOrder;
    private String phone_tblOrder;
    private String address_tblOrder;

    public OrderDTO() {
    }

    public OrderDTO(int orderID, float total, String dateCreate, int accountID, String username_tblOrder, String phone_tblOrder, String address_tblOrder) {
        this.orderID = orderID;
        this.total = total;
        this.dateCreate = dateCreate;
        this.accountID = accountID;
        this.username_tblOrder = username_tblOrder;
        this.phone_tblOrder = phone_tblOrder;
        this.address_tblOrder = address_tblOrder;
    }

    public OrderDTO(float total, String dateCreate, int accountID, String username_tblOrder, String phone_tblOrder, String address_tblOrder) {
        this.total = total;
        this.dateCreate = dateCreate;
        this.accountID = accountID;
        this.username_tblOrder = username_tblOrder;
        this.phone_tblOrder = phone_tblOrder;
        this.address_tblOrder = address_tblOrder;
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public String getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(String dateCreate) {
        this.dateCreate = dateCreate;
    }

    public int getAccountID() {
        return accountID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    public String getUsername_tblOrder() {
        return username_tblOrder;
    }

    public void setUsername_tblOrder(String username_tblOrder) {
        this.username_tblOrder = username_tblOrder;
    }

    public String getPhone_tblOrder() {
        return phone_tblOrder;
    }

    public void setPhone_tblOrder(String phone_tblOrder) {
        this.phone_tblOrder = phone_tblOrder;
    }

    public String getAddress_tblOrder() {
        return address_tblOrder;
    }

    public void setAddress_tblOrder(String address_tblOrder) {
        this.address_tblOrder = address_tblOrder;
    }

    
}
