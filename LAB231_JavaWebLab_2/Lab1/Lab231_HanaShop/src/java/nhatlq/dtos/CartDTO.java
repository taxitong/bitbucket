/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.dtos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Admin
 */
public class CartDTO implements Serializable{

    private Map<String, ProductDTO> cart;

    public CartDTO() {
    }

    public CartDTO(Map<String, ProductDTO> cart) {
        this.cart = cart;
    }

    public Map<String, ProductDTO> getCart() {
        return cart;
    }

    public void setCart(Map<String, ProductDTO> cart) {
        this.cart = cart;
    }

    public void add(ProductDTO product) {
        if (cart == null) {
            cart = new HashMap<>();
        }
        if (this.cart.containsKey(product.getProductID())) {
            int quantityToBuy = cart.get(product.getProductID()).getQuantityToBuy() + product.getQuantityToBuy();
            if (quantityToBuy > product.getQuantityInStock()) {
                quantityToBuy = product.getQuantityInStock();
            }
            product.setQuantityToBuy(quantityToBuy);
        }
        cart.put(product.getProductID(), product);
    }

    public void delete(String id) {
        if (cart == null) {
            return;
        }
        if (cart.containsKey(id)) {
            cart.remove(id);
        }
    }

    public void update(String id, ProductDTO product) {
        if (cart != null) {
            if (cart.containsKey(id)) {
                cart.replace(id, product);
            }
        }
    }

    public boolean isCartEmpty() {
        return cart.isEmpty();
    }
}
