/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.cons;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nhatlq.dtos.AccountDTO;
import nhatlq.dtos.CartDTO;
import nhatlq.dtos.ProductDTO;

/**
 *
 * @author Admin
 */
public class CartActionCon extends HttpServlet {

    private static final String ERROR = "error.jsp";
    private static final String SUCCESS_UN = "cartUn.jsp";
    private static final String SUCCESS_US = "cartUs.jsp";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        String action = request.getParameter("btnAction");
        try {
            HttpSession session = request.getSession();
            CartDTO cartDto = (CartDTO) session.getAttribute("CART");
            if (action.equals("Update")) {
                String productID = request.getParameter("txtProductID");
                String productName = request.getParameter("txtProductName");
                float price = Float.parseFloat(request.getParameter("txtPrice"));
                int quantityInStock = Integer.parseInt(request.getParameter("txtQuantityInStock"));
                String categoryID = request.getParameter("txtCategoryID");
                String createDate = request.getParameter("txtCreateDate");
                String image = request.getParameter("txtImage");
                String statusID = request.getParameter("txtStatusID");
                String description = request.getParameter("txtDescription");
                int quantityToBuy = Integer.parseInt(request.getParameter("txtQuantityToBuy"));
                //HttpSession session = request.getSession();
                //CartDTO cartDto = (CartDTO) session.getAttribute("CART");
                ProductDTO proDto = null;
                for (ProductDTO proTmp : cartDto.getCart().values()) {
                    if (proTmp.getProductID().equals(productID)) {
                        proDto = new ProductDTO(productID, productName, price, quantityInStock, createDate, image, description, categoryID, statusID, quantityToBuy);
                        break;
                    }
                }
                cartDto.update(productID, proDto);
                session.setAttribute("CART", cartDto);
            } else if (action.equals("Remove")) {
                String id = request.getParameter("txtProductID");
                //HttpSession session = request.getSession();
                //CartDTO cartDto = (CartDTO) session.getAttribute("CART");
                cartDto.delete(id);

                if (cartDto.isCartEmpty()) {
                    session.setAttribute("CART", null);
                } else {
                    session.setAttribute("CART", cartDto);
                }
            }
            AccountDTO accountDto_User = (AccountDTO) session.getAttribute("LOGIN_USER");
            if (accountDto_User != null) {
                url = SUCCESS_US;
            } else {
                url = SUCCESS_UN;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
