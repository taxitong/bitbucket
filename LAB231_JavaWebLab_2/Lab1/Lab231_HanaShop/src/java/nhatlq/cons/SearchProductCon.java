/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.cons;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nhatlq.daos.ProductDAO;
import nhatlq.dtos.AccountDTO;
import nhatlq.dtos.ProductDTO;

/**
 *
 * @author Admin
 */
public class SearchProductCon extends HttpServlet {

    private final static String ERROR_JSP = "error.jsp";
    private final static String SUCCESS_AD_JSP = "searchAd.jsp";
    private final static String SUCCESS_UN_JSP = "searchUn.jsp";
    private final static String SUCCESS_US_JSP = "searchUs.jsp";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR_JSP;
        try {
            ProductDAO productDao = new ProductDAO();
            String search = request.getParameter("txtSearch");
            String minNum = request.getParameter("txtSearch_MinNum");
            String maxNum = request.getParameter("txtSearch_MaxNum");
            float realMinNum = 1;
            float realMaxNum = productDao.getMaxPrice();
            if (!minNum.isEmpty()) {
                realMinNum = Float.parseFloat(minNum);
            }
            if (!maxNum.isEmpty()) {
                realMaxNum = Float.parseFloat(maxNum);
            }
            String categoryID = request.getParameter("txtSearch_CategoryID");
            int index = Integer.parseInt(request.getParameter("index"));

            int count = 0;
            int size = 20; //each page has n=20 product;
            int endPage = 0;
            List<ProductDTO> list = null;

            HttpSession session = request.getSession();
            AccountDTO accountDto_User = (AccountDTO) session.getAttribute("LOGIN_USER");
            AccountDTO accountDto_Admin = (AccountDTO) session.getAttribute("LOGIN_ADMIN");

            if (accountDto_Admin != null) {
                url = SUCCESS_AD_JSP;
                count = productDao.countAllProduct_Admin(search, realMinNum, realMaxNum, categoryID);
                list = productDao.searchProduct_Admin(search, realMinNum, realMaxNum, categoryID, index, size);
            } else {
                if (accountDto_User != null) {
                    url = SUCCESS_US_JSP;
                } else {
                    url = SUCCESS_UN_JSP;
                }
                count = productDao.countAllProduct_User(search, realMinNum, realMaxNum, categoryID);
                list = productDao.searchProduct_User(search, realMinNum, realMaxNum, categoryID, index, size);
            }
            endPage = count / size;
            if (count % size != 0) {
                endPage++;
            }
            session.setAttribute("endPage", endPage);
            session.setAttribute("LIST_PRODUCT", list);
            session.setAttribute("txtSearch", search);
            session.setAttribute("index", index);
            session.setAttribute("txtSearch_MinNum", minNum);
            session.setAttribute("txtSearch_MaxNum", maxNum);
            session.setAttribute("txtSearch_CategoryID", categoryID);

            session.setAttribute("MAX_PRICE", maxNum);//help monitor category & maxPrice changes
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
