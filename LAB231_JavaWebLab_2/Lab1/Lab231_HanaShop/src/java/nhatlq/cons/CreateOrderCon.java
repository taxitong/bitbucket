/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.cons;

import java.io.IOException;
import java.sql.Timestamp;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nhatlq.daos.OrderDAO;
import nhatlq.daos.OrderDetailsDAO;
import nhatlq.daos.ProductDAO;
import nhatlq.dtos.AccountDTO;
import nhatlq.dtos.CartDTO;
import nhatlq.dtos.OrderDTO;
import nhatlq.dtos.OrderDetailsDTO;
import nhatlq.dtos.ProductDTO;

/**
 *
 * @author Admin
 */
public class CreateOrderCon extends HttpServlet {

    private static final String ERROR = "error.jsp";
    private static final String SUCCESS_US = "orderNotice_SuccessfullUs.jsp";
    private static final String FAILED_US = "orderNotice_FailedUs.jsp";
//    private static final String SUCCESS_UN = "orderNotice_SuccessfullUn.jsp";
//    private static final String FAILED_UN = "orderNotice_FailedUn.jsp";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            HttpSession session = request.getSession();
            CartDTO cartDTO = (CartDTO) session.getAttribute("CART");
            AccountDTO accDto = (AccountDTO) session.getAttribute("LOGIN_USER");
            //create Order
            float totalOrder = Float.parseFloat(request.getParameter("txtTotalOrder"));
            String dateCreate = "" + new Timestamp(System.currentTimeMillis());
            int accountID_tblOrder;

            accountID_tblOrder = accDto.getAccountID();//logged in
            String username_tblOrder = request.getParameter("txtName_CreateOrder");
            String phone_tblOrder = request.getParameter("txtPhoneNo_CreateOrder");
            String address_tblOrder = request.getParameter("txtAddress_CreateOrder");
            OrderDTO orderDTO = new OrderDTO(totalOrder, dateCreate, accountID_tblOrder, username_tblOrder, phone_tblOrder, address_tblOrder);
            //insert the order into database
            OrderDAO orderDao = new OrderDAO();
            int orderID = orderDao.insertOder(orderDTO);
            //check quantity of each product in database
            for (ProductDTO proDto : cartDTO.getCart().values()) {
                ProductDAO proDao = new ProductDAO();
                String productID = proDto.getProductID();
                float price = proDto.getPrice();
                int quantityToBuy = proDto.getQuantityToBuy();
                int quantityInStock = proDao.getQuantiyInStock(productID);
                if (quantityToBuy > quantityInStock) {
                    orderDao.deleteOder(orderID);
                    //cartDTO.clearCart();
                    session.setAttribute("CART", null);
                    //url = FAILED;
                    url = FAILED_US;
                } else {
                    int newQuantityInStock = quantityInStock - quantityToBuy;//calculate new quantity
                    proDao.updateProduct(productID, newQuantityInStock);//update quantity of product
                    //create and add new Order Details
                    OrderDetailsDTO orderDetailsDto = new OrderDetailsDTO(orderID, productID, price, quantityToBuy);
                    OrderDetailsDAO orderDetailsDao = new OrderDetailsDAO();
                    orderDetailsDao.insertOrderDetails(orderDetailsDto);
                    //cartDTO.clearCart();
                    session.setAttribute("CART", null);
                    //url = SUCCESS;
                    url = SUCCESS_US;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
