/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.cons;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Random;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nhatlq.daos.ProductDAO;
import nhatlq.daos.UpdateRecorderDAO;
import nhatlq.dtos.AccountDTO;
import nhatlq.dtos.UpdateRecorderDTO;
import nhatlq.utils.MyTools;

/**
 *
 * @author Admin
 */
public class DeleteProductCon extends HttpServlet {

    private static final String SUCCESS = "MainCon?btnAction=SearchProductCon&index=1&txtSearch=&txtSearch_MinNum=&txtSearch_MaxNum=&txtSearch_CategoryID=";
    private static final String ERROR = "error.jsp";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            String productID = request.getParameter("txtProductID");
            ProductDAO productDao = new ProductDAO();
            boolean checkDeleteAsUpdate = productDao.deleteProductAsUpdate(productID);
            if (checkDeleteAsUpdate) {
                HttpSession session = request.getSession(false);
                //1record update
                AccountDTO accDto = (AccountDTO) session.getAttribute("LOGIN_ADMIN");
                int accountID = accDto.getAccountID();
                String updateDate = "" + new Timestamp(System.currentTimeMillis());
                //id for UpdateRecorder
                String updateRecorderID = generateProductID(productID, productID, updateDate);
                UpdateRecorderDTO updateRecorderDto = new UpdateRecorderDTO(updateRecorderID, productID, accountID, updateDate);
                UpdateRecorderDAO updateRecorderDao = new UpdateRecorderDAO();
                updateRecorderDao.inserUpdateRecorder(updateRecorderDto);
                //0record update
                url = SUCCESS;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            response.sendRedirect(url);
        }
    }

    String generateProductID(String productID, String accountID, String updateDate) {
        String result = null;
        //https://www.tutorialspoint.com/generate-random-bytes-in-java
        Random rd = new Random();
        byte[] arr = new byte[7];
        rd.nextBytes(arr);
        MyTools mt = new MyTools();
        try {
            result = mt.toHexString(mt.getSHA(arr + productID + accountID + updateDate));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
