/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.cons;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nhatlq.daos.AccountDAO;
import nhatlq.dtos.AccountDTO;

/**
 *
 * @author Admin
 */
public class LoginCon extends HttpServlet {
    
    private final static String ERROR = "login.jsp";
    private final static String SUCCESS_AD_JSP = "mainAd.jsp";
    private final static String SUCCESS_US_JSP = "mainUs.jsp";
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            String username = request.getParameter("txtUsername");
            String password = request.getParameter("txtPassword");
            AccountDAO accountDao = new AccountDAO();
            AccountDTO accountDto = accountDao.checkLogin(username, password);
            if (accountDto != null && "active".equals(accountDto.getStatusID())) {
                HttpSession session = request.getSession();
                switch (accountDto.getRoleID()) {
                    case "admin":
                        session.setAttribute("LOGIN_ADMIN", accountDto);
                        session.setAttribute("LOGIN_USER", null);
                        url = SUCCESS_AD_JSP;
                        break;
                    case "user":
                        session.setAttribute("LOGIN_USER", accountDto);
                        session.setAttribute("LOGIN_ADMIN", null);
                        url = SUCCESS_US_JSP;
                        break;
                }
            } else {
                request.setAttribute("invalidAccount", "Invalid Account. Please check username & password again");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
