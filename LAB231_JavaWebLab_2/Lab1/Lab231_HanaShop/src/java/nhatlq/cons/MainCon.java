/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.cons;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nhatlq.daos.CategoryDAO;
import nhatlq.daos.StatusDAO;
import nhatlq.dtos.CategoryDTO;
import nhatlq.dtos.StatusDTO;

/**
 *
 * @author Admin
 */
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
        maxFileSize = 1024 * 1024 * 10, // 10MB
        maxRequestSize = 1024 * 1024 * 50)
public class MainCon extends HttpServlet {

    private final static String ERROR_JSP = "error.jsp";

    private static final String LOGIN_CON = "LoginCon";
    private static final String LOGIN_JSP = "login.jsp";
    private static final String LOGOUT_CON = "LogoutCon";
    private static final String SEARCH_CON = "SearchProductCon";
    private static final String PRODUCT_DETAILS_CON = "ProductDetailsCon";
    private static final String ADD_TO_CART_CON = "AddToCartCon";
    private static final String CART_ACTION_CON = "CartActionCon";
    private static final String CREATE_ORDER_CON = "CreateOrderCon";
    private static final String CREATE_PRODUCT_CON = "CreateProductCon";
    private static final String PRE_UPDATE_PRODUCT_CON = "PreUpdateProductCon";
    private static final String UPDATE_PRODUCT_CON = "UpdateProductCon";
    private static final String DELETE_PRODUCT_CON = "DeleteProductCon";
    private static final String TRACKING_FIRST_CON = "TrackingCon?txtDateRange=&txtReceiverName=&index=1";
    private static final String TRACKING_CON = "TrackingCon?index=1";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR_JSP;
        try {
            String action = request.getParameter("btnAction");
            switch (action) {
                case "LoginCon":
                    url = LOGIN_CON;
                    break;
                case "Log in now":
                    url = LOGIN_JSP;
                    break;
                case "LogoutCon":
                    url = LOGOUT_CON;
                    break;
                case "SearchProductCon":
                    url = SEARCH_CON;
                    break;
                case "View Details":
                    url = PRODUCT_DETAILS_CON;
                    break;
                case "Add to Cart":
                    url = ADD_TO_CART_CON;
                    break;
                case "Update":
                    url = CART_ACTION_CON;
                    break;
                case "Remove":
                    url = CART_ACTION_CON;
                    break;
                case "Buy now":
                    url = CREATE_ORDER_CON;
                    break;
                case "Create Product":
                    url = CREATE_PRODUCT_CON;
                    break;
                case "Edit":
                    url = PRE_UPDATE_PRODUCT_CON;
                    break;
                case "Edit now":
                    url = UPDATE_PRODUCT_CON;
                    break;
                case "Delete":
                    url = DELETE_PRODUCT_CON;
                    break;
                case "TrackingFirstCon":
                    url = TRACKING_FIRST_CON;
                    break;
                case "Search Order":
                    url = TRACKING_CON;
                    break;
            }
            //help monitor category, status changes
            CategoryDAO categoryDao = new CategoryDAO();
            StatusDAO statusDao = new StatusDAO();
            List<CategoryDTO> listCategory = categoryDao.getAllCategory();
            List<StatusDTO> listStatus = statusDao.getAllStatus();

            HttpSession session = request.getSession();
            session.setAttribute("LIST_CATEGORY", listCategory);
            session.setAttribute("LIST_STATUS", listStatus);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
