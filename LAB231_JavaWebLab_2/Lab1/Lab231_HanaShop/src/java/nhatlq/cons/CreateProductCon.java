/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.cons;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nhatlq.daos.ProductDAO;
import nhatlq.dtos.ProductDTO;
import nhatlq.utils.MyTools;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;

/**
 *
 * @author Admin
 */
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
        maxFileSize = 1024 * 1024 * 10, // 10MB
        maxRequestSize = 1024 * 1024 * 50)
public class CreateProductCon extends HttpServlet {

    private final static String ERROR = "error.jsp";
    private final static String SUCCESS = "mainAd.jsp";

    public static final String UPLOAD_DIR = "images";
    public String dbFileName = "";
    //String s = request.getSession().getServletContext().getRealPath("/");
    //System.out.println("s: " + s);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            HttpSession session = request.getSession(false);
//            AccountDTO accDto = (AccountDTO) session.getAttribute("LOGIN_ADMIN");

            String productName = "";
            float price = 0;
            int quantityInStock = 0;
            String categoryID = "";
            String createDate = "";
            String description = "";

            String saveFileName = "";
            boolean check = true;
            //https://stackoverflow.com/questions/2422468/how-to-upload-files-to-server-using-jsp-servlet
            int count = 1;
            List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
            for (FileItem item : items) {
                if (item.isFormField()) {
                    // Process regular form field (input type="text|radio|checkbox|etc", select, etc).
                    switch (count) {
                        case 1:
                            productName = item.getString();
                            break;
                        case 2:
                            price = Float.parseFloat(item.getString());
                            break;
                        case 3:
                            quantityInStock = Integer.parseInt(item.getString());
                            break;
                        case 4:
                            categoryID = item.getString().toLowerCase();//lowercase first letter of word (first letter uppercase because of the front-end)
                            break;
                        case 5:
                            description = item.getString();
                            break;
//                        case 6:
//                            //expirationDate = "1999-04-26 13:00:00";
//                            expirationDate = item.getString() + " 00:00:00";
//                            //System.out.println("expirationDate: " + expirationDate);
//                            break;
                    }
                    count++;
                    // ... (do your job here)
                } else {
                    // Process form file field (input type="file").
                    String fieldName = item.getFieldName();
                    String fileName = FilenameUtils.getName(item.getName());
                    InputStream fileContent = item.getInputStream();
                    // ... (DYJH)
                    //https://stackoverflow.com/questions/24452332/how-to-create-an-image-from-an-inputstream-resize-it-and-save-it
                    Image image = ImageIO.read(fileContent);
                    BufferedImage bi = this.createResizedCopy(image, 180, 180, true);
                    //
                    String applicationPath = getServletContext().getRealPath("");//this project path (until Lab231_4\build\web
                    String uploadPath = applicationPath + UPLOAD_DIR;//
                    File fileUploadDirectory = new File(uploadPath);
                    if (!fileUploadDirectory.exists()) {
                        fileUploadDirectory.mkdirs();
                    }
                    //
                    String savePath = uploadPath + File.separator + fileName;
                    String sRootPath = new File(savePath).getAbsolutePath();
                    File fileSaveDir1 = new File(savePath);
                    //dbFileName = UPLOAD_DIR + File.separator + fileName;
                    saveFileName = UPLOAD_DIR + "/" + fileName;
                    ImageIO.write(bi, "jpg", new File(savePath));
                }
            }
            String statusID = "active";
            createDate = "" + new Timestamp(System.currentTimeMillis());
            String productID = generateProductID(createDate);
            ProductDTO proDto = new ProductDTO(productID, productName, price, quantityInStock, createDate, saveFileName, description, categoryID, statusID, quantityInStock);
            ProductDAO productDao = new ProductDAO();
            boolean checkInsert = productDao.insertProduct(proDto);
            if (checkInsert) {
                url = SUCCESS;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    BufferedImage createResizedCopy(Image originalImage, int scaledWidth, int scaledHeight, boolean preserveAlpha) {
        //https://stackoverflow.com/questions/24452332/how-to-create-an-image-from-an-inputstream-resize-it-and-save-it
        int imageType = preserveAlpha ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
        BufferedImage scaledBI = new BufferedImage(scaledWidth, scaledHeight, imageType);
        Graphics2D g = scaledBI.createGraphics();
        if (preserveAlpha) {
            g.setComposite(AlphaComposite.Src);
        }
        g.drawImage(originalImage, 0, 0, scaledWidth, scaledHeight, null);
        g.dispose();
        return scaledBI;
    }

    String generateProductID(String createDate) {
        String result = null;
        //https://www.tutorialspoint.com/generate-random-bytes-in-java
        Random rd = new Random();
        byte[] arr = new byte[7];
        rd.nextBytes(arr);
        MyTools mt = new MyTools();
        try {
            result = mt.toHexString(mt.getSHA(Arrays.toString(arr) + createDate));
        } catch (NoSuchAlgorithmException e) {
        }
        return result;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
