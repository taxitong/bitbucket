/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.cons;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nhatlq.daos.OrderDAO;
import nhatlq.dtos.AccountDTO;
import nhatlq.dtos.OrderDTO;

/**
 *
 * @author Admin
 */
public class TrackingCon extends HttpServlet {

    private final static String ERROR = "error.jsp";
    private final static String SUCCESS = "tracking.jsp";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            HttpSession session = request.getSession();
            AccountDTO accDto = (AccountDTO)session.getAttribute("LOGIN_USER");//get my account id

            OrderDAO orderDao = new OrderDAO();
            int accID = accDto.getAccountID();
            String dateRange = request.getParameter("txtDateRange");
            String receiverName = request.getParameter("txtReceiverName");
            int index = Integer.parseInt(request.getParameter("index"));
            
            int count = orderDao.countOrder(accID, dateRange, receiverName);
            int size = 6; //each page has n=6 order;
            int endPage = count / size;
            if (count % size != 0) {
                endPage++;
            }
            List<OrderDTO> list = orderDao.searchOrder(accID, index, dateRange, receiverName);
            List<String> listDateRange = orderDao.getAllDateOrder(accDto.getAccountID());//get date for search funct in tracking
            request.setAttribute("endPage", endPage);
            request.setAttribute("LIST_ORDER", list);
            request.setAttribute("index", index);
            
            session.setAttribute("LIST_ALL_DATE_ORDER", listDateRange);
            request.setAttribute("txtDateRange", dateRange);
            request.setAttribute("txtReceiverName", receiverName);
            url = SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
