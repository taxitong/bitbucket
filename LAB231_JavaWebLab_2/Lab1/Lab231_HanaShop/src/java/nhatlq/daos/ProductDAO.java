/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import nhatlq.dtos.ProductDTO;
import nhatlq.utils.DBConnection;

/**
 *
 * @author Admin
 */
public class ProductDAO {

    Connection conn;
    PreparedStatement preStm;
    ResultSet rs;

    public ProductDAO() {
        conn = null;
        preStm = null;
        rs = null;
    }

    private void closeConnection2() throws SQLException {
        if (conn != null) {
            conn.close();
        }
        if (preStm != null) {
            preStm.close();
        }
    }

    private void closeConnection3() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (preStm != null) {
            preStm.close();
        }
        if (conn != null) {
            conn.close();
        }
    }

    //generate
    String generateSQL_SearchProduct_Admin(String categoryID, int size) {
        String productAttributes = "productID, productName, price, quantityInStock, createDate, image, description, categoryID, statusID ";
        String condition1 = "productName LIKE ? ";
        String condition2 = "price >= ? ";
        String condition3 = "price <= ? ";
        String condition4 = "categoryID = ? ";
        String condition5 = "quantityInStock > 0 ";
        if (categoryID.isEmpty()) {//If categoryID "", change sql to search like
            condition4 = "categoryID LIKE ? ";
        }
        String WB = "WHERE r BETWEEN ?*x-(x-1) AND ?*x";
        String WHERE_BETWEEN = WB.replace("x", size + "");
        String sql = "SELECT " + productAttributes
                + "FROM " + "(SELECT ROW_NUMBER() OVER (ORDER BY createDate DESC) AS r, "
                + productAttributes + "FROM tblProduct WHERE "
                + condition1 + "AND " + condition2 + "AND " + condition3 + "AND "
                + condition4 + "AND " + condition5
                + ") as x " + WHERE_BETWEEN;
        //System.out.println(sql);
        return sql;
    }

    public List<ProductDTO> searchProduct_Admin(String search, float min, float max, String categoryID, int index, int size) throws SQLException {
        List<ProductDTO> list = null;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = generateSQL_SearchProduct_Admin(categoryID, size);
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, "%" + search + "%");
                preStm.setFloat(2, min);
                preStm.setFloat(3, max);
                preStm.setString(4, categoryID);
                if (categoryID.isEmpty()) {
                    preStm.setString(4, "%" + categoryID + "%");
                }
                preStm.setInt(5, index);
                preStm.setInt(6, index);
                rs = preStm.executeQuery();
                list = new ArrayList<>();
                while (rs.next()) {
                    String productID = rs.getString("productID");
                    String productName = rs.getString("productName");
                    float price = rs.getFloat("price");
                    int quantityInStock = rs.getInt("quantityInStock");
                    String createDate = rs.getString("createDate");
                    String image = rs.getString("image");
                    String description = rs.getString("description");
                    String categoryIDTmp = rs.getString("categoryID");
                    String statusID = rs.getString("statusID");
                    list.add(new ProductDTO(productID, productName, price, quantityInStock, createDate, image, description, categoryIDTmp, statusID));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection3();
        }
        return list;
    }

    //generate
    String generateSQL_SearchProduct_User(String categoryID, int size) {
        String productAttributes = "productID, productName, price, quantityInStock, createDate, image, description, categoryID ";
        String condition1 = "productName LIKE ? ";
        String condition2 = "price >= ? ";
        String condition3 = "price <= ? ";
        String condition4 = "categoryID = ? ";
        String condition5 = "quantityInStock > 0 ";
        String condition6 = "statusID = ? ";
        if (categoryID.isEmpty()) {//If categoryID "", change sql to search like
            condition4 = "categoryID LIKE ? ";
        }
        String WB = "WHERE r BETWEEN ?*x-(x-1) AND ?*x";
        String WHERE_BETWEEN = WB.replace("x", size + "");
        String sql = "SELECT " + productAttributes
                + "FROM " + "(SELECT ROW_NUMBER() OVER (ORDER BY createDate DESC) AS r, "
                + productAttributes + "FROM tblProduct WHERE "
                + condition1 + "AND " + condition2 + "AND " + condition3 + "AND "
                + condition4 + "AND " + condition5 + "AND " + condition6
                + ") as x " + WHERE_BETWEEN;
        //System.out.println(sql);
        return sql;
    }

    public List<ProductDTO> searchProduct_User(String search, float min, float max, String categoryID, int index, int size) throws SQLException {
        List<ProductDTO> list = null;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = generateSQL_SearchProduct_User(categoryID, size);
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, "%" + search + "%");
                preStm.setFloat(2, min);
                preStm.setFloat(3, max);
                preStm.setString(4, categoryID);
                if (categoryID.isEmpty()) {//If categoryID "", change sql to search like
                    preStm.setString(4, "%" + categoryID + "%");
                }
                preStm.setString(5, "active");
                preStm.setInt(6, index);
                preStm.setInt(7, index);
                rs = preStm.executeQuery();
                list = new ArrayList<>();
                while (rs.next()) {
                    String productID = rs.getString("productID");
                    String productName = rs.getString("productName");
                    float price = rs.getFloat("price");
                    int quantityInStock = rs.getInt("quantityInStock");
                    String createDate = rs.getString("createDate");
                    String image = rs.getString("image");
                    String description = rs.getString("description");
                    String categoryIDTmp = rs.getString("categoryID");
                    list.add(new ProductDTO(productID, productName, price, quantityInStock, createDate, image, description, categoryIDTmp, "active"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection3();
        }
        return list;
    }

    //generate
    String generateSQL_CountAllProduct_Admin(String categoryID) {
        String condition1 = "productName LIKE ? ";
        String condition2 = "price >= ? ";
        String condition3 = "price <= ? ";
        String condition4 = "categoryID = ? ";
        String condition5 = "quantityInStock > 0 ";
        if (categoryID.isEmpty()) {//If categoryID "", change sql to search like
            condition4 = "categoryID LIKE ? ";
        }
        String sql = "SELECT count(productID) FROM tblProduct WHERE "
                + condition1 + "AND " + condition2 + "AND " + condition3 + "AND "
                + condition4 + "AND " + condition5;
        //System.out.println(sql);
        return sql;
    }

    public int countAllProduct_Admin(String search, float minNum, float maxNum, String categoryID) throws SQLException {
        int result = 0;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = generateSQL_CountAllProduct_Admin(categoryID);
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, "%" + search + "%");
                preStm.setFloat(2, minNum);
                preStm.setFloat(3, maxNum);
                preStm.setString(4, categoryID);
                if (categoryID.isEmpty()) {//If categoryID "", change sql to search like
                    preStm.setString(4, "%" + categoryID + "%");
                }
                rs = preStm.executeQuery();
                if (rs.next()) {
                    result = rs.getInt(1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection3();
        }
        return result;
    }

    //generate
    String generateSQL_CountAllProduct_User(String categoryID) {
        String condition1 = "productName LIKE ? ";
        String condition2 = "price >= ? ";
        String condition3 = "price <= ? ";
        String condition4 = "categoryID = ? ";
        String condition5 = "quantityInStock > 0 ";
        String condition6 = "statusID = ? ";
        if (categoryID.isEmpty()) {//If categoryID "", change sql to search like
            condition4 = "categoryID LIKE ? ";
        }
        String sql = "SELECT count(productID) FROM tblProduct WHERE "
                + condition1 + "AND " + condition2 + "AND " + condition3 + "AND "
                + condition4 + "AND " + condition5 + "AND " + condition6;
        //System.out.println(sql);
        return sql;
    }

    public int countAllProduct_User(String search, float minNum, float maxNum, String categoryID) throws SQLException {
        int result = 0;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = generateSQL_CountAllProduct_User(categoryID);
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, "%" + search + "%");
                preStm.setFloat(2, minNum);
                preStm.setFloat(3, maxNum);
                preStm.setString(4, categoryID);
                if (categoryID.isEmpty()) {//If categoryID "", change sql to search like
                    preStm.setString(4, "%" + categoryID + "%");
                }
                preStm.setString(5, "active");
                rs = preStm.executeQuery();
                if (rs.next()) {
                    result = rs.getInt(1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection3();
        }
        return result;
    }

    public float getMaxPrice() throws SQLException {
        float result = 0;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "SELECT MAX(price) from tblProduct";
                preStm = conn.prepareStatement(sql);
                rs = preStm.executeQuery();
                if (rs.next()) {
                    result = rs.getFloat(1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection3();
        }
        return result;
    }

    public int getQuantiyInStock(String search) throws SQLException {
        int result = -1;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "SELECT quantityInStock FROM tblProduct WHERE productID = ?";
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, search);
                rs = preStm.executeQuery();
                if (rs.next()) {
                    int productID = rs.getInt("quantityInStock");
                    result = productID;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection3();
        }
        return result;
    }

    public boolean updateAvailableProduct_WithImage(ProductDTO productDto) throws SQLException {
        boolean result = false;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "UPDATE tblProduct SET "
                        + "productName=?, price=?, quantityInStock=?, image=?, "
                        + "description=?, categoryID=?, statusID=? WHERE productID=?";
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, productDto.getProductName());
                preStm.setFloat(2, productDto.getPrice());
                preStm.setInt(3, productDto.getQuantityInStock());
                preStm.setString(4, productDto.getImage());
                preStm.setString(5, productDto.getDescription());
                preStm.setString(6, productDto.getCategoryID());
                preStm.setString(7, productDto.getStatusID());
                preStm.setString(8, productDto.getProductID());
                result = preStm.executeUpdate() > 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection2();
        }
        return result;
    }

    public boolean updateAvailableProduct_WithoutImage(ProductDTO productDto) throws SQLException {
        boolean result = false;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "UPDATE tblProduct SET "
                        + "productName=?, price=?, quantityInStock=?, "
                        + "description=?, categoryID=?, statusID=? WHERE productID=?";
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, productDto.getProductName());
                preStm.setFloat(2, productDto.getPrice());
                preStm.setInt(3, productDto.getQuantityInStock());
                preStm.setString(4, productDto.getDescription());
                preStm.setString(5, productDto.getCategoryID());
                preStm.setString(6, productDto.getStatusID());
                preStm.setString(7, productDto.getProductID());
                result = preStm.executeUpdate() > 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection2();
        }
        return result;
    }

    public boolean updateProduct(String productID, int newQuantityInStock) throws SQLException {
        boolean result = false;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "UPDATE tblProduct SET quantityInStock=? WHERE productID=?";
                preStm = conn.prepareStatement(sql);
                preStm.setInt(1, newQuantityInStock);
                preStm.setString(2, productID);
                result = preStm.executeUpdate() > 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection2();
        }
        return result;
    }

    public boolean insertProduct(ProductDTO product) throws SQLException {
        boolean result = false;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "INSERT INTO tblProduct(productID, productName, price, quantityInStock, categoryID, createDate, image, statusID, description) VALUES(?,?,?,?,?,?,?,?,?)";
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, product.getProductID());
                preStm.setString(2, product.getProductName());
                preStm.setFloat(3, product.getPrice());
                preStm.setInt(4, product.getQuantityInStock());
                preStm.setString(5, product.getCategoryID());
                preStm.setString(6, product.getCreateDate());
                String s = product.getImage();
                preStm.setString(7, s);
                preStm.setString(8, product.getStatusID());
                preStm.setString(9, product.getDescription());
                result = preStm.executeUpdate() > 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection2();
        }
        return result;
    }

    public boolean deleteProductAsUpdate(String productID) throws SQLException {
        boolean result = false;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "UPDATE tblProduct SET statusID=? WHERE productID=?";
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, "suspend");
                preStm.setString(2, productID);
                result = preStm.executeUpdate() > 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection2();
        }
        return result;
    }

    //generate
    String generateSQL_GetProduct() {
        String productAttributes = "productName, price, quantityInStock, createDate, image, description, categoryID, statusID ";
        String condition1 = "productID = ? ";

        String sql = "SELECT " + productAttributes
                + "FROM tblProduct WHERE " + condition1;
        return sql;
    }

    public ProductDTO getProduct(String productID) throws SQLException {
        ProductDTO result = null;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = generateSQL_GetProduct();
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, productID);
                rs = preStm.executeQuery();
                if (rs.next()) {
                    String productName = rs.getString("productName");
                    float price = rs.getFloat("price");
                    int quantityInStock = rs.getInt("quantityInStock");
                    String createDate = rs.getString("createDate");
                    String image = rs.getString("image");
                    String description = rs.getString("description");
                    String categoryIDTmp = rs.getString("categoryID");
                    String statusID = rs.getString("statusID");
                    result = new ProductDTO(productID, productName, price, quantityInStock, createDate, image, description, categoryIDTmp, statusID);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection3();
        }
        return result;
    }
}
