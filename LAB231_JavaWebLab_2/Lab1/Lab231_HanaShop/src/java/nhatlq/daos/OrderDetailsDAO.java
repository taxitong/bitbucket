/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import nhatlq.dtos.OrderDetailsDTO;
import nhatlq.utils.DBConnection;

/**
 *
 * @author Admin
 */
public class OrderDetailsDAO {

    private Connection conn;
    private PreparedStatement preStm;
    private ResultSet rs;

    public OrderDetailsDAO() {
        conn = null;
        preStm = null;
        rs = null;
    }

//    private void closeConnection3() throws SQLException {
//        if (conn != null) {
//            conn.close();
//        }
//        if (preStm != null) {
//            preStm.close();
//        }
//        if (rs != null) {
//            rs.close();
//        }
//    }

    private void closeConnection2() throws SQLException {
        if (conn != null) {
            conn.close();
        }
        if (preStm != null) {
            preStm.close();
        }
    }

    public void insertOrderDetails(OrderDetailsDTO orderDetails) throws SQLException {
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "INSERT INTO tblOrderDetails(orderID, productID, price, quantity) VALUES(?,?,?,?)";
                preStm = conn.prepareStatement(sql);
                preStm.setInt(1, orderDetails.getOrderID());
                preStm.setString(2, orderDetails.getProductID());
                preStm.setFloat(3, orderDetails.getPrice());
                preStm.setInt(4, orderDetails.getQuantity());
                preStm.executeUpdate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection2();
        }
    }

//    public int countAllOrderDetails(int orderID, int index) throws SQLException {
//        int result = 0;
//        try {
//            conn = DBConnection.getConnection();
//            if (conn != null) {
//                String sql = "SELECT count(orderDetailsID) FROM tblOrderDetails "
//                        + "WHERE orderID = ?";
//                preStm = conn.prepareStatement(sql);
//                preStm.setInt(1, orderID);
//                rs = preStm.executeQuery();
//                if (rs.next()) {
//                    result = rs.getInt(1);
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            closeConnection3();;
//        }
//        return result;
//    }
}
