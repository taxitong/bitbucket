/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import nhatlq.dtos.UpdateRecorderDTO;
import nhatlq.utils.DBConnection;

/**
 *
 * @author Admin
 */
public class UpdateRecorderDAO {

    Connection conn;
    PreparedStatement preStm;
    ResultSet rs;

    public UpdateRecorderDAO() {
        conn = null;
        preStm = null;
        rs = null;
    }

    private void closeConnection2() throws SQLException {
        if (conn != null) {
            conn.close();
        }
        if (preStm != null) {
            preStm.close();
        }
    }

    public boolean inserUpdateRecorder(UpdateRecorderDTO updateRecorder) throws SQLException {
        boolean result = false;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "INSERT INTO tblUpdateRecorder(updateRecorderID, productID, accountID, dateUpdate) VALUES(?,?,?,?)";
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, updateRecorder.getUpdateRecorderID());
                preStm.setString(2, updateRecorder.getProductID());
                preStm.setInt(3, updateRecorder.getAccountID());
                preStm.setString(4, updateRecorder.getDateUpdate());
                result = preStm.executeUpdate() > 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection2();
        }
        return result;
    }
}
