/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import nhatlq.dtos.OrderDTO;
import nhatlq.utils.DBConnection;

/**
 *
 * @author Admin
 */
public class OrderDAO {

    private Connection conn;
    private PreparedStatement preStm;
    private ResultSet rs;

    public OrderDAO() {
        conn = null;
        preStm = null;
        rs = null;
    }

    private void closeConnection3() throws SQLException {
        if (conn != null) {
            conn.close();
        }
        if (preStm != null) {
            preStm.close();
        }
        if (rs != null) {
            rs.close();
        }
    }

    private void closeConnection2() throws SQLException {
        if (conn != null) {
            conn.close();
        }
        if (preStm != null) {
            preStm.close();
        }
    }

    public int insertOder(OrderDTO order) throws SQLException {
        //https://o7planning.org/en/11487/get-the-values-of-the-columns-automatically-increment-when-insert-a-record-using-jdbc
        int result = -1;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "INSERT INTO tblOrder(total, dateCreate, accountID, username_tblOrder, phone_tblOrder, address_tblOrder) VALUES(?,?,?,?,?,?)";
                preStm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                preStm.setFloat(1, order.getTotal());
//                float tmp = order.getTotal();
                preStm.setString(2, order.getDateCreate());
                preStm.setInt(3, order.getAccountID());
                preStm.setString(4, order.getUsername_tblOrder());
                preStm.setString(5, order.getPhone_tblOrder());
                preStm.setString(6, order.getAddress_tblOrder());
                preStm.executeUpdate();
                rs = preStm.getGeneratedKeys();
                if (rs.next()) {
                    result = rs.getInt(1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection2();
        }
        return result;
    }

    public boolean deleteOder(int orderID) throws SQLException {
        boolean result = false;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "DELETE FROM tblOrder WHERE orderID = ?";
                preStm = conn.prepareStatement(sql);
                preStm.setInt(1, orderID);
                result = preStm.executeUpdate() > 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection2();
        }
        return result;
    }

    public int countOrder(int accountID, String dateCreate, String receiverName) throws SQLException {
        int result = 0;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "SELECT count(orderID) FROM tblOrder "
                        + "WHERE accountID = ? AND dateCreate LIKE ? AND username_tblOrder LIKE ?";
                preStm = conn.prepareStatement(sql);
                preStm.setInt(1, accountID);
                preStm.setString(2, "%" + dateCreate + "%");
                preStm.setString(3, "%" + receiverName + "%");
                rs = preStm.executeQuery();
                if (rs.next()) {
                    result = rs.getInt(1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection3();
        }
        return result;
    }

    public List<OrderDTO> searchOrder(int accountID, int index, String dateCreate, String receiverName) throws SQLException {
        List<OrderDTO> result = new ArrayList<>();
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "SELECT orderID, total, dateCreate, accountID, username_tblOrder, phone_tblOrder, address_tblOrder FROM\n"
                        + "(SELECT ROW_NUMBER() OVER (ORDER BY dateCreate DESC) AS r,  orderID, total, dateCreate, accountID, username_tblOrder, phone_tblOrder, address_tblOrder \n"
                        + "FROM tblOrder WHERE accountID = ? AND dateCreate LIKE ? AND username_tblOrder LIKE ?) as x\n"
                        + "WHERE r BETWEEN ?*6-5 AND ?*6";
                preStm = conn.prepareStatement(sql);
                preStm.setInt(1, accountID);
                preStm.setString(2, "%" + dateCreate + "%");
                preStm.setString(3, "%" + receiverName + "%");
                preStm.setInt(4, index);
                preStm.setInt(5, index);
                rs = preStm.executeQuery();
                while (rs.next()) {
                    int orderID = rs.getInt("orderID");
                    float total = rs.getFloat("total");
                    String dateCreateTmp = rs.getString("dateCreate");

                    //int accountID = rs.getInt("accountID");
                    String username_tblOrder = rs.getString("username_tblOrder");
                    String phone_tblOrder = rs.getString("phone_tblOrder");
                    String address_tblOrder = rs.getString("address_tblOrder");
                    result.add(new OrderDTO(orderID, total, dateCreateTmp, accountID, username_tblOrder, phone_tblOrder, address_tblOrder));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
//            logger.error("Error: " + e.getMessage() + e.getClass().getSimpleName());
        } finally {
            closeConnection3();
        }
        return result;
    }

    public List<String> getAllDateOrder(int accountID) throws SQLException {
        List<String> result = new ArrayList<>();
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "SELECT dateCreate FROM tblOrder WHERE accountID = ? GROUP BY dateCreate";
                preStm = conn.prepareStatement(sql);
                preStm.setInt(1, accountID);
                rs = preStm.executeQuery();
                while (rs.next()) {
                    String tmp = rs.getString("dateCreate");
                    result.add(tmp);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
//            logger.error("Error: " + e.getMessage() + e.getClass().getSimpleName());
        } finally {
            closeConnection3();
        }
        return result;
    }
}
