/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import nhatlq.dtos.AccountDTO;
import nhatlq.utils.DBConnection;
import nhatlq.utils.MyTools;

/**
 *
 * @author Admin
 */
public class AccountDAO {

    private Connection conn;
    private PreparedStatement preStm;
    private ResultSet rs;

    public AccountDAO() {
        conn = null;
        preStm = null;
        rs = null;
    }

    private void closeConnection() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (preStm != null) {
            preStm.close();
        }
        if (conn != null) {
            conn.close();
        }
    }

    public AccountDTO checkLogin(String username, String password) throws SQLException {
        AccountDTO accountDto = null;
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "SELECT accountID, displayName, phoneNo, address, roleID, statusID "
                        + "FROM tblAccount WHERE username=? AND password=? AND statusID=?";
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, username);
                MyTools mt = new MyTools();
                String newPassword = mt.toHexString(mt.getSHA(password));
                preStm.setString(2, newPassword);
                preStm.setString(3, "active");
                rs = preStm.executeQuery();
                if (rs.next()) {
                    int accountID = rs.getInt("accountID");
                    //String usernameTmp = rs.getString("username");//We don't need this because in order to execute the = query, we already have to know its value in advance
                    String displayName = rs.getString("displayName");
                    String phoneNo = rs.getString("phoneNo");
                    String address = rs.getString("address");
                    String roleID = rs.getString("roleID");
                    String statusID = "active";
                    accountDto = new AccountDTO(accountID, username, displayName, phoneNo, address, roleID, statusID);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return accountDto;
    }
}
