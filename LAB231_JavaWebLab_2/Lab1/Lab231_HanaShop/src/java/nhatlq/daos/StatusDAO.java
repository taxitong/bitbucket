/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhatlq.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import nhatlq.dtos.StatusDTO;
import nhatlq.utils.DBConnection;

/**
 *
 * @author Admin
 */
public class StatusDAO {
    
    private Connection conn;
    private PreparedStatement preStm;
    private ResultSet rs;

    public StatusDAO() {
        conn = null;
        preStm = null;
        rs = null;
    }

    private void closeConnection() throws SQLException {
        if (conn != null) {
            conn.close();
        }
        if (preStm != null) {
            preStm.close();
        }
        if (rs != null) {
            rs.close();
        }
    }

    public List<StatusDTO> getAllStatus() throws SQLException {
        List<StatusDTO> result = new ArrayList<>();
        try {
            conn = DBConnection.getConnection();
            if (conn != null) {
                String sql = "SELECT statusID, statusName "
                        + "FROM tblStatus WHERE statusID LIKE '%%'";
                preStm = conn.prepareStatement(sql);
                rs = preStm.executeQuery();
                while (rs.next()) {
                    String statusID = rs.getString("statusID");
                    String statusName = rs.getString("statusName");
                    result.add(new StatusDTO(statusID, statusName));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
           closeConnection();
        }
        return result;
    }
}
