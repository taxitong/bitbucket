<%-- 
    Document   : mainPageUs
    Created on : Jan 13, 2021, 9:11:26 PM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="sharedJSP/cssAndBackgroundImg.jsp"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Main Us</title>
    </head>
    <body>
        <!--1Redirect if not Us-->
        <c:if test="${sessionScope.LOGIN_ADMIN != null}">
            <c:redirect url="mainAd.jsp"/>
        </c:if>
        <c:if test="${sessionScope.LOGIN_USER == null}">
            <c:redirect url="mainUn.jsp"/>
        </c:if>
        <!--0Redirect if not Us-->

        <div class="container">
            <jsp:include page="sharedJSP/header.jsp"/>
            <jsp:include page="sharedJSP/menuBarUs.jsp"/>
            <div class="main">
                <div class="left">
                    <jsp:include page="sharedJSP/welcomeToOurWebsite.jsp"/>
                </div>
                <div class="right">
                    <jsp:include page="sharedJSP/rightUs.jsp"/>
                </div>
            </div>
            <jsp:include page="sharedJSP/footer.jsp"/>
        </div>
    </body>
</html>
