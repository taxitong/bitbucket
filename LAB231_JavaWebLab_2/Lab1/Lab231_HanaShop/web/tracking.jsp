<%-- 
    Document   : trackingPage
    Created on : Jan 20, 2021, 4:36:06 AM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="sharedJSP/cssAndBackgroundImg.jsp"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tracking Page</title>
    </head>
    <body>
        <!--1Redirect if not Us-->
        <c:if test="${sessionScope.LOGIN_ADMIN != null}">
            <c:redirect url="mainAd.jsp"/>
        </c:if>
        <c:if test="${sessionScope.LOGIN_USER == null}">
            <c:redirect url="mainUn.jsp"/>
        </c:if>
        <!--0Redirect if not Us-->
        <div class="container">
            <jsp:include page="sharedJSP/header.jsp"/>
            <jsp:include page="sharedJSP/menuBarUs.jsp"/>
            <div class="main">
                <div class="left">
                    <div class="title">
                        All your order(s): 
                    </div>
                    <form action="MainCon">
                        Search: Date
                        <!--<select name="txtDateRange" class="inputTxt" name="dateCategory" id="dateCategory">-->
                        <select name="txtDateRange" class="inputTxt" id="dateCategory">
                            <!--style="text-transform: capitalize;" https://stackoverflow.com/questions/5577364/make-the-first-character-uppercase-in-css-->
                            <option value="">All</option>
                            <c:forEach var="dateRange" varStatus="counter" items="${sessionScope.LIST_ALL_DATE_ORDER}">
                                <c:if test="${requestScope.txtDateRange == dateRange}">
                                    <option value="${dateRange}" selected="true">${dateRange}</option>
                                </c:if>
                                <c:if test="${requestScope.txtDateRange != dateRange}">
                                    <option value="${dateRange}">${dateRange}</option>
                                </c:if>
                                
                                
                                <!--<option value="${dateRange}">${dateRange}</option>-->
                            </c:forEach>
                        </select>
                        Receiver: 
                        <input class="inputTxt" type="text" name="txtReceiverName" value="${txtReceiverName}"/>
                        <input style="margin-left: 12px;" class="submitTxt" type="submit" name="btnAction" value="Search Order" />
                        <c:if test="${requestScope.LIST_ORDER != null}">
                            <c:if test="${not empty requestScope.LIST_ORDER}">
                                <table border="1">
                                    <thead>
                                        <tr>
                                            <th>Total</th>
                                            <th>Date Create</th>
                                            <th>Receiver</th>
                                            <th>Phone</th>
                                            <th>Address</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="tmp" varStatus="counter" items="${requestScope.LIST_ORDER}">
                                            <tr>
                                                <td>${tmp.total}$</td>
                                                <td>${tmp.dateCreate}</td>
                                                <td>${tmp.username_tblOrder}</td>
                                                <td>${tmp.phone_tblOrder}</td>
                                                <td>${tmp.address_tblOrder}</td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table></br>
                                <div class="paging">
                                    <c:forEach begin="1" end="${endPage}" var="i">
                                        <a id="${i}" href="TrackingCon?index=${i}&txtDateRange=${requestScope.txtDateRange}&txtReceiverName=${requestScope.txtReceiverName}">${i}</a>
                                    </c:forEach>
                                    <script>
                                        document.getElementById('${index}').style.color = "red";
                                    </script>
                                </div>
                            </c:if>
                        </c:if>
                        <c:if test="${empty requestScope.LIST_ORDER}">
                            </br></br>
                            No suitable order found</br></br>
                        </c:if>
                    </form> 
                </div>
                <div class="right">
                    <jsp:include page="sharedJSP/rightUs.jsp"/>
                </div>
            </div>
            <jsp:include page="sharedJSP/footer.jsp"/>
        </div>
    </body>
</html>
