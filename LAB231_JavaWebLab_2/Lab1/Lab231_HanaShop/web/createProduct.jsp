<%-- 
    Document   : createProduct
    Created on : Jan 19, 2021, 9:29:56 AM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="sharedJSP/cssAndBackgroundImg.jsp"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Create Product</title>
    </head>
    <body>
        <!--1Redirect if not ad-->
        <c:if test="${sessionScope.LOGIN_USER != null}">
            <c:redirect url="mainUs.jsp"/>
        </c:if>
        <c:if test="${sessionScope.LOGIN_ADMIN == null}">
            <c:redirect url="mainUn.jsp"/>
        </c:if>
        <!--0Redirect if not ad-->
        <c:if test="${sessionScope.LOGIN_ADMIN == null}">
            <c:if test="${sessionScope.LOGIN_USER != null}">
                <c:redirect url="mainUs.jsp"/>
            </c:if>
            <c:if test="${sessionScope.LOGIN_USER == null}">
                <c:redirect url="mainUn.jsp"/>
            </c:if>
        </c:if>
        <div class="container">
            <jsp:include page="sharedJSP/header.jsp"/>
            <jsp:include page="sharedJSP/menuBarAd.jsp"/>
            <div class="main">
                <div class="left">
                    <div class="title">
                        New Product
                    </div>
                    <form action="CreateProductCon" method="post" enctype="multipart/form-data">
                        Product Name:
                        <input class="custom-text-cart" type="text" name="txtProductName_CreateProduct" maxlength="16" required="required"/></br>
                        Price: 
                        <input class="custom-text-cart" type="number" name="txtPrice_CreateProduct" min="0.01" max="99999.99" step="0.01" required="required"/></br>
                        Quantity In Stock: 
                        <input class="custom-text-cart" type="number" name="txtQuantityInStock_CreateProduct" min="1" max="99999" step="1" required="required"/></br></br>
                        Category ID:
                        <select name="txtCategoryID_CreateProduct" class="inputTxt" name="productCategory" id="productCategory" style="text-transform: capitalize;">
                            <!--style="text-transform: capitalize;" https://stackoverflow.com/questions/5577364/make-the-first-character-uppercase-in-css-->
                            <c:forEach var="category" varStatus="counter" items="${sessionScope.LIST_CATEGORY}">
                                <option value="${category.categoryName}">${category.categoryName}</option>
                            </c:forEach>
                        </select>
                        StatusID: Active (default)
                        </br></br></br>
                        Description: <input class="custom-text-cart" type="text" name="txtDescription_CreateProduct" maxlength="500" required="required"/></br>
                        Image: <input class="custom-text-cart" type="file" name="txtImage_CreateProduct" required="required"/></br>
                        <input class="now" type="submit" name="btnAction" value="Create Product"/>
                    </form>
                </div>
                <div class="right">
                    <jsp:include page="sharedJSP/rightAd.jsp"/>
                </div>
            </div>
            <jsp:include page="sharedJSP/footer.jsp"/>
        </div>
    </body>
</html>
