<%-- 
    Document   : error
    Created on : Jan 13, 2021, 9:12:30 PM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="sharedJSP/cssAndBackgroundImg.jsp"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Error</title>
    </head>
    <body>
        <div class="container">
            <jsp:include page="sharedJSP/header.jsp"/>
            <c:if test="${sessionScope.LOGIN_ADMIN != null}">
                <div class="menuBar"><a href="mainAd.jsp">Home</a></div>
            </c:if>
            <c:if test="${sessionScope.LOGIN_USER != null}">
                <div class="menuBar"><a href="mainUs.jsp">Home</a></div>
            </c:if>
            <c:if test="${sessionScope.LOGIN_ADMIN == null && sessionScope.LOGIN_USER == null}">
                <div class="menuBar"><a href="mainUn.jsp">Home</a></div>
            </c:if>
            <div class="main">
                <div class="left">
                    <div class="title"><h1>Something went wrong</h1></div>
                    <a href="mainPage.jsp" style="color: blue">Return Main Page</a>
                </div>
                <div class="right">
                    <div class="title_Right"><h3>Welcome</h3></div>
                    <div class="each">
                        <!--<div class="description">-->
                            With our easy to use online order form, you will be spoilt with many choices of freshly baked and smooth grind roast cakes!</br></br>
                            <!--More than just cakes. We help celebrate this special day - Mid-Autumn Festival with sweet treats from The Sai Gon Mooncake Shop.</br></br>-->
                            Not sure what to order? You can always call our friendly sales staff at Hotline: +65 6570 3099 for more information.</br></br>
                        <!--</div>-->
                    </div>
                </div>
            </div>
            <jsp:include page="sharedJSP/footer.jsp"/>
        </div>
    </body>
</html>
