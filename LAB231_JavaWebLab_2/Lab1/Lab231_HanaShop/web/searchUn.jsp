<%-- 
    Document   : searchUn
    Created on : Jan 16, 2021, 9:47:47 AM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="sharedJSP/cssAndBackgroundImg.jsp"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Search Un</title>
    </head>
    <body>
        <!--1Redirect if not Un-->
        <c:if test="${sessionScope.LOGIN_ADMIN != null}">
            <c:redirect url="mainAd.jsp"/>
        </c:if>
        <c:if test="${sessionScope.LOGIN_USER != null}">
            <c:redirect url="mainUs.jsp"/>
        </c:if>
        <!--0Redirect if not Un-->
        <div class="container">
            <jsp:include page="sharedJSP/header.jsp"/>
            <jsp:include page="sharedJSP/menuBarUn.jsp"/>
            <div class="main">
                <div class="left">
                    <jsp:include page="sharedJSP/left_Search.jsp"/>
                </div>
                <div class="right">
                    <jsp:include page="sharedJSP/rightUn.jsp"/>
                </div>
            </div>
            <jsp:include page="sharedJSP/footer.jsp"/>
        </div>
    </body>
</html>
