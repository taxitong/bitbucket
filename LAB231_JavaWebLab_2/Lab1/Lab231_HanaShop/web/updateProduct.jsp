<%-- 
    Document   : updateProduct
    Created on : Jan 19, 2021, 10:53:15 PM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="sharedJSP/cssAndBackgroundImg.jsp"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <!--1Redirect if not Ad-->
        <c:if test="${sessionScope.LOGIN_USER != null}">
            <c:redirect url="mainUs.jsp"/>
        </c:if>
        <c:if test="${sessionScope.LOGIN_ADMIN == null}">
            <c:redirect url="mainUn.jsp"/>
        </c:if>
        <!--0Redirect if not Ad-->
        <div class="container">
            <jsp:include page="sharedJSP/header.jsp"/>
            <jsp:include page="sharedJSP/menuBarAd.jsp"/>
            <div class="main">
                <div class="left">
                    <div class="title">
                        New Product
                    </div>
                    <form action="UpdateProductCon" method="post" enctype="multipart/form-data">
                        <!--class="custom-text-cart" here isn't right, please consider changing it later-->
                        Product Name:
                        <input class="custom-text-cart" type="text" name="txtProductName_UpdateProduct" maxlength="16" required="required" value="${sessionScope.PRODUCT_UPDATE.productName}"/></br>
                        Price: 
                        <input class="custom-text-cart" type="number" name="txtPrice_CreateProduct" min="0.01" max="99999.99" step="0.01" required="required" value="${sessionScope.PRODUCT_UPDATE.price}"/></br>
                        Quantity In Stock: 
                        <input class="custom-text-cart" type="number" name="txtQuantityInStock_CreateProduct" min="1" max="99999" step="1" value="1" required="required" value="${sessionScope.PRODUCT_UPDATE.quantityInStock}"/></br></br>
                        Category ID:
                        <select name="txtCategoryID_UpdateProduct" class="inputTxt" name="productCategory" id="productCategory" style="text-transform: capitalize;">
                            <c:forEach var="category" items="${sessionScope.LIST_CATEGORY}">
                                <c:if test="${sessionScope.PRODUCT_UPDATE.categoryID == category.categoryName}">
                                    <option value="${category.categoryName}" selected="true">${category.categoryName}</option>
                                </c:if>
                                <c:if test="${sessionScope.PRODUCT_UPDATE.categoryID != category.categoryName}">
                                    <option value="${category.categoryName}">${category.categoryName}</option>
                                </c:if>
                            </c:forEach>
                        </select>
                        Status ID: 
                        <select name="txtStatusID_UpdateProduct" class="inputTxt" name="productStatus" id="productStatus" style="text-transform: capitalize;">
                            <c:forEach var="status" items="${sessionScope.LIST_STATUS}">
                                <c:if test="${sessionScope.PRODUCT_UPDATE.statusID == status.statusID}">
                                    <option value="${status.statusID}" selected="true">${status.statusID}</option>
                                </c:if>
                                <c:if test="${sessionScope.PRODUCT_UPDATE.statusID != status.statusID}">
                                    <option value="${status.statusID}">${status.statusID}</option>
                                </c:if>
                            </c:forEach>
                        </select>
                        </br></br>
                        Description: <input class="custom-text-cart" type="text" name="txtDescription_UpdateProduct" maxlength="500" required="required"  value="${sessionScope.PRODUCT_UPDATE.description}"/></br>
                        Image (If you don't want to update the image, don't choose image):
                        <input class="custom-text-cart" type="file" name="txtImage_UpdateProduct"/></br>
                        <input type="hidden" name="txtProductID_UpdateProduct" value="${sessionScope.PRODUCT_UPDATE.productID}" />
                        <input type="submit" name="btnAction" value="Edit now"/>
                    </form>
                </div>
                <div class="right">
                    <jsp:include page="sharedJSP/rightAd.jsp"/>
                </div>
            </div>
            <jsp:include page="sharedJSP/footer.jsp"/>
        </div>
    </body>
</html>
