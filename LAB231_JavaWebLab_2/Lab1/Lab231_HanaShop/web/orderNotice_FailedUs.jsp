<%-- 
    Document   : orderNotice_FailedUs
    Created on : Jan 18, 2021, 3:55:33 AM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="sharedJSP/cssAndBackgroundImg.jsp"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Order Fail Us</title>
    </head>
    <body>
        <!--1Redirect if not Us-->
        <c:if test="${sessionScope.LOGIN_ADMIN != null}">
            <c:redirect url="mainAd.jsp"/>
        </c:if>
        <c:if test="${sessionScope.LOGIN_USER == null}">
            <c:redirect url="mainUn.jsp"/>
        </c:if>
        <!--0Redirect if not Us-->
        <div class="container">
            <jsp:include page="sharedJSP/header.jsp"/>
            <jsp:include page="sharedJSP/menuBarUs.jsp"/>
            <div class="main">
                <div class="left">
                    <div class="title">
                        ERROR</br></br>
                        1 or some items in your cart has been sold out.</br>
                    </div>
                </div>
                <div class="right">
                    <jsp:include page="sharedJSP/rightUs.jsp"/>
                </div>
            </div>
            <jsp:include page="sharedJSP/footer.jsp"/>
        </div>
    </body>
</html>