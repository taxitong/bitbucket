<%-- 
    Document   : paging
    Created on : Jan 17, 2021, 7:26:56 PM
    Author     : Admin
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div class="paging">
    <c:forEach begin="1" end="${endPage}" var="i">
        <a id="${i}" href="MainCon?btnAction=SearchProductCon&index=${i}&txtSearch=${txtSearch}&txtSearch_MinNum=${txtSearch_MinNum}&txtSearch_MaxNum=${txtSearch_MaxNum}&txtSearch_CategoryID=${txtSearch_CategoryID}">${i}</a>
    </c:forEach>
    <script>
        document.getElementById('${index}').style.color = "red";
    </script>
</div>