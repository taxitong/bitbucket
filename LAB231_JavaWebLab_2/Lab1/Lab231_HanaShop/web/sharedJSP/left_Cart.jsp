<%-- 
    Document   : left_Cart
    Created on : Jan 18, 2021, 7:24:42 PM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %><!--https://stackoverflow.com/questions/11094402/how-print-a-float-variable-up-to-two-decimal-places-in-jsp-->
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<c:if test="${sessionScope.CART != null}">
    <c:if test="${not empty sessionScope.CART}">
        <table border="1">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Buy</th>
                    <th>Stock</th>
                    <th>Total</th>
                    <th>Update</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                <c:set var="totalOrder" value="${0}"/>
                <c:forEach var="tmp" varStatus="counter" items="${sessionScope.CART.cart}">
                <form action="MainCon" method="POST">
                    <tr>
                        <td>${counter.count}</td>
                        <td>${tmp.value.productName}</td>
                        <td>${tmp.value.price}$</td>
                        <td>
                            <input type="number" id="quantity" name="txtQuantityToBuy" value="${tmp.value.quantityToBuy}" min="1" max="${tmp.value.quantityInStock}">
                        </td>
                        <td>${tmp.value.quantityInStock}</td>
                        <!--https://stackoverflow.com/questions/11094402/how-print-a-float-variable-up-to-two-decimal-places-in-jsp-->
                        <fmt:formatNumber var="rounding" type="number" maxFractionDigits="2" value="${tmp.value.quantityToBuy * tmp.value.price}"/>
                        <c:set var="totalOrderDetails" value="${rounding}"/>
                        <td>${totalOrderDetails}$</td>
                    <input type="hidden" name="txtProductID" value="${tmp.value.productID}"/>
                    <input type="hidden" name="txtProductName" value="${tmp.value.productName}"/>
                    <input type="hidden" name="txtPrice" value="${tmp.value.price}"/>
                    <input type="hidden" name="txtQuantityInStock" value="${tmp.value.quantityInStock}"/>
                    <input type="hidden" name="txtCategoryID" value="${tmp.value.categoryID}"/>
                    <input type="hidden" name="txtCreateDate" value="${tmp.value.createDate}"/>
                    <input type="hidden" name="txtImage" value="${tmp.value.image}"/>
                    <input type="hidden" name="txtStatusID" value="${tmp.value.statusID}"/>
                    <input type="hidden" name="txtDescription" value="${tmp.value.description}"/>
                    <!--<input type="hidden" name="txtQuantityToBuy" value="${tmp.value.quantityToBuy}"/>-->
                    <td>
                        <input type="submit" name="btnAction" value="Update"/>
                    </td>
                    <td>
                        <input type="submit" name="btnAction" value="Remove" onclick="return confirm('Are you sure you want to delete?')"/>
                    </td>
                    </tr>
                </form>
                <c:set var="totalOrder" value="${totalOrder + totalOrderDetails}" />
            </c:forEach>
        </tbody>
    </table>
    <form action="MainCon" method="POST">
        <p>(*) If there are changes to the product quantity after adding it to the shopping cart, please press the update button before buying</p></br>
        Shipping fee: 5$</br>
        <fmt:formatNumber var="roundingTotalOrder" type="number" maxFractionDigits="2" value="${totalOrder + 5}"/>
        Total: ${roundingTotalOrder}$</br></br><!--https://stackoverflow.com/questions/27274091/how-to-calculate-the-total-of-a-sum-in-jstl-->
        <input type="hidden" name="txtTotalOrder" value="${roundingTotalOrder}"/>
        Receiver name:
        <input class="custom-text-cart" type="text" name="txtName_CreateOrder" minlength="2" maxlength="100" required="required" value="${sessionScope.LOGIN_USER.displayName}"/></br>
        Phone: 
        <input class="custom-text-cart" type="text" name="txtPhoneNo_CreateOrder" minlength="10" maxlength="11" required="required" value="${sessionScope.LOGIN_USER.phoneNo}"/></br>
        Delivery address: 
        <input class="custom-text-cart" type="text" name="txtAddress_CreateOrder" minlength="2" maxlength="100" required="required" value="${sessionScope.LOGIN_USER.address}"/></br>
        </br>
        Payment menthod: Cash on delivery (COD)</br>
        <input class="now" type="submit" name="btnAction" value="Buy now">
    </form>
</c:if>
</c:if>
<c:if test="${sessionScope.CART == null}">
    You have not selected any products yet</br></br>
</c:if>
