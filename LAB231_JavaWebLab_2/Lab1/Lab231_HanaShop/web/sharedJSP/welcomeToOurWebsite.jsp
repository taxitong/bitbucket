<%-- 
    Document   : welcomeToOurWebsite
    Created on : Jan 17, 2021, 9:19:44 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div class="title">
    Welcome to our website 
</div>
<div class="each">
    <div class="image">
        <img src="images/i1.jpg" style="width: 232px; height: 289px"alt=""/>
    </div>
    <div class="description">
        With our easy to use online order form, you will be spoilt with many choices of freshly baked and smooth grind roast cakes!</br></br>
        Press Search to start searching.</br></br>
        Not sure what to order? You can always call our friendly sales staff at Hotline: +65 6570 3099 for more information.</br></br>
    </div>
</div>
