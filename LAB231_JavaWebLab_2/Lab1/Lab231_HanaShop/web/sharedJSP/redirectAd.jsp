<%-- 
    Document   : redirectAd
    Created on : Jan 21, 2021, 10:19:12 PM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!--1Redirect if not Ad-->
<c:if test="${sessionScope.LOGIN_USER != null}">
    <c:redirect url="mainUs.jsp"/>
</c:if>
<c:if test="${sessionScope.LOGIN_ADMIN == null}">
    <c:redirect url="mainUn.jsp"/>
</c:if>
<!--0Redirect if not Ad-->

