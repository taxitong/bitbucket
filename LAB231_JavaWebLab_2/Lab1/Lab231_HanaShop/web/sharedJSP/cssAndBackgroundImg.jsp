<%-- 
    Document   : cssAndBackgroundImg
    Created on : Jan 17, 2021, 9:21:40 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<style>
    body{
        background-image: url('images/bg-01.jpg');
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-size: 100% 100%;
        /*https://www.w3schools.com/html/html_images_background.asp*/
    }
</style>
<link href="css/main.css" rel="stylesheet" type="text/css"/>
<link href="css/style_HanaShop.css" rel="stylesheet" type="text/css"/>
<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>