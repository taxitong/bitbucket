<%-- 
    Document   : descprition_ProductDetails
    Created on : Jan 18, 2021, 6:59:04 PM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div class="title">
    ${sessionScope.PRODUCT_DETAILS.productName}
</div>
<div class="each">
    <div class="image">
        <img style="width: 230px; height: 200px;" src=${sessionScope.PRODUCT_DETAILS.image} alt=""/>
    </div>
    <div class="description">
        ${sessionScope.PRODUCT_DETAILS.description}</br></br>
        <div style="color: #d50000;">
            Price: ${sessionScope.PRODUCT_DETAILS.price} $</br>
        </div>
        <c:if test="${sessionScope.LOGIN_USER != null}">
            <form action="MainCon" method="POST">
                Quantity: <input type="number" id="quantity" name="txtQuantityToBuy" value="1" min="1" max="${sessionScope.PRODUCT_DETAILS.quantityInStock}"></br></br>
                (Shop's available: ${sessionScope.PRODUCT_DETAILS.quantityInStock})</br> 
                <input type="hidden" name="txtProductID" value="${sessionScope.PRODUCT_DETAILS.productID}"/>
                <input type="hidden" name="txtProductName" value="${sessionScope.PRODUCT_DETAILS.productName}"/>
                <input type="hidden" name="txtPrice" value="${sessionScope.PRODUCT_DETAILS.price}"/>
                <input type="hidden" name="txtQuantityInStock" value="${sessionScope.PRODUCT_DETAILS.quantityInStock}"/>
                <input type="hidden" name="txtCreateDate" value="${sessionScope.PRODUCT_DETAILS.createDate}"/>
                <input type="hidden" name="txtImage" value="${sessionScope.PRODUCT_DETAILS.image}"/>
                <input type="hidden" name="txtDescription" value="${sessionScope.PRODUCT_DETAILS.description}"/>

                <input type="hidden" name="txtCategoryID" value="${sessionScope.PRODUCT_DETAILS.categoryID}"/>
                <input type="hidden" name="txtStatusID" value="${sessionScope.PRODUCT_DETAILS.statusID}"/></br>
                <input style="width: auto;" class="controlButton" type="submit" name="btnAction" value="Add to Cart"/>
            </form>
        </c:if>
        <c:if test="${sessionScope.LOGIN_USER == null}">
            <p>(*) You must be logged in to be able to purchase</p>
            <form action="MainCon" method="POST">
                <input style="width: auto;" class="controlButton" type="submit" name="btnAction" value="Log in now"/>
            </form>
        </c:if>
    </div>
</div>