<%-- 
    Document   : rightWelcomeAd
    Created on : Jan 17, 2021, 6:42:26 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="title_Right">Welcome admin ${sessionScope.LOGIN_ADMIN.username}</div>
<div class="shortDes">
    <!--Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam massa enim, ornare eu risus eget, tempus sollicitudin justo-->
</div>
<div class="title_Right">Search: </div>
<form action="MainCon?btnAction=SearchProductCon&index=1" method="POST">
    <div class="search">
        <input class="inputTxt" type="text" name="txtSearch" value="${txtSearch}">
        <input class="submitTxt" type="submit" value="Go">
    </div>
    <div class="title_Right">Price: </div>
    <div class="search">
        Min ($):  
        <input class="inputTxt" type="number" name="txtSearch_MinNum" step="0.1" value="${txtSearch_MinNum}"/>
        Max ($):  
        <input class="inputTxt" type="number" name="txtSearch_MaxNum" step="0.1" value="${txtSearch_MaxNum}"/>
    </div>
    <div class="title_Right">Category: </div>
    <div class="search">
        <select name="txtSearch_CategoryID" class="inputTxt" name="productCategory" id="productCategory" style="text-transform: capitalize;">
            <!--style="text-transform: capitalize;" https://stackoverflow.com/questions/5577364/make-the-first-character-uppercase-in-css-->
            <option value="">All</option>
            <c:forEach var="category" varStatus="counter" items="${sessionScope.LIST_CATEGORY}">
                <option value="${category.categoryName}">${category.categoryName}</option>
            </c:forEach>
        </select>
    </div>
</form>