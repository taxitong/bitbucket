<%-- 
    Document   : left_Search
    Created on : Jan 18, 2021, 8:05:54 PM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<c:if test="${sessionScope.LIST_PRODUCT != null}">
    <c:if test="${not empty sessionScope.LIST_PRODUCT}">
        <div class="title">
            Product:
        </div>
        <c:forEach var="product" varStatus="counter" items="${sessionScope.LIST_PRODUCT}">
            <div class="each2">
                <div class="image">
                    <img src="${product.image}" alt=""/>
                    <div style="text-align: center;">
                        ${product.productName}</br>
                        <div style="color: #d50000;">
                            Price: ${product.price} $</br>
                        </div>
                        <c:if test="${sessionScope.LOGIN_ADMIN != null}">
                            Status: ${product.statusID}</br>
                        </c:if>
                        </br>
                        <form action="MainCon" method="POST">
                            <input type="hidden" name="txtProductID" value="${product.productID}"/>
                            <c:if test="${sessionScope.LOGIN_ADMIN != null}">
                                <input style="width: 70px;" class="controlButton" type="submit" name="btnAction" value="Edit"/>
                                <input style="width: 70px;" class="controlButton" type="submit" name="btnAction" onclick="return confirm('Are you sure you want to delete?')" value="Delete"/>
                            </c:if>
                            <c:if test="${sessionScope.LOGIN_ADMIN == null}">
                                <input style="width: auto;" class="controlButton" type="submit" name="btnAction" value="View Details"/>
                            </c:if>    
                        </form>
                    </div>
                </div>
            </div>
        </c:forEach>
        <jsp:include page="pagingLink.jsp"/>
    </c:if>
    <c:if test="${empty sessionScope.LIST_PRODUCT}">
        <jsp:include page="noProductFound.jsp"/>
    </c:if>
</c:if>