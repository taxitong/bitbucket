CREATE DATABASE Lab231_HanaShop_BD

USE Lab231_HanaShop_BD

CREATE TABLE tblStatus (
	statusID VARCHAR(40) PRIMARY KEY,
	statusName NVARCHAR(50) NOT NULL,
);

CREATE TABLE tblAccountRole (
	roleID VARCHAR(40) PRIMARY KEY,
	roleName NVARCHAR(50)
);

CREATE TABLE tblAccount (
	accountID int IDENTITY(1,1) PRIMARY KEY,--Next time use it, change it to a string type
	username NVARCHAR(50) NOT NULL,
	password NVARCHAR(100) NOT NULL,

	displayName NVARCHAR(50),
	phoneNo varchar(15),
	address NVARCHAR(100),

	roleID VARCHAR(40) NOT NULL,
	statusID VARCHAR(40) NOT NULL,

	FOREIGN KEY(roleID) REFERENCES dbo.tblAccountRole(roleID),
	FOREIGN KEY(statusID) REFERENCES dbo.tblStatus(statusID)
);

CREATE TABLE tblOrder (
	orderID INT IDENTITY(1,1) PRIMARY KEY,--Next time use it, try to change it to a string type
	total decimal(18, 2),
	dateCreate DATE,

	accountID INT,
	username_tblOrder NVARCHAR(100),
	phone_tblOrder varchar(15),
	address_tblOrder NVARCHAR(100),
	FOREIGN KEY(accountID) REFERENCES dbo.tblAccount(accountID),
);

CREATE TABLE tblOrderDetails (
	orderDetailsID INT IDENTITY(1,1) PRIMARY KEY,--Next time use it, try to change it to a string type
	orderID INT,
	productID NVARCHAR(100),
	price decimal(18, 2),
	quantity INT,

	FOREIGN KEY(orderID) REFERENCES dbo.tblOrder(orderID),
	FOREIGN KEY(productID) REFERENCES dbo.tblProduct(productID),
);

CREATE TABLE tblProduct (
	productID NVARCHAR(100) PRIMARY KEY,
	productName NVARCHAR(16),
	price decimal(18, 2),
	quantityInStock INT,
	createDate DATE,
	-- expirationDate DATETIME,
	image VARCHAR(200),
	description NVARCHAR(500),

	categoryID VARCHAR(40),
	statusID VARCHAR(40),

	FOREIGN KEY(categoryID) REFERENCES dbo.tblCategory(categoryID),
	FOREIGN KEY(statusID) REFERENCES dbo.tblStatus(statusID),
);

CREATE TABLE tblCategory (
	categoryID VARCHAR(40) PRIMARY KEY,
	categoryName VARCHAR(50),
);

CREATE TABLE tblUpdateRecorder (
	updateRecorderID NVARCHAR(100) PRIMARY KEY,
	productID NVARCHAR(100),
	accountID INT,
	dateUpdate DATE,

	FOREIGN KEY(productID) REFERENCES dbo.tblProduct(productID),
	FOREIGN KEY(accountID) REFERENCES dbo.tblAccount(accountID),
);